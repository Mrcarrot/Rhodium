//Implementation of core library constant definitions for Rhodium
noinclude StdLib;

//static const void *nullptr = null;
unsafe static const void *nullptr = (void*)0;

//typedef bool uint8;
typedef char uint16;
typedef asciiChar uint8;

//Unary operators for overloading- not a comprehensive list, will be added to
opdef *     params 1 pos 0;
opdef &     params 1 pos 0;
opdef ++    params 1 pos 0;
opdef ++    params 1 pos 1;
opdef --    params 1 pos 0;
opdef --    params 1 pos 1;
opdef !     params 1 pos 0;
opdef ~     params 1 pos 0;

//Binary operators for overloading
opdef +     params 2 pos 1; //0 1 2
                            //x + y
opdef -     params 2 pos 1;
opdef *     params 2 pos 1;
opdef /     params 2 pos 1;
opdef %     params 2 pos 1;

opdef <<    params 2 pos 1;
opdef >>    params 2 pos 1;
opdef &     params 2 pos 1;
opdef |     params 2 pos 1;

opdef &&    params 2 pos 1;
opdef ||    params 2 pos 1;

//Ternary operator
opdef ? :   params 3 pos 1, 3; //0 1 2 3 4
                               //x ? y : z

//Array index operator
opdef [ ]   params 2 pos 1, 3; //0 1 2 3
                               //x [ y ]

//Operator definition example
//opdef * ^ & % $ params 6 pos 0, 2, 4, 6, 8; //0 1 2 3 4 5 6 7 8 9
                                              //* x ^ y & z % a $ b

//Safe pointer implementation
struct safeptr<T>
{
    //Unsafe pointer to the actual data
    protected unsafe T *address;

    //Size of data contained in pointer
    protected int64 dataSize;
    
    //Number of elements in the array, or 0 if the object is not in an array.
    protected int64 numElements;

    //The index of the pointer in the array
    protected ptr_size_t index;

    //The number of references to the pointer in the program.
    protected int64 references;

    safeptr<T>(memObject object)
    {
        address = &object;
        dataSize = sizeof(T);
        numElements = 

        references = 0;
    }

    T operator * ()
    {
        unsafe return *address;
    }

    safeptr<T> operator + (ptr_size_t amount)
    {
        if (index + amount < numElements)
        {
            return safeptr(this[index + amount]);
        }
        return nullptr;
    }

    safeptr<T> operator - (ptr_size_t amount)
    {
        if (index - amount > 0)
        {
            return safeptr(this[index - amount]);
        }
        return nullptr;
    }

    destructor
    {
        references--;
        if (references == 0)
        {
            unsafe UFree(address);
        }
    }
}

//noconst keyword- no constructors available, must be created by a core language process
noconst struct memObject
{
    type dataType;
    unsafeptr address;

    safeptr<dataType> operator &()
    {
        return safeptr(this);
    }

    unsafe (this.dataType)* operator &()
    {
        return ((dataType*)address);
    }
}