Namespace: none

Note: The Rhodium Core Library provides a number of crucial definitions for language features to function. The much larger Standard Library is optional in builds, but the CoreLib is not. For more information, see [Libraries](../Libraries.md).

## Basic Datatypes

# `struct safeptr<T>`
The safe pointer is a crucial component of Rhodium's memory safety. For more details, see [pointers](Pointers.md).

Properties:
* `unsafe T* address`: An unsafe pointer around which the safe pointer wraps
* `int64 dataSize`: The size of the data stored in the pointer
* `int64 numElements`: The number of elements in the array(or, if the referenced data is not in an array, 0)
* `int64 references`: The number of references to the pointer, which is deallocated if references ever hit 0

Constructors:
* `safeptr<T>(memObject object)`: Creates a safe pointer to a memory object. The object will not be deallocated if the pointer to it is still in use elsewhere. Used by the & operator in safe mode.