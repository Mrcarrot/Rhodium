section .data
    text1       db "What is your name?",10,0
    text2       db "Hello, ",0

section .bss
    name resb 40

section .text
    global _start

_start:
    mov rax, text1
    call _print

    mov rax, 0 ; SYS_READ
    mov rdi, 0 ; STDIN
    mov rsi, name
    mov rdx, 40
    syscall
    
    mov rax, text2
    call _print

    mov rax, name
    call _print

    mov rax, 60 ; SYS_EXIT
    mov rdi, 0
    syscall
ret

_print: 
    mov rbx, 0
    push rax

    _printLoop:
	mov cl, [rax]
	cmp cl, 0
	je _endPrintLoop
	inc rbx
	inc rax
	jmp _printLoop

    _endPrintLoop:
	mov rax, 1
	mov rdi, 1
	pop rsi
	mov rdx, rbx
	syscall
ret
