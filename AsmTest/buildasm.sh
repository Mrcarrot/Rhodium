#!/bin/sh

echo "Building $1 with NASM"
nasm -f elf64 -o ${1%.*}.o -gdwarf $1 &&
echo "Linking ${1%.*}.o with gcc" &
gcc -nostartfiles ${1%.*}.o -o ${1%.*} -lc -no-pie