# Rhodium Programming Language

Rhodium is an experimental programming language inspired primarily by C and C#. It is planned to support many features found in C, such as low-level memory management and interfacing with system functions, as well as higher-level features such as more advanced string management and memory safety.

EVERYTHING IN THIS REPOSITORY IS SUBJECT TO CHANGE AND LIKELY WILL. Note that most documents in this directory are hypothetical documentation for the language and its ecosystem, which currently **do not exist in any semblance of a completed state**. As such, they may use language such as "is" and "supports" or otherwise describe features that do not actually exist yet. For documentation about what the language *does* support, see [the compiler roadmap](PreliminaryCompiler/Roadmap.md).

Currently in this repository:
* Markdown files describing features and structures of the language
* .rh and .rhd(interchangeable, but .rh files are generally newer) source files containing unfinished preliminary implementation code
* A directory in which I was doing some basic testing of x86_64 Linux Assembly
* A very basic preliminary C# compiler which depends on the .NET SDK and clang