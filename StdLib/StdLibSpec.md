Namespace: StdLib

Note: This namespace is included by default in all projects. This namespace can, however, be explicitly specified for disambiguation, or removed(functioning like a namespace without using statements) with `noinclude StdLib;`

The Rhodium Standard Library defines many commonly used functions and datatypes for better ease of development. This is in contrast with the much smaller Core Library, which provides necessary definitions for core language features, such as safe pointers, to work.

## Basic I/O Functions

# `ostream OpenFileWrite`

`OpenFileWrite` is used to open a file for writing.

Parameters:
*  `string path`: The path to the file.

Optional Parameters:
* `bool append`: Whether to append data to the file instead of overwriting it. Default: false.

# `binObject Read`
`Read` is used to read text or binary data from a given stream (by default, StdIn).

Required Parameters: None

Optional Parameters:
* `func object readFunc(ref byte[] buffer)`: The function used to read the data. Can use the entire buffer, but is not required to. Must remove used portion from buffer(if all used, must return an array containing a null terminator). By default uses ReadLine.
* `istream input`: The input to read the data from.

# `string Str`
`Str` is used to convert a binObject to a string value. Each type must define a function to convert it to a string. If this is not done, `Str` will return null.

# `void Write`

`Write` is used to write text or binary data to a given stream (by default, StdOut).

Required Parameters:
* `binObject data`: The data to be written.

Optional Parameters:
* `ostream output`: The output stream to write the data to.
* `func string writeFunc(binObject)`: The function used to write the data. Defaults to Str(data).