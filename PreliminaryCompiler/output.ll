source_filename = "00_Tests/HelloWorld.rh"
target triple = "x86_64-pc-linux-gnu"
@.str.0 = private unnamed_addr constant [3 x i8] c"\25\73\00", align 1
@.str.1 = private unnamed_addr constant [36 x i8] c"\54\68\69\73\20\70\72\6F\67\72\61\6D\20\77\61\73\20\63\6F\6D\70\69\6C\65\64\20\66\6F\72\20\4C\69\6E\75\78\00", align 1
@.str.2 = private unnamed_addr constant [40 x i8] c"\54\68\69\73\20\70\72\6F\67\72\61\6D\20\77\61\73\20\6E\6F\74\20\63\6F\6D\70\69\6C\65\64\20\66\6F\72\20\6D\61\63\4F\53\00", align 1
@.str.3 = private unnamed_addr constant [37 x i8] c"\54\68\69\73\20\70\72\6F\67\72\61\6D\20\77\61\73\20\63\6F\6D\70\69\6C\65\64\20\66\6F\72\20\78\38\36\5F\36\34\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"\48\65\6C\6C\6F\2C\20\77\6F\72\6C\64\21\00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"\48\65\6C\6C\6F\2C\20\77\6F\72\6C\64\21\0A\00", align 1
@.str.6 = private unnamed_addr constant [4 x i8] c"\25\70\0A\00", align 1
@.str.7 = private unnamed_addr constant [4 x i8] c"\25\64\0A\00", align 1
@.str.8 = private unnamed_addr constant [5 x i8] c"\25\6C\66\0A\00", align 1
@.str.9 = private unnamed_addr constant [13 x i8] c"\74\72\75\65\20\69\73\20\74\72\75\65\00", align 1
@.str.10 = private unnamed_addr constant [14 x i8] c"\66\61\6C\73\65\20\69\73\20\74\72\75\65\00", align 1
declare i32 @time(i64 noundef %__0) #1
declare void @srand(i32 noundef %__0) #1
declare i32 @rand() #1
declare i64 @putchar(i8 noundef %__0) #1
declare i64 @wputchar(i32 noundef %__0) #1
declare i32 @puts(ptr noundef %__0) #1
declare i32 @printf(ptr noundef %__0) #1
define dso_local void @Write(ptr noundef %__0) #0
{
%str = alloca ptr, align 8 ; 1
store ptr %__0, ptr %str, align 8

%__2 = load ptr, ptr %str, align 8
call i32 @printf(ptr noundef @.str.0,ptr %__2)
ret void
}
define dso_local void @WriteLine(ptr noundef %__0) #0
{
%str = alloca ptr, align 8 ; 1
store ptr %__0, ptr %str, align 8
%__2 = load ptr, ptr %str, align 8
call i32 @puts(ptr %__2)
ret void
}
define dso_local i32 @GetANumber() #0
{
%result = alloca i32, align 4
%__0 = call i32 @rand()
store i32 %__0, ptr %result, align 4
%__1 = load i32, ptr %result, align 4
ret i32 %__1
}
define dso_local i32 @Fun() #0
{
%x = alloca i32, align 4
%__0 = call i32 @GetANumber()
store i32 %__0, ptr %x, align 4
%__1 = load i32, ptr %x, align 4
ret i32 %__1
}
define dso_local i32 @main(i32 noundef %__0,ptr noundef %__1) #0
{
%argc = alloca i32, align 4 ; 2
store i32 %__0, ptr %argc, align 4
%argv = alloca ptr, align 8 ; 3
store ptr %__1, ptr %argv, align 8
%t = alloca i32, align 4

%__4 = call i32 @time(i32 0)
store i32 %__4, ptr %t, align 4
%__5 = load i32, ptr %t, align 4
call void @srand(i32 %__5)

call i32 @puts(ptr noundef @.str.1)

call i32 @puts(ptr noundef @.str.2)

call i32 @puts(ptr noundef @.str.3)
call i32 @Fun()

call void @WriteLine(ptr noundef @.str.4)

call void @Write(ptr noundef @.str.5)
%x = alloca i32, align 4
store i32 10, ptr %x, align 4
%y = alloca i32, align 4
%__10 = call i32 @GetANumber()
store i32 %__10, ptr %y, align 4
%z = alloca double, align 8
store double 1.3, ptr %z, align 8

%__11 = load ptr, ptr %argv, align 8
call i32 @printf(ptr noundef @.str.6,ptr %__11)

%__13 = load i32, ptr %argc, align 4
call i32 @printf(ptr noundef @.str.7,i32 %__13)

%__15 = load double, ptr %z, align 8
call i32 @printf(ptr noundef @.str.8,double %__15)
br i1 true, label %__l0, label %__l1
__l0:

call i32 @puts(ptr noundef @.str.9)
br label %__l1
__l1:
br i1 false, label %__l2, label %__l3
__l2:

call i32 @puts(ptr noundef @.str.10)
br label %__l3
__l3:
ret i32 0
}