# Rhc.NET Roadmap
This is a basic roadmap for the first version of the Rhodium compiler built with C#. The goals of this project are to produce a compiler capable of compiling a similar project written in Rhodium. The current project is referred to as Rhc.NET, as opposed to Rhc(which is, for the sake of clarity, typically referred to as Rhc.Rh).

Milestones are fairly arbitrary but aim to describe current progress and future goals.

## Basic Functionality
These are features that are seen as necessary to get Rhodium "working" as a typical programming language. They are split into a few areas.

Items that are considered complete are marked with `x`. Items that are working in some form but are incomplete or have significant issues are marked with `*`.
* `[x]` Variable declaration and assignment
* `[x]` Function declaration and definition
* `[x]` Constant string data and expressions
* `[*]` Expression parsing and codegen
* `[x]` Function calls
* `[*]` Flow control(if, while, for, etc.)
* `[*]` Multi-pass compilation
* `[ ]` Strict type checking
* `[ ]` Structure(record) types
* `[ ]` Object(class) types
* `[ ]` Indirection(pointers/references/arrays)

## Advanced Functionality(Possibly for Rhc.Rh)
Broad descriptions of features that are planned for Rhodium in the future. They likely will not be implemented in Rhc.NET but will probably make it into Rhc.Rh, assuming that this project makes it that far.
* `[ ]` Generic types
* `[ ]` Memory safety
* `[ ]` Build system
* `[ ]` Compiler libraries
* `[ ]` Functional constructs
* `[ ]` Multithreading support