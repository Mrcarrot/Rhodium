%include "linux64.inc"
section .data
	const_text1 db 116, 101, 115, 116, 32, 232, 156, 130, 32, 116, 101, 120, 116, 10, 0
	const_text1_len equ 15
	const_text2 db 80, 108, 101, 97, 115, 101, 32, 101, 110, 116, 101, 114, 32, 97, 32, 115, 116, 114, 105, 110, 103, 58, 10, 0
	const_text2_len equ 24
section .bss
	var_inputText resb 40
	var_inputText2 resb 80
section .text
	global _start
_start:
	mov rax, 1
	mov rdi, 1
	mov rsi, const_text2
	mov rdx, const_text2_len
	syscall
	print const_text1
	print const_text2
	input var_inputText, 40
	print var_inputText
	exit 0
