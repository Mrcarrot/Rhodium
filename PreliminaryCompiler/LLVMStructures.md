# List of Rhodium Language Concepts and Corresponding LLVM Representations

```
Structure Name              Rhodium Example                 LLVM Example

Variable Declaration*       int32 x;                        %x = alloca i32, align 4
Variable Assignment         x = 4;                          store i32 4, i32* %x, align 4
Function Declaration        void Write(stringptr data)      define void @Write(i8* noundef %0)
External Function           extern int32 putchar(char c);   declare i32 @putchar(i32 noundef) #1
Function call               putchar('A');                   call i32 @putchar(i32 noundef 65)

* Note that in Rhodium, variables are not declared without also being assigned.