//#define COMPILE_TEST
//Basic compiler for Rhodium for bootstrapping.
//Current Features(* = partial, x = complete)
// [x] Variable declaration and assignment
// [x] Function declaration and definition
// [*] Constant string data and expressions
// [ ] Full expression parsing and codegen
// [*] Function calls
// [ ] Flow control(if, while, for, etc.)
// [ ] 
// [ ] Structure(record) types
// [ ] Object(class) types
using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

class Program
{
#if COMPILE_TEST
    [DllImport("LLVMInterface.so")]
    extern static void CPPHello();

    [DllImport("LLVMInterface.so")]
    extern static void CPPSilentCrash();

    [DllImport("LLVMInterface.so")]
    extern unsafe static void CompileRhIF(byte* bytes);
#endif

    static Dictionary<string, string> stringConstants = new Dictionary<string, string>();
    static Dictionary<string, int> stringVariables = new Dictionary<string, int>();
    static void Main(string[] args)
    {
        //Console.WriteLine(LLVMInterface.IsEven(5));
#if COMPILE_TEST
        CPPHello();
        //Dummy RhIF code to test the C++ backend
        byte[] bytes = 
        {
            0x52, 0x68, 0x49, 0x46,                         //RhIF
            0x00, 0x00, 0x00, 0x00,                         //Version 0
            0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //120(0x78) bytes
            0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //String length of file name
            0x66, 0x69, 0x6c, 0x65, 0x2e, 0x72, 0x68,       //File name in UTF-8
            0x02, 0x00,                                     //fdec
            0x00, 0x00, 0x00, 0x00,                         //flags
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //type(void)
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //name length(4)
            0x4d, 0x61, 0x69, 0x6e,                         //"Main"
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //Param count(0)
            0x00, 0x00,                                     //bsc
            0x04, 0x00,                                     //vdec
            0x00, 0x00, 0x00, 0x00,                         //flags
            0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //type(int32)
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //name length(1)
            0x78,                                           //"x"
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //expression length(1)
            0x00, 0x00,                                     //element type(constant)
            0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //data type(int32)
            0x04, 0x00, 0x00, 0x00,                         //value(4)
            0x07, 0x00,                                     //exit
            0x00, 0x00, 0x00, 0x00,                         //exit code(0)
            0x01, 0x00,                                     //esc
        };
        unsafe
        {
            fixed (byte* bytesPtr = bytes)
            {
                CompileRhIF(bytesPtr);
            }
        }
        return;
#endif
        bool runPreprocessor = !args.Contains("--disable-preprocessor");
        bool compileOnly = args.Contains("-c");
        bool outputAsm = args.Contains("-S");
        bool stripOutput = args.Contains("-s");
        bool outputLLVM = args.Contains("-emit-llvm");
        string outputName = "a.out";
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "-o")
            {
                i++;
                if (i >= args.Length)
                {
                    ErrorReporting.WriteError("Expected filename when given the -o option");
                }
                else
                {
                    outputName = args[i];
                }
            }
            if (args[i].StartsWith("--target"))
            {
                if (args[i].StartsWith("--target="))
                {
                    Target target = Platform.ParseTriple(args[i].Split('=')[1]);
                    Platform.SetTarget(target);
                    Console.WriteLine($"rhc: Setting buld target to {Platform.GetTriple(target)}");
                }
                else
                {
                    i++;
                    if (i >= args.Length)
                    {
                        ErrorReporting.WriteError("Expected target name when given the --target option");
                    }
                    else
                    {
                        Target target = Platform.ParseTriple(args[i]);
                        Platform.SetTarget(target);
                        Console.WriteLine($"rhc: Setting buld target to {Platform.GetTriple(target)}");
                    }
                }
            }
        }
        Platform.InitializeTargetInfo();
        if (args.Length >= 1)
        {
            if (args[0] == "lex")
            {
                if (args.Length >= 2)
                {
                    try
                    {
                        List<Token>? tokens = Lexer.LexFile(args[1], true);
                        if (tokens is null) return;
                        foreach (Token tok in tokens)
                        {
                            Console.WriteLine($"{tok.file}\t{tok.line}:{tok.col} {tok.text}");
                        }
                    }
                    catch (RhCompilerException e)
                    {
                        ErrorReporting.WriteError(e.Message, e.File, e.Line, e.Col);
                    }

                }
                else
                {
                    Console.WriteLine("rhc: Please provide a file as input");
                }
            }
            else if (args[0] == "parse")
            {
                if (args.Length >= 2)
                {
                    try
                    {
                        SyntaxTree tree = Parser.ProcessTokens(Lexer.LexFile(args[1], runPreprocessor)!)!;
                        if (ErrorReporting.Errors > 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Error.WriteLine($"Compilation failed: {ErrorReporting.Errors} errors, {ErrorReporting.Warnings} warnings. Fix errors and try again.");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.WriteLine(tree);
                            BinaryWriter writer = new(new FileStream("RhIFTest.rhif", FileMode.Create));
                            writer.Write(tree.ToRhIF());
                            writer.Close();
                        }
                    }
                    catch (RhCompilerException e)
                    {
                        ErrorReporting.WriteError(e.Message, e.File, e.Line, e.Col);
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(e.ToString());
                        Console.ResetColor();
                    }
                }
                else
                {
                    Console.WriteLine("rhc: Please provide a file as input");
                }
            }
            else if (args[0] == "com")
            {
                if (args.Length >= 2)
                {
                    try
                    {
                        var tokens = Lexer.LexFile(args[1], runPreprocessor);
                        if (tokens is not null)
                        {
                            var tree = Parser.ProcessTokens(tokens);
                            if (tree is not null)
                            {
                                string code = Compiler.Compile(tree);
                                if (ErrorReporting.Errors == 0)
                                {
                                    Console.Error.WriteLine($"Compilation finished: 0 errors, {ErrorReporting.Warnings} warnings");
                                    Console.WriteLine(code);
                                    if (outputLLVM)
                                    {
                                        File.WriteAllText(outputName, code);
                                        return;
                                    }
                                    File.WriteAllText("out.ll", code);
                                    string clangArgs = $"out.ll -o {outputName} --target={Platform.GetTriple(Platform.CurrentTarget)}";
                                    if (compileOnly) clangArgs += " -c";
                                    if (outputAsm) clangArgs += " -S -masm=intel";
                                    if (stripOutput) clangArgs += " -s";
                                    Process clangProc = Process.Start("clang", clangArgs);
                                    clangProc.WaitForExit();
                                    if (clangProc.ExitCode != 0)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.Error.WriteLine($"rhc: Error: clang subprocess returned {clangProc.ExitCode}");
                                        Environment.Exit(clangProc.ExitCode);
                                    }
                                }
                            }
                        }
                    }
                    catch (RhCompilerException e)
                    {
                        ErrorReporting.WriteError(e.Message, e.File, e.Line, e.Col);
                    }
                    //catch (Exception e)
                    {
                        //throw;
                        Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine(e.ToString());
                        Console.ResetColor();
                    }
                }
            }
            else if (args[0] == "expr")
            {
                Console.WriteLine("Entering expression debug mode. Type an expression to see how it will be parsed.");
                while (true)
                {
                    Console.Write("> ");
                    if (Console.ReadLine() is { } code)
                        if (Lexer.LexCode(code, "stdin", false) is { } exprTokens)
                        {
                            BasicScope scope = new(exprTokens[0], null);
                            Console.WriteLine($"{exprTokens.Count - 2} tokens");
                            RhExpression expr = Parser.ParseExpression(exprTokens.Where(x => x.text.Length > 0).ToList(), scope);
                            foreach (ExpressionElement element in expr.Elements)
                            {
                                Console.WriteLine(element.Type);
                            }
                            Console.WriteLine(ExpressionParser.CreateExpressionTree(expr.Elements, scope).ToString());
                        }
                        else Console.WriteLine("Could not lex expression");
                    else Console.WriteLine("Could not read input");
                }
            }
            else
            {
                ErrorReporting.WriteError($"Unrecognized operation: {args[0]}\nSupported operations: lex parse com expr");
            }
            if (ErrorReporting.Errors > 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"Compilation failed: {ErrorReporting.Errors} errors, {ErrorReporting.Warnings} warnings. Fix errors and try again.");
                Console.ResetColor();
            }
        }
        else
        {
            Console.WriteLine($"Rhc {Utils.CurrentVersion}: Compiler for the Rhodium programming language\nBuilt for {Platform.GetTriple(Platform.NativeTarget())}");
        }
        /*else
        {
            stringConstants.Add("text1", "test 蜂 text\n");
            stringConstants.Add("text2", "Please enter a string:\n");

            stringVariables.Add("inputText", 40);
            stringVariables.Add("inputText2", 80);

            string outputFile = "%include \"linux64.inc\"";

            //data section
            outputFile += $"\nsection .data";
            foreach (KeyValuePair<string, string> constant in stringConstants)
            {
                outputFile += $"\n{DeclareString(constant.Key, constant.Value)}";
            }

            //bss section
            outputFile += "\nsection .bss";
            foreach (KeyValuePair<string, int> variable in stringVariables)
            {
                outputFile += $"\n{DeclareStringVariable(variable.Key, variable.Value)}";
            }

            //text section
            outputFile += "\nsection .text\n\tglobal _start\n_start:\n";

            outputFile += Syscall(1, "1", "const_text2", "const_text2_len");

            outputFile += "\n\tprint const_text1\n\tprint const_text2";

            outputFile += "\n\tinput var_inputText, 40\n\tprint var_inputText";

            outputFile += "\n\texit 0\n";

            Console.WriteLine(outputFile);

            if (File.Exists("temp_asm.asm")) File.Delete("temp_asm.asm");
            if (File.Exists("temp_obj.o")) File.Delete("temp_obj.o");

            File.WriteAllText("temp_asm.asm", outputFile);

            Console.WriteLine("Compiling asm with nasm.");
            var nasm = Process.Start("nasm", $"-f elf64 -o temp_obj.o temp_asm.asm");
            nasm.WaitForExit();
            Console.WriteLine("Linking obj with ld.");
            var ld = Process.Start("ld", $"temp_obj.o -o a.out");
            ld.WaitForExit();
            Console.WriteLine("Compilation finished.");
        }*/
    }

    /// <summary>
    /// Determines the x86_64 assembly code used to define a given string constant in the .data section.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    static string DeclareString(string name, string data)
    {
        byte[] binData = Encoding.UTF8.GetBytes(data);
        string output = $"\tconst_{name} db {binData[0]}";

        for (int i = 1; i < binData.Length; i++)
        {
            output += $", {binData[i]}";
        }
        output += ", 0";
        output += $"\n\tconst_{name}_len equ {binData.Length + 1}";

        return output;
    }

    /// <summary>
    /// Determines the x86_64 assembly code used to reserve a given string variable in the .bss section.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    static string DeclareStringVariable(string name, int length)
    {
        string output = $"\tvar_{name} resb {length}";

        return output;
    }



    static string Syscall(int operation, params string[] parameters)
    {
        string output = $"\tmov rax, {operation}";

        string[] registers =
        {
            "rdi",
            "rsi",
            "rdx",
            "rcx",
            "r8",
            "r9"
        };

        for (int i = 0; i < parameters.Length; i++)
        {
            if (i < registers.Length)
            {
                output += $"\n\tmov {registers[i]}, {parameters[i]}";
            }
            else
            {
                output += $"\n\tpush {parameters[i]}";
            }
        }

        output += "\n\tsyscall";

        return output;
    }
}
