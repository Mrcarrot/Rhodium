using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

//public record ExpressionElement(ExpressionElementType type, Token text, byte[] contents);

public class RhExpression : STNode
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public List<ExpressionElement> Elements;

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public RhExpression(Token startToken, Scope parent, IReadOnlyCollection<ExpressionElement> elements)
    {
        Parent = parent;
        StartToken = startToken;
        Elements = elements.ToList();
    }

    public RhExpression(Token startToken, Token endToken, Scope parent, IReadOnlyCollection<ExpressionElement> elements)
    {
        Parent = parent;
        StartToken = startToken;
        EndToken = endToken;
        Elements = elements.ToList();
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(64);
        foreach (ExpressionElement element in Elements)
        {
            output.Append($" {element.ToString()} ");
        }
        return output.ToString();
    }

    public string CodegenLLVMIR(out string toUse, bool includeType = true, bool passedToOther = false)
    {
        if (Elements.Count > 1)
        {
            return ExpressionParser.CreateExpressionTree(Elements, Parent).ToLLVMIR(out toUse);
            //ErrorReporting.CompilerError(Elements[1].StartToken, "Expressions with more than 1 element are not supported in this version of rhc");
        }
        StringBuilder output = new(32);
        if (Elements[0] is ExpressionVariable variable)
        {
            RhType type = TypeSystem.Types[variable.Variable.type];
            string llvmType = TypeSystem.GetLLVMTypeName(type.name);
            output.Append($"%__{Parent.OperationIndex} = load {llvmType}, ptr %{variable.Variable.identifier}, align {type.size}");
            toUse = includeType ?
            $"{llvmType} %__{Parent.OperationIndex}" :
            $"%__{Parent.OperationIndex}";
            Console.WriteLine($"{includeType}: {toUse}");
            Parent.OperationIndex++;
        }
        else if (Elements[0] is ExpressionFunctionCall functionCall)
        {
            string llvmType = TypeSystem.GetLLVMTypeName(functionCall.Function.type);
            StringBuilder call = new(64);
            call.Append($"%__00_OP_INDEX = call {llvmType} @{functionCall.Function.identifier}(");
            for (int i = 0; i < functionCall.Parameters.Count - 1; i++)
            {
                output.Append($"\n{functionCall.Parameters[i].CodegenLLVMIR(out string expression, true, true)}");
                call.Append($"{expression},");
            }
            if (functionCall.Parameters.Count > 0)
            {
                Debug.Assert(Parent == functionCall.Parameters[0].Parent);
                output.Append($"\n{functionCall.Parameters[^1].CodegenLLVMIR(out string expression, true, true)}");
                call.Append($"{expression}");
            }
            call.Append(')');
            output.Append($"\n{call.Replace("00_OP_INDEX", Parent.OperationIndex.ToString())}");
            toUse = includeType ?
            $"{llvmType} %__{Parent.OperationIndex}" :
            $"%__{Parent.OperationIndex}";
            Parent.OperationIndex++;
        }
        else toUse = ToLLVMIR(includeType, passedToOther); //If we don't need to prepare anything, use this for codegen
        return output.ToString();
    }

    public string ToLLVMIR() => ToLLVMIR(true);
    public string ToC()
    {
        StringBuilder output = new();
        ExpressionNode treeRoot = ExpressionParser.CreateExpressionTree(Elements, Parent);
        throw new NotImplementedException();
    }

    //Remember to implement properly later
    public string ToLLVMIR(bool includeType, bool passedToOther = false)
    {
        if (Elements.Count == 1)
        {
            if (Elements[0] is ExpressionValue value)
            {
                RhType type = TypeSystem.Types[value.Value.type];
                if (type.name != "stringptr")
                {
                    string LLVMType = TypeSystem.GetLLVMTypeName(type.name);
                    string valueString = TypeSystem.GetLLVMString(value.Value);
                    string valueStr = includeType ?
                        $"{LLVMType} {valueString}" :
                        valueString;
                    return valueStr;
                }
                else
                {
                    int stringIndex = (int)BitConverter.ToUInt64(value.Value.contents[0..8]);
                    int length = Tree.Strings[stringIndex].contents.Length - 8;
                    string text = passedToOther ?
                        $"noundef @.str.{stringIndex}" :
                        $"@.str.{stringIndex}";
                    if (includeType) text = $"ptr {text}";
                    return text;
                }
            }
            else if (Elements[0] is ExpressionVariable variable)
            {
                //RhType type = TypeHandler.Types.FirstOrDefault(x => x.Value.id == BitConverter.ToUInt64(Elements[0].contents[0..8])).Value;
                string variableName = variable.Variable.identifier;
                string llvmType = TypeSystem.GetLLVMTypeName(Parent.Variables[variableName].type);
                return includeType ?
                $"{llvmType} %{variableName}" :
                $"%{variableName}";
            }
            /*else if (Elements[0] is ExpressionFunctionCall functionCall)
            {
                StringBuilder output = new(64);
                output.Append($"call {TypeSystem.GetLLVMTypeName(functionCall.Function.type)} @{functionCall.Function.identifier}(");
                for (int i = 0; i < functionCall.Parameters.Count - 1; i++)
                {
                    output.Append($"{functionCall.Parameters[i].ToLLVMIR()},");
                }
                if (functionCall.Parameters.Count > 0)
                {
                    output.Append($"{functionCall.Parameters[^1].ToLLVMIR()}");
                }
                output.Append(')');
                return output.ToString();
            }*/
            else
            {
                ErrorReporting.CompilerError(StartToken, "Expression element types other than constant values, variable names, and function calls are not supported in this version of rhc");
            }
        }
        else if (Elements.Count == 3) //Single binary operator
        {
            if (Elements[1] is ExpressionBuiltInOp op)
            {
                switch (op.Operator)
                {
                    case (BuiltInOperator.Add):
                    {

                        break;
                    }
                }
            }
            else
            {
                ErrorReporting.CompilerError(Elements[1].StartToken, $"Unexpected operator in binary expression: {Elements[1].StartToken.text}");
            }
        }
        else if (Elements.Count > 1)
        {
            ErrorReporting.CompilerError(StartToken, "Expressions with more than one element are not supported in this version of rhc");
        }

        return includeType ? "void" : "";
    }

    public RhType DetermineType()
    {
        if (Elements.Count == 1) return Elements[0].GetReturnType();
        RhType currentType = TypeSystem.Types["void"];
        return Elements.Aggregate(currentType, (current, element) => TypeSystem.PromoteType(current, element.GetReturnType()) ?? current);
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(64);
        output.AddRange(BitConverter.GetBytes((ulong)Elements.Count));
        //throw new NotImplementedException("Expression RhIF support is currently non-functional");
        foreach (ExpressionElement element in Elements)
        {
            output.AddRange(BitConverter.GetBytes((UInt16)element.Type));
            output.AddRange(element.ToRhIF());
        }
        return output.ToArray();
    }

    public static RhExpression Empty(Token startToken, Scope parent)
    {
        return new(startToken, startToken, parent, new List<ExpressionElement>());
    }

    /*public static RhExpression ParseRhIF(byte[] expressionRhIF, ref int index)
    {
        ulong expressionLength = BitConverter.ToUInt64(expressionRhIF[index..(index + 8)]);
        index += 8;
        return null;
    }*/

    public static readonly BuiltInOperator[] BinaryOperators =
    {
        BuiltInOperator.Add,
        BuiltInOperator.Subtract,
        BuiltInOperator.Multiply,
        BuiltInOperator.Divide,
        BuiltInOperator.Modulus,
        BuiltInOperator.EqualTo,
        BuiltInOperator.NotEqual,
        BuiltInOperator.GreaterThan,
        BuiltInOperator.GreaterOrEqual,
        BuiltInOperator.LessThan,
        BuiltInOperator.LessOrEqual,
        BuiltInOperator.And,
        BuiltInOperator.Or,
        BuiltInOperator.LeftShift,
        BuiltInOperator.RightShift
    };
}

public interface ExpressionElement
{
    public ExpressionElementType Type { get; }
    public Token StartToken { get; }
    public byte[] ToRhIF();
    public RhType GetReturnType();
    //public string ToC();
}

public class ExpressionValue : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.Value; } }

    public Token StartToken { get; init; }

    public RhValue Value { get; init; }

    public byte[] ToRhIF()
    {
        List<byte> output = new(32);

        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(BitConverter.GetBytes(TypeSystem.Types[Value.type].id));
        output.AddRange(Value.contents);

        return output.ToArray();
    }

    public RhType GetReturnType()
    {
        TypeSystem.ResolveTypeAlias(Value.type, out string canonicalTypeName);
        return TypeSystem.Types[canonicalTypeName];
    }

    public ExpressionValue(Token startToken, RhValue value)
    {
        StartToken = startToken;
        Value = value;
    }

    public override string ToString()
    {
        return TypeSystem.GetString(Value);
    }
}

public class ExpressionVariable : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.Variable; } }

    public Token StartToken { get; init; }

    public RhVariable Variable { get; init; }

    public ExpressionVariable(Token startToken, RhVariable variable)
    {
        StartToken = startToken;
        Variable = variable;
    }

    public RhType GetReturnType()
    {
        TypeSystem.ResolveTypeAlias(Variable.type, out string canonicalTypeName);
        return TypeSystem.Types[canonicalTypeName];
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(32);

        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(BitConverter.GetBytes(TypeSystem.Types[Variable.type].id));
        output.AddRange(Utils.MakeRhIFString(Variable.identifier));

        return output.ToArray();
    }

    public override string ToString() => Variable.identifier;
}

public class ExpressionBuiltInOp : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.BuiltInOp; } }

    public Token StartToken { get; init; }

    public BuiltInOperator Operator { get; init; }

    public ExpressionBuiltInOp(Token startToken, BuiltInOperator op)
    {
        StartToken = startToken;
        Operator = op;
    }

    public RhType GetReturnType() => TypeSystem.Types["void"];

    public byte[] ToRhIF()
    {
        List<byte> output = new(4);

        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(BitConverter.GetBytes((UInt16)Operator));

        return output.ToArray();
    }

    public string ToC(params ExpressionElement[] operands) => Operator switch
    {
        _ => throw new NotImplementedException()
        //BuiltInOperator.Add => $"({operands[0].ToC()}) + ({operands[1].ToC()})"
    };

    public override string ToString() => Operator.ToString();
}

public class ExpressionCustomOp : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.CustomOp; } }

    public Token StartToken { get; init; }

    public string Operator { get; init; }

    public ExpressionCustomOp(Token startToken, string op)
    {
        StartToken = startToken;
        Operator = op;
    }

    public RhType GetReturnType() => TypeSystem.Types["void"];

    public byte[] ToRhIF()
    {
        List<byte> output = new(32);

        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(Utils.MakeRhIFString(Operator));

        return output.ToArray();
    }

    public override string ToString() => Operator;
}

public class ExpressionOpenParen : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.OpenParen; } }

    public Token StartToken { get; init; }

    public ExpressionOpenParen(Token startToken)
    {
        StartToken = startToken;
    }

    public RhType GetReturnType() => TypeSystem.Types["void"];

    public byte[] ToRhIF() => BitConverter.GetBytes((UInt16)Type);

    public override string ToString() => "(";
}

public class ExpressionCloseParen : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.CloseParen; } }

    public Token StartToken { get; init; }

    public ExpressionCloseParen(Token startToken)
    {
        StartToken = startToken;
    }

    public RhType GetReturnType() => TypeSystem.Types["void"];

    public byte[] ToRhIF() => BitConverter.GetBytes((UInt16)Type);

    public override string ToString() => ")";
}

public class ExpressionFunctionCall : ExpressionElement
{
    public ExpressionElementType Type { get { return ExpressionElementType.FunctionCall; } }

    public Token StartToken { get; init; }

    public RhFunction Function { get; init; }

    public List<RhExpression> Parameters { get; init; }

    public ExpressionFunctionCall(Token startToken, RhFunction function, IEnumerable<RhExpression>? parameters = null)
    {
        StartToken = startToken;
        Function = function;
        Parameters = (List<RhExpression>?)parameters ?? new List<RhExpression>();
    }

    public RhType GetReturnType()
    {
        TypeSystem.ResolveTypeAlias(Function.type, out string canonicalTypeName);
        return TypeSystem.Types[canonicalTypeName];
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(64);

        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(Utils.MakeRhIFString(Function.identifier));
        output.AddRange(BitConverter.GetBytes((ulong)Parameters.Count));
        foreach (RhExpression exp in Parameters)
        {
            output.AddRange(exp.ToRhIF());
        }

        return output.ToArray();
    }

    public override string ToString()
    {
        StringBuilder output = new(64);
        output.Append($"call {Function.identifier}(");
        foreach (RhExpression expr in Parameters)
        {
            output.Append($" {expr.ToString()} ");
        }
        output.Append(")");
        return output.ToString();
    }
}

public class ExpressionCast : ExpressionElement
{
    public ExpressionElementType Type { get => ExpressionElementType.Cast; }

    public Token StartToken { get; init; }

    public RhType CastType { get; init; }

    public ExpressionCast(Token startToken, RhType type)
    {
        StartToken = startToken;
        CastType = type;
    }

    public RhType GetReturnType() => CastType;

    public byte[] ToRhIF()
    {
        List<byte> output = new(10);
        output.AddRange(BitConverter.GetBytes((UInt16)Type));
        output.AddRange(BitConverter.GetBytes(CastType.id));
        return output.ToArray();
    }

    public override string ToString() => $"(cast {CastType.name})";
}

public class ExpressionTypeName : ExpressionElement
{
    public ExpressionElementType Type => ExpressionElementType.TypeName;

    public Token StartToken { get; init; }

    public RhType NamedType { get; init; }

    public ExpressionTypeName(Token startToken, RhType type)
    {
        StartToken = startToken;
        NamedType = type;
    }

    public RhType GetReturnType() => NamedType;

    public byte[] ToRhIF() => throw new NotImplementedException();

    public override string ToString() => $"(type {NamedType.name})";
}

public enum ExpressionElementType : UInt16
{
    Value,
    Variable,
    BuiltInOp,
    CustomOp,
    OpenParen,
    CloseParen,
    FunctionCall,
    Cast,
    TypeName
}

public enum BuiltInOperator : UInt16
{
    Add,
    Subtract,
    Multiply,
    Divide,
    Modulus,
    EqualTo,
    NotEqual,
    And,
    Or,
    Not,
    LessThan,
    GreaterThan,
    LessOrEqual,
    GreaterOrEqual,
    LeftShift = 17,
    RightShift
}

/*  Things expressions need to implement
    * Parentheses
    * Operands(variable and constant)
    * Operators(extensible)
*/
