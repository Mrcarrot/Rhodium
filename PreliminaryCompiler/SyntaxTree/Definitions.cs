using System.Text;
public class TypeDefinition : STNode
{
    public string Alias { get; }

    public string UnderlyingType { get; }

    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(32);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"typedef {Alias} {UnderlyingType}");
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        var aliasBytes = Utils.MakeRhIFString(Alias);
        var typeBytes = BitConverter.GetBytes(TypeSystem.Types[UnderlyingType].id);
        List<byte> output = new(aliasBytes.Length + typeBytes.Length + 2);
        output.AddRange(BitConverter.GetBytes((Int16)6));
        output.AddRange(aliasBytes);
        output.AddRange(typeBytes);
        return output.ToArray();
    }

    public string ToC() => "";

    public string ToLLVMIR() => "";

    public TypeDefinition(Token startToken, Token endToken, Scope parent, string alias, string underlyingType)
    {
        StartToken = startToken;
        EndToken = endToken;
        Parent = parent;
        Alias = alias;
        UnderlyingType = underlyingType;
    }
}
