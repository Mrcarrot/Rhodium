using System.Text;

#warning TODO: Implement C backend for Functions.cs

public class FunctionDeclaration : Scope
{
    public string Identifier { get; }

    public RhFunction Function { get; }

    public List<RhVariable> Parameters { get; }

    public bool Extern { get; internal set; }

    public bool uncheckedParams { get; internal set; }

    public Scope Parent { get; internal set; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public List<STNode> Children { get; }

    public Dictionary<string, RhVariable> Variables { get; }

    public Dictionary<string, List<RhFunction>> Functions { get; }

    public ulong OperationIndex { get; set; }

    public FunctionDeclaration(Token startToken, Scope parent, string identifier, RhFunction function, bool rh_extern = false, bool uncheckedParams = false)
    {
        StartToken = startToken;
        Parent = parent;
        Identifier = identifier;
        Function = function;
        Parameters = function.parameters;
        Extern = rh_extern;
        Children = new();
        Variables = new();
        Functions = new();
        foreach (RhVariable param in function.parameters)
        {
            Variables.Add(param.identifier, param);
        }
        //OperationIndex = 1;
    }

    public FunctionDeclaration(Token startToken, Token endToken, Scope parent, string name, RhFunction function, bool rh_extern = false, bool uncheckedParams = false)
    : this(startToken, parent, name, function, rh_extern, uncheckedParams)
    {
        EndToken = endToken;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(256);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"func");
        if (Extern) output.Append(" extern");
        output.Append($" {Function.type} {Identifier} (");
        foreach (RhVariable variable in Parameters)
        {
            output.Append($" {variable.type} {variable.identifier} ");
        }
        output.Append(')');
        if (!Extern)
        {
            output.Append($"\n");
            for (int i = 0; i < depth; i++)
            {
                output.Append('\t');
            }
            output.Append("Scope");
            foreach (STNode node in Children)
            {
                output.Append($"\n{node.ToString(depth + 1)}");
            }
            output.Append("\n");
            for (int i = 0; i < depth; i++)
            {
                output.Append('\t');
            }
            output.Append("End Scope");
        }
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    public string ToLLVMIR()
    {
        string paramsList()
        {
            StringBuilder output = new("(", 64);
            int i;
            for (i = 0; i < Function.parameters.Count - 1; i++)
            {
                output.Append($"{TypeSystem.GetLLVMTypeName(Function.parameters[i].type)} noundef %__{OperationIndex},");
                OperationIndex++;
            }
            if (i < Function.parameters.Count)
            {
                output.Append($"{TypeSystem.GetLLVMTypeName(Function.parameters[i].type)} noundef %__{OperationIndex}");
                OperationIndex++;
            }
            output.Append(")");
            return output.ToString();
        }

        StringBuilder output = new(256);
        if (Extern)
        {
            return $"declare {TypeSystem.GetLLVMTypeName(Function.type)} @{Identifier}{paramsList()} #1";
        }
        //string type = identifier != "main" ? TypeHandler.GetLLVMTypeName(Function.type) : "i32";
        output.Append($"define dso_local {TypeSystem.GetLLVMTypeName(Function.type)} @{Identifier}{paramsList()} #0");
        output.Append("\n{");
        //OperationIndex = 0;
        for (int i = 0; i < Parameters.Count; i++)
        {
            RhType paramType = TypeSystem.Types[Parameters[i].type];
            string llvmType = TypeSystem.GetLLVMTypeName(paramType.name);
            output.Append($"\n%{Parameters[i].identifier} = alloca {llvmType}, align {paramType.size} ; {OperationIndex}");
            OperationIndex++;
            output.Append($"\nstore {llvmType} %__{i}, ptr %{Parameters[i].identifier}, align {paramType.size}");
        }
        //OperationIndex = (ulong)(Parameters.Count - 1 < 0 ? 0 : Parameters.Count - 1);
        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToLLVMIR()}");
        }
        if (!Children.Any(x => x is ReturnStatement))
        {
            if (Function.type == "void")
                output.Append("\nret void");
            else
                ErrorReporting.CompilerWarning(EndToken!, "The function {Identifier} might not return a value");
        }
        if (output.ToString().Split('\n')[^1].EndsWith(':'))
        {
            output.Append($"\n{TypeSystem.GetDefaultReturn(Function.type)}");
        }
        output.Append("\n}");
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(128);
        output.AddRange(BitConverter.GetBytes((UInt16)2));
        UInt32 flags = 0;
        if (Extern) flags |= 0x2000;
        output.AddRange(BitConverter.GetBytes(flags));
        output.AddRange(BitConverter.GetBytes(TypeSystem.Types[Function.type].id));
        output.AddRange(Utils.MakeRhIFString(Function.identifier));
        output.AddRange(BitConverter.GetBytes((ulong)Parameters.Count));
        for (int i = 0; i < Parameters.Count; i++)
        {
            UInt32 pflags = 0;
            //Parameter flags not yet implemented
            output.AddRange(BitConverter.GetBytes(pflags));
            output.AddRange(BitConverter.GetBytes(TypeSystem.Types[Parameters[i].type].id));
            output.AddRange(Utils.MakeRhIFString(Parameters[i].identifier));
        }
        return new List<byte>().ToArray();
    }
}

public class FunctionCall : STNode
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public RhFunction Function { get; }

    public List<RhExpression> Parameters { get; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public FunctionCall(Scope parent, Token startToken, RhFunction function)
        : this(parent, startToken, null, function, new List<RhExpression>()) { }

    public FunctionCall(Scope parent, Token startToken, RhFunction function, IEnumerable<RhExpression> parameters)
        : this(parent, startToken, null, function, parameters) { }

    public FunctionCall(Scope parent, Token startToken, Token? endToken, RhFunction function, IEnumerable<RhExpression> parameters)
    {
        Parent = parent;
        StartToken = startToken;
        EndToken = endToken;
        Function = function;
        Parameters = new();
        Parameters = parameters.ToList();
    }

    public string ToString(int depth)
    {
        StringBuilder output = new(64);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"call {Function.identifier} (");
        foreach (RhExpression value in Parameters)
        {
            output.Append(value.ToString());
        }
        output.Append(")");
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    public string ToLLVMIR()
    {
        StringBuilder output = new(64);
        StringBuilder call = new(64);
        call.Append($"call {TypeSystem.GetLLVMTypeName(Function.type)} @{Function.identifier}(");
        foreach (RhExpression expr in Parameters)
        {
            output.Append($"\n{expr.CodegenLLVMIR(out string expression, true, true)}");
            call.Append($"{expression},");
        }
        if (call[^1] == ',')
        {
            call.Remove(call.Length - 1, 1);
        }
        call.Append(')');
        output.Append($"\n{call}");
        if (Function.type != "void") Parent.OperationIndex++;
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(64);
        output.AddRange(Utils.MakeRhIFString(Function.identifier));
        output.AddRange(BitConverter.GetBytes((ulong)Function.parameters.Count));
        foreach (RhExpression value in Parameters)
        {
            output.AddRange(value.ToRhIF());
        }
        return output.ToArray();
    }
}

public class ReturnStatement : STNode
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public RhExpression Expression { get; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public ReturnStatement(Token startToken, Scope parent, RhExpression? expression = null)
    {
        StartToken = startToken;
        Parent = parent;
        EndToken = startToken;
        Expression = expression ?? RhExpression.Empty(startToken, parent);
    }

    public ReturnStatement(Token startToken, Token endToken, Scope parent, RhExpression? expression)
    {
        StartToken = startToken;
        Parent = parent;
        EndToken = endToken;
        Expression = expression ?? RhExpression.Empty(endToken, parent);
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(32);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("return ");
        if (Expression != null) output.Append(Expression.ToString());
        return output.ToString();
    }
    
    public string ToC() => throw new NotImplementedException();

    //Remember to implement properly once I figure out expression codegen
    public string ToLLVMIR()
    {
        if (IsInFunctionRecursive(Parent, out FunctionDeclaration? fdec))
        {
            //Under the conditions where this code runs, fdec is never null, so we override the warning.
            /*if (fdec!.Function.identifier == "Main" || fdec.Function.identifier == "main")
            {
                if (Expression.ToLLVMIR().Trim() == "void")
                {
                    return "ret i32 0";
                }
                else
                {
                    return $"ret i32 {Expression.ToLLVMIR(false, true)}";
                }
            }*/
            string expressionPrep = Expression.CodegenLLVMIR(out string expression, false, true);
            return $"{expressionPrep}\nret {TypeSystem.GetLLVMTypeName(fdec!.Function.type)} {expression}";
        }
        else ErrorReporting.CompilerError(StartToken, "Cannot return: not in a function");
        string expressionPrep2 = Expression.CodegenLLVMIR(out string expression2, true, true);
        return $"{expressionPrep2}\nret {expression2}";
    }

    private bool IsInFunctionRecursive(Scope scope, out FunctionDeclaration? fdec)
    {
        if (scope is FunctionDeclaration func)
        {
            fdec = func;
            return true;
        }
        if (scope.Parent == null)
        {
            fdec = null;
            return false;
        }
        return IsInFunctionRecursive(scope.Parent, out fdec);
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(64);
        output.AddRange(BitConverter.GetBytes((ushort)12));
        output.AddRange(Expression.ToRhIF());
        return output.ToArray();
    }
}
