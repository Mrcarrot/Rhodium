using System.Text;

public class ExitCall : STNode
{
    public Scope? Parent { get; }
    public Token StartToken { get; }
    public Token? EndToken { get; set; }

    public int ExitCode { get; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public ExitCall(Token startToken, Token endToken, Scope? parent, int exitCode)
    {
        StartToken = startToken;
        EndToken = endToken;
        Parent = parent;
        ExitCode = exitCode;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(64);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"Exit with code {ExitCode}");
        return output.ToString();
    }

#warning TODO: Make sure that the exit() function is declared when compiling to C
    public string ToC() => $"exit({ExitCode});";

    public string ToLLVMIR() => $"ret i32 {ExitCode}";

    public byte[] ToRhIF()
    {
        List<byte> output = new(6);
        output.AddRange(BitConverter.GetBytes((Int16)7));
        output.AddRange(BitConverter.GetBytes(ExitCode));
        return output.ToArray();
    }
}
