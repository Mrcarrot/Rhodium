using System.Text;

public class ExpressionNode
{
    public ExpressionOperation Operation { get; }
    public ExpressionElement Element { get; init; }
    public List<ExpressionNode> Children { get; init; }
    public Scope Scope { get; init; }

    public string GetReturnType()
    {
        return Element is ExpressionBuiltInOp or ExpressionCustomOp ? 
            TypeSystem.PromoteType(Children[0].Element.GetReturnType(), Children[1].Element.GetReturnType())!.name : 
            Element.GetReturnType().name;
    }
    
    public ExpressionNode(ExpressionElement element, Scope scope)
    {
        Element = element;
        Operation = ExpressionParser.GetOperationFromElement(element);
        Children = new();
        Scope = scope;
    }

    public string ToLLVMIR(out string tempToUse, bool includeType = true)
    {
        StringBuilder output = new(128);
        
        if (Element is ExpressionBuiltInOp)
        {
            //{TypeSystem.GetLLVMTypeName(Element.GetReturnType().name)}
            //if (Children.Count != 2) ErrorReporting.CompilerError(Element.StartToken, "This version of rhc does not support unary operators");
            output.Append($"\n{Children[0].ToLLVMIR(out string element0, false)}");
            output.Append($"\n{Children[1].ToLLVMIR(out string element1, false)}");
            RhType returnType = TypeSystem.PromoteType(Children[0].GetReturnType(), Children[1].GetReturnType())!;
            string llvmType = TypeSystem.GetLLVMTypeName(returnType.name);
            output.Append($"%__{Scope.OperationIndex} = {TypeSystem.GetOperationInstruction(returnType.name, 
                Operation)} {llvmType} {element0}, {element1}");
            tempToUse = includeType ? 
                        $"{llvmType} %__{Scope.OperationIndex++}" :
                        $"%__{Scope.OperationIndex++}";
        }
        else if (Element is ExpressionValue val)
        {
            tempToUse = includeType ? $"{TypeSystem.GetLLVMTypeName(val.Value.type)} {TypeSystem.GetLLVMString(val.Value)}"
                                    : $"{TypeSystem.GetLLVMString(val.Value)}";
        }
        else if (Element is ExpressionVariable var)
        {
            string llvmType = TypeSystem.GetLLVMTypeName(var.Variable.type);
            output.Append($"\n%__{Scope.OperationIndex} = load {llvmType}, ");
            output.Append($"ptr %{var.Variable.identifier}, align {TypeSystem.Types[var.Variable.type].size}");
            tempToUse = includeType ? $"{llvmType} %__{Scope.OperationIndex++}"
                                    : $"%__{Scope.OperationIndex++}";
        }
        else
        {
            throw new NotImplementedException();
            //output.Append("");
        }
        
        return output.ToString();
    }

    public string ToC()
    {
        StringBuilder output = new(32);
        if (Element is ExpressionBuiltInOp op)
        {
            //output.Append($"{op.ToC()}");
        }
        else throw new NotImplementedException();

        return output.ToString();
    }

    public override string ToString() => ToString(0);
    
    public string ToString(int depth)
    {
        StringBuilder output = new();
        for (int i = 0; i < depth - 1; i++)
        {
            output.Append('\t');
        }

        if (depth > 0) output.Append("└── ");

        output.Append(Element.ToString());
        foreach (ExpressionNode branch in Children)
        {
            output.Append('\n');
            output.Append(branch.ToString(depth + 1));
        }

        return output.ToString();
    }
}

public class ExpressionParser
{
    /*
    Expression Tree Format

    Root (last operation to run)
                /\
               /  \
              /    \
             /      \
            /        \
         More Ops   Of Higher Precedence

    Parsing strategy:
    1) Read right-to-left, storing the operation with the least precedence.
    2) Split the expression into two separate expressions and recurse until finished.
    3) If we encounter parentheses, immediately recurse.
    */
    public static ExpressionNode CreateExpressionTree(List<ExpressionElement> elements, Scope currentScope)
    {
        int lowestPriority = -1;
        int lowestPriorityIndex = 0;
        for (int i = elements.Count - 1; i >= 0; i--)
        {
            int currentPriority = GetOperationPriority(elements[i]);
            if (currentPriority > lowestPriority) 
            {
                lowestPriority = currentPriority;
                lowestPriorityIndex = i;
            }
            
        }
        
        ExpressionNode output = new(elements[lowestPriorityIndex], currentScope);

        if (lowestPriorityIndex > 0)
        {
            List<ExpressionElement> sliceBefore = elements.ToArray()[..(lowestPriorityIndex)].ToList();
            if (sliceBefore.Count > 0) output.Children.Add(CreateExpressionTree(sliceBefore, currentScope));
        }
        else
        {
            //ErrorReporting.CompilerError(elements[0].StartToken, "This version of rhc does not support unary operators");
        }
        
        List<ExpressionElement> sliceAfter = elements.ToArray()[(lowestPriorityIndex + 1)..].ToList();
        if (sliceAfter.Count > 0) output.Children.Add(CreateExpressionTree(sliceAfter, currentScope));
        //else ErrorReporting.CompilerError(elements[0].StartToken, "This version of rhc does not support unary operators");

        return output;
    }

    public static ExpressionOperation GetOperationFromElement(ExpressionElement element) => element.Type switch
    {
        ExpressionElementType.BuiltInOp => (ExpressionOperation)(element as ExpressionBuiltInOp)!.Operator, //We can cast the ExpressionBuiltInOp to an ExpressionOperation because the two types are exactly compatible for that range
        ExpressionElementType.TypeName or ExpressionElementType.Cast => ExpressionOperation.Cast,
        ExpressionElementType.CustomOp => ExpressionOperation.UserDefinedOperator,
        _ => ExpressionOperation.None
    };

    /// <summary>
    /// Defines the precedence of various operations. Note that 0 is the operation that will be done *first*.
    /// -1 indicates that the element does not cause an operation to occur with other elements.
    /// </summary>
    public static int GetOperationPriority(ExpressionOperation op) => op switch
    {
        ExpressionOperation.Add => 3,
        ExpressionOperation.Subtract => 3,
        ExpressionOperation.Multiply => 2,
        ExpressionOperation.Divide => 2,
        ExpressionOperation.Modulus => 2,
        ExpressionOperation.EqualTo => 6,
        ExpressionOperation.NotEqual => 6,
        ExpressionOperation.And => 10,
        ExpressionOperation.Or => 11,
        ExpressionOperation.Not => 1,
        ExpressionOperation.LessThan => 5,
        ExpressionOperation.LessOrEqual => 5,
        ExpressionOperation.GreaterThan => 5,
        ExpressionOperation.GreaterOrEqual => 5,
        ExpressionOperation.LeftShift => 4,
        ExpressionOperation.RightShift => 4,
        ExpressionOperation.MemberAccess => 0,
        ExpressionOperation.FunctionCall => 0,
        ExpressionOperation.ArrayAccess => 0,
        ExpressionOperation.UnaryPlus => 1,
        ExpressionOperation.UnaryMinus => 1,
        ExpressionOperation.AddressOf => 1,
        ExpressionOperation.ValueOfPtr => 1,
        ExpressionOperation.Cast => 1,
        ExpressionOperation.UserDefinedOperator => 15,
        ExpressionOperation.None => -1,
        _ => throw new NotImplementedException()
    };

    public static int GetOperationPriority(ExpressionElement element) => GetOperationPriority(GetOperationFromElement(element));
}

public enum ExpressionOperation
{
    Add,
    Subtract,
    Multiply,
    Divide,
    Modulus,
    EqualTo,
    NotEqual,
    And,
    Or,
    Not,
    LessThan,
    GreaterThan,
    LessOrEqual,
    GreaterOrEqual,
    LeftShift = 17,
    RightShift,
    MemberAccess,
    FunctionCall,
    ArrayAccess,
    UnaryPlus,
    UnaryMinus,
    AddressOf,
    ValueOfPtr,
    Cast,
    UserDefinedOperator,
    None
}

