using System;
using System.Collections.Generic;
using System.Text;

public class IfStatement : Scope
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public RhExpression Expression { get; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public List<STNode> Children { get; }

    public ElseStatement? Else { get; set; }
    public Dictionary<string, RhVariable> Variables { get; }

    public ulong OperationIndex { get; set; }

    public IfStatement(Token startToken, Scope parent, RhExpression expression)
    {
        StartToken = startToken;
        EndToken = null;
        Parent = parent;
        Expression = expression;
        Children = new();
        Else = null;
        Variables = new(Parent.Variables);
        OperationIndex = Parent.OperationIndex + 1;
    }

    public IfStatement(Token startToken, Token endToken, Scope parent, RhExpression expression) : this(startToken, parent, expression)
    {
        EndToken = endToken;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(32);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("if ");
        output.Append(Expression.ToString());
        return output.ToString();
    }

    //Implement later
    /*
    br i1 %condition, label %iftrue, label %iffalse

    iftrue:
        ; code if true
        br label %end
    iffalse:
        ; code if false
        br label %end
    end:
        ; both branches should go here
    */
    public string ToLLVMIR()
    {
        StringBuilder output = new(64);
        output.Append($"; if {Expression.ToString()}\n");
        output.Append(Expression.CodegenLLVMIR(out string expr));
        (int startLabelIndex, int elseLabelIndex, int endLabelIndex) = (Tree.LabelIndex++, Tree.LabelIndex++, Tree.LabelIndex++);
        output.Append($"\nbr {expr}, label %__l{startLabelIndex}, label %__l{elseLabelIndex}");
        output.Append($"\n__l{startLabelIndex}:");
        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToLLVMIR()}");
        }
        output.Append($"\nbr label %__l{endLabelIndex}");

        output.Append($"\n__l{elseLabelIndex}:");
        if (Else is not null)
        {
            if (Else.Children.Count == 0) output.Append("\n");
            foreach (STNode node in Else.Children)
            {
                output.Append($"\n{node.ToLLVMIR()}");
            }
        }
        output.Append($"\nbr label %__l{endLabelIndex}");

        output.Append($"\n__l{endLabelIndex}:");
        
        return output.ToString();
    }

    public string ToC()
    {
        StringBuilder output = new(64);
        //output.Append($"if({Expression.CodegenC()})\n{{");
        throw new NotImplementedException();
    }

    public byte[] ToRhIF()
    {
        List<byte> output = new(32);
        output.AddRange(BitConverter.GetBytes((ushort)8));
        output.AddRange(Expression.ToRhIF());
        return output.ToArray();
    }
}

public class ElseStatement : Scope
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public List<STNode> Children { get; }

    public Dictionary<string, RhVariable> Variables { get; }

    public ulong OperationIndex { get; set; }

    public IfStatement If { get; }

    public ElseStatement(Token startToken, Scope parent, IfStatement @if)
    {
        StartToken = startToken;
        EndToken = startToken;
        Parent = parent;
        Children = new();
        Variables = new(Parent.Variables);
        OperationIndex = Parent.OperationIndex + 1;
        If = @if;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(8);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("else");
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    //Implement later
    //Actually don't implement because the if statement will handle this part
    public string ToLLVMIR() => "";

    public byte[] ToRhIF()
    {
        return BitConverter.GetBytes((ushort)9);
    }
}

public class WhileStatement : Scope
{
    public Scope Parent { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public List<STNode> Children { get; }

    public Dictionary<string, RhVariable> Variables { get; }

    public ulong OperationIndex { get; set; }

    public RhExpression Expression { get; }

    public WhileStatement(Token startToken, Scope parent, RhExpression expression)
    {
        StartToken = startToken;
        Parent = parent;
        Children = new();
        Variables = new(Parent.Variables);
        OperationIndex = Parent.OperationIndex + 1;
        Expression = expression;
    }

    public WhileStatement(Token startToken, Token endToken, Scope parent, RhExpression expression) : this (startToken, parent, expression)
    {
        EndToken = endToken;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(64);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("while ");
        output.Append(Expression.ToString());
        foreach (STNode node in Children)
        {
            output.Append('\n');
            output.Append(node.ToString(depth + 1));
        }

        output.Append('\n');
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("End Scope");
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    public string ToLLVMIR()
    {
        /*
        loop:
            br i1 %condition, label %start, label %end

        start:
            ; code goes here
            br label %loop
        */
        StringBuilder output = new(64);
        output.Append($"; while {Expression.ToString()}");

        output.Append($"\nbr label %__l{Tree.LabelIndex}");
        output.Append($"\n__l{Tree.LabelIndex}:"); //Start label to jump to for loop
        int startLabelIndex = Tree.LabelIndex;
        Tree.LabelIndex++;

        output.Append($"\n{Expression.CodegenLLVMIR(out string expr)}"); //Set up conditional expression

        output.Append($"\nbr {expr}, label %__l{Tree.LabelIndex}, label %__l{Tree.LabelIndex + 1}"); //Main condition test

        output.Append($"\n__l{Tree.LabelIndex}:"); //Label for contents if the loop is run
        Tree.LabelIndex++;
        int endLabelIndex = Tree.LabelIndex++;

        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToLLVMIR()}");
        }

        output.Append($"\nbr label %__l{startLabelIndex}"); //Go back to the beginning to check the condition again

        output.Append($"\n__l{endLabelIndex}:"); //End label for when the loop finishes

        return output.ToString();
    }

    public byte[] ToRhIF() => throw new NotImplementedException();
}
