using System.Text;

public class VariableDeclaration : STNode
{
    public Scope? Parent { get; }

    public List<STNode> Children { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public string Identifier { get; }

    public string Type { get; }

    public RhExpression Value { get; }

    public bool Extern { get; internal set; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public VariableDeclaration(Token startToken, Scope parent, string identifier, string type, RhExpression value, bool rh_extern = false)
    {
        Parent = parent;
        StartToken = startToken;
        EndToken = null;
        Children = new();
        Identifier = identifier;
        Type = type;
        Value = value;
        Extern = rh_extern;
    }

    public VariableDeclaration(Token startToken, Token endToken, Scope parent, string type, string identifier, RhExpression value, bool rh_extern = false)
    : this(startToken, parent, identifier, type, value, rh_extern)
    {
        EndToken = endToken;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new StringBuilder(64);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"decl");
        if (Extern) output.Append($" extern");
        output.Append($" {Type} {Identifier} = {Value.ToString()}");
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    public string ToLLVMIR()
    {
        StringBuilder output = new(64);
        string llvmType = TypeSystem.GetLLVMTypeName(Type);
        ulong typeSize = TypeSystem.Types[Type].size;
        output.Append($"%{Identifier} = alloca {llvmType}, align {typeSize}");
        string expressionPrep = Value.CodegenLLVMIR(out string expression);
        output.Append($"\n{expressionPrep}");
        output.Append($"\nstore {expression}, ptr %{Identifier}, align {typeSize}");
        //alloca {llvmType}, align {typeSize}");
        //output.Append($"\nstore {Value.ToLLVMIR()}, ptr %{Identifier}, align {typeSize}");
        /*if (Type != "stringptr")
        {
            output.Append($"%{Identifier} = alloca {llvmType}, align {typeSize}");
            output.Append($"\nstore {llvmType} {TypeHandler.GetLLVMString(Value)}, ptr %{Identifier}, align {typeSize}");
        }
        else
        {
            int stringIndex = (int)BitConverter.ToUInt64(Value.contents);
            int length = Tree.Strings[stringIndex].contents.Length - 8;
            output.Append($"%{Identifier} = alloca ptr, align 8");
            output.Append($"\nstore ptr @.str.{stringIndex}, ptr %{Identifier}, align 8");
        }*/
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        uint flags = 0;
        if (Extern) flags &= 1 << 29;
        var opcodeBytes = BitConverter.GetBytes((UInt16)4);
        var flagsBytes = BitConverter.GetBytes(flags);
        var typeBytes = BitConverter.GetBytes(TypeSystem.Types[Type].id);
        var identBytes = Utils.MakeRhIFString(Identifier);
        var expressionSizeBytes = BitConverter.GetBytes((long)1);
        var elementTypeBytes = BitConverter.GetBytes((Int16)0);
        var valueBytes = Value.ToRhIF();
        int size = opcodeBytes.Length + flagsBytes.Length + typeBytes.Length + identBytes.Length + expressionSizeBytes.Length + elementTypeBytes.Length
            + typeBytes.Length + valueBytes.Length;
        List<byte> output = new(size);
        output.AddRange(opcodeBytes);
        output.AddRange(flagsBytes);
        output.AddRange(typeBytes);
        output.AddRange(identBytes);
        output.AddRange(expressionSizeBytes);
        output.AddRange(elementTypeBytes);
        output.AddRange(typeBytes);
        output.AddRange(valueBytes);
        return output.ToArray();
    }
}

public class VariableAssignment : STNode
{
    public Scope Parent { get; }

    public List<STNode> Children { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public string Identifier { get; }

    public RhExpression Value { get; }

    public SyntaxTree Tree { get { return Parent!.Tree; } }

    public VariableAssignment(Token startToken, Scope parent, string identifier, RhExpression value)
    {
        Parent = parent;
        StartToken = startToken;
        EndToken = null;
        Children = new();
        Identifier = identifier;
        Value = value;
    }

    public VariableAssignment(Token startToken, Token endToken, Scope parent, string identifier, RhExpression value)
    {
        Parent = parent;
        StartToken = startToken;
        EndToken = endToken;
        Children = new();
        Identifier = identifier;
        Value = value;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new StringBuilder(64);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append($"assign {Identifier} = {Value.ToString()} ({Value.DetermineType().name})");
        return output.ToString();
    }

    public string ToC() => throw new NotImplementedException();

    public string ToLLVMIR()
    {
        string llvmType = TypeSystem.GetLLVMTypeName(Value.DetermineType().name);
        //if (Value.DetermineType().name != "stringptr")
        {
            StringBuilder output = new(32);
            output.Append(Value.CodegenLLVMIR(out string expr));
            output.Append($"\nstore {expr}, {llvmType}, ptr %{Identifier}, align {TypeSystem.Types[Value.DetermineType().name].size}");
            return output.ToString();
        }
        /*else
        {
            int stringIndex = (int)BitConverter.ToUInt64(Value.contents);
            int length = Tree.Strings[stringIndex].contents.Length - 8;
            return $"%{Identifier} = getelementptr [{length + 1} x i8], [{length + 1} x i8], ptr @.str.{stringIndex}, i64 0, i64 0";
        }*/
    }

    public byte[] ToRhIF()
    {
        throw new NotImplementedException();
        /*List<byte> output = new(32);
        output.AddRange(BitConverter.GetBytes((Int16)5));
        output.AddRange(BitConverter.GetBytes((long)1));
        output.AddRange(BitConverter.GetBytes((long)0));
        output.AddRange(BitConverter.GetBytes(TypeSystem.Types[Value.type].id));
        output.AddRange(Value.contents);
        return output.ToArray();*/
    }
}
