using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LLVMSharp;

/// <summary>
/// Interface for syntax tree nodes, used for code generation
/// </summary>
public interface STNode
{
    public Scope? Parent { get; }
    Token StartToken { get; }
    Token? EndToken { get; set; }
    SyntaxTree Tree { get; }

    public string ToString(int depth = 0);

    public string ToLLVMIR();

    public string ToC();

    public byte[] ToRhIF();
}

/// <summary>
/// A syntax tree, representing the post-parse contents of a file
/// </summary>
public class SyntaxTree
{
    public List<RhString> Strings { get; }
    public string File { get; }
    public BasicScope GlobalScope { get; }
    public Dictionary<string, List<RhFunction>> Functions { get; }
    public int LabelIndex { get; internal set; } = 0;

    public static SyntaxTree InvalidTree = new(new("Invalid syntax tree", 0, 0, "Invalid file"));

    public SyntaxTree(Token startToken)
    {
        File = startToken.file;
        GlobalScope = new(startToken, null, this);
        Strings = new();
        Functions = new();
    }

    public override string ToString()
    {
        StringBuilder output = new(1024);
        output.Append($"{File} Syntax Tree\n");
        for (int i = 0; i < Strings.Count; i++)
        {
            output.Append($"literal string {i} = \"{Encoding.UTF8.GetString(Strings[i].contents).ReplaceLineEndings(@"\n")}\"\n");
        }
        output.Append(GlobalScope.ToString());
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        byte[] magicBytes = { 0x52, 0x68, 0x49, 0x46 }; //RhIF identifying characters- "RhIF"
        byte[] versionBytes = BitConverter.GetBytes((uint)0); //Format version
        byte[] fileNameBytes = Utils.MakeRhIFString(File); //File name
        byte[] globalScopeBytes = GlobalScope.ToRhIF(); //Contents
        byte[] sizeBytes = BitConverter.GetBytes((ulong)(magicBytes.Length + versionBytes.Length + 8 + fileNameBytes.Length + globalScopeBytes.Length)); //8 is the size of this array
        List<byte> output = new(2048);
        output.AddRange(magicBytes);
        output.AddRange(versionBytes);
        output.AddRange(sizeBytes);
        output.AddRange(fileNameBytes);
        output.AddRange(globalScopeBytes);
        return output.ToArray();
    }

    public unsafe byte* GetRhIFPtr()
    {
        fixed (byte* output = ToRhIF())
        {
            return output;
        }
    }

    public void AddFunction(FunctionDeclaration decl)
    {
        if (!Functions.ContainsKey(decl.Identifier))
        {
            Functions.Add(decl.Identifier, new());
            Functions[decl.Identifier].Add(decl.Function);
        }
        else
        {
            if (Functions[decl.Identifier].Any(x => TypeSystem.ParamListsSame(x.parameters, decl.Parameters)))
            {
                ErrorReporting.CompilerError(decl.StartToken, "A function with the same name and parameters already exists in this context");
            }
            else
            {
                Functions[decl.Identifier].Add(decl.Function);
            }
        }
    }
}

public interface Scope : STNode
{
    public List<STNode> Children { get; }

    public Dictionary<string, RhVariable> Variables { get; }

    public ulong OperationIndex { get; set; }

    public void AddVariable(Token type, Token identifier, List<Token> value)
    {
        if (Variables.ContainsKey(identifier.text))
        {
            ErrorReporting.CompilerError(identifier, $"The name {identifier.text} already exists in this context, so it cannot be created");
            return;
        }
        RhExpression expr = Parser.ParseExpression(value, this);
        if (!TypeSystem.ResolveTypeAlias(type.text, out string canonicalType))
        {
            ErrorReporting.CompilerError(type, $"Type name {type.text} not found");
            return;
        }
        string exprType = expr.DetermineType().name;
        if (exprType != canonicalType && !TypeSystem.AreTypesCompatible(canonicalType, exprType))
        {
            ErrorReporting.CompilerError(expr.StartToken, $"Could not read value as {type.text}");
        }
        //RhValue rhValue = TypeHandler.ParseValue(type, value);
        Variables.Add(identifier.text, new RhVariable(identifier.text, canonicalType));
        Children.Add(new VariableDeclaration(type, this, identifier.text, canonicalType, expr));
    }

    public void AddFunction(FunctionDeclaration decl)
    {
        //Tree.AddFunction(decl);
        Children.Add(decl);
    }
}

public class BasicScope : Scope
{
    public Scope? Parent { get; }

    public List<STNode> Children { get; }

    public Token StartToken { get; }

    public Token? EndToken { get; set; }

    public Dictionary<string, RhVariable> Variables { get; }

    public ulong OperationIndex { get; set; }

    //public Dictionary<string, List<RhFunction>> Functions { get; }

    public SyntaxTree Tree { get; }

    public BasicScope(Token startToken, Scope? parent)
    {
        StartToken = startToken;
        Parent = parent;
        Children = new();
        if (parent != null)
        {
            Variables = new(parent.Variables); //Each child scope should have access to the variables in its parent, but not the other way around
            //Functions = new(Tree.Functions);
            Tree = parent.Tree;
        }
        else
        {
            Variables = new();
            //Functions = new();
            Tree = SyntaxTree.InvalidTree;
        }
    }

    public BasicScope(Token startToken, Scope? parent, SyntaxTree tree) : this(startToken, parent)
    {
        Tree = tree;
        OperationIndex = 1;
    }

    public string ToString(int depth = 0)
    {
        StringBuilder output = new(128);
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("Scope");
        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToString(depth + 1)}");
        }
        output.Append("\n");
        for (int i = 0; i < depth; i++)
        {
            output.Append('\t');
        }
        output.Append("End Scope");
        return output.ToString();
    }

    public byte[] ToRhIF()
    {
        List<byte> bytes = new(1024);
        bytes.AddRange(BitConverter.GetBytes((UInt16)0)); //Begin scope
        foreach (STNode node in Children)
        {
            bytes.AddRange(node.ToRhIF());
        }
        bytes.AddRange(BitConverter.GetBytes((UInt16)1)); //End scope
        return bytes.ToArray();
    }

    public string ToLLVMIR()
    {
        StringBuilder output = new(128);
        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToLLVMIR()}");
        }
        return output.ToString();
    }

    public string ToC()
    {
        throw new NotSupportedException("The C backend is not complete and not supported.");
        /*
        StringBuilder output = new(128);
        foreach (STNode node in Children)
        {
            output.Append($"\n{node.ToC()}");
        }
        return output.ToString();
        */
    }
}

/*
    Structures the tree needs to represent:
        * Variable declaration
        * Function definition
        * {} for scope
        * () for expressions
        * Variable assignment
        * Function calls, including expressions
        * Operators on variables as well as constants
        * Additional language features to add in future

    Necessary information to represent these things:

    --Variable declaration--
        * Name
        * Type
    
    --Variable assignment--
        * Name
        * Value (type-checked and parsed to the correct type)

    --Function definition--
        * Name
        * Type
        * Changes scope

    --Scope--
        * Different levels of nodes
        * May reference variables and functions from an enclosing scope, but may not reference variables or functions in a parallel or child scope
*/
