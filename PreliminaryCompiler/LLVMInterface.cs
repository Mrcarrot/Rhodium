using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using LLVMSharp;

public class LLVMInterface
{
    //public static NativeLibrary LLVMLibrary = NativeLibrary.Load("");

    public static void InitializeTarget(Target target)
    {
        switch (target.arch)
        {
            case CPUArch.i686 or CPUArch.x86_64:
                LLVM.InitializeX86Target();
                break;

            case CPUArch.aarch64:
                LLVM.InitializeAArch64Target();
                break;

            case CPUArch.arm:
                LLVM.InitializeARMTarget();
                break;
        }
    }

    public static Dictionary<string, string> RhToLLVMTypes = new()
    {
        { "int32", "i32" },
        { "int8", "i8" },
        { "byte", "u8" },
        { "char", "u32" },
        { "char8", "i8"}
    };

    public static int RunLLVMSum(int x, int y)
    {
        LLVMModuleRef mod = LLVM.ModuleCreateWithName("my_module");

        LLVMTypeRef[] param_types = { LLVM.Int32Type(), LLVM.Int32Type() };
        LLVMTypeRef ret_type = LLVM.FunctionType(LLVM.Int32Type(), param_types, false);
        LLVMValueRef sum = LLVM.AddFunction(mod, "sum", ret_type);

        LLVMBasicBlockRef entry = LLVM.AppendBasicBlock(sum, "entry");

        LLVMBuilderRef builder = LLVM.CreateBuilder();
        LLVM.PositionBuilderAtEnd(builder, entry);
        LLVMValueRef tmp = LLVM.BuildAdd(builder, LLVM.GetParam(sum, 0), LLVM.GetParam(sum, 1), "tmp");
        LLVM.BuildRet(builder, tmp);

        {
            LLVM.VerifyModule(mod, LLVMVerifierFailureAction.LLVMAbortProcessAction, out string? error);
            if (error is not null) unsafe
                {
                    byte[] chars = Encoding.UTF8.GetBytes(error);
                    fixed (byte* charPtr = &chars[0])
                        LLVM.DisposeMessage((IntPtr)charPtr);
                }
        }

        LLVM.LinkInMCJIT();
        InitializeTarget(Platform.CurrentTarget);
        {
            if (LLVM.CreateExecutionEngineForModule(out LLVMExecutionEngineRef engine, mod, out string? error))
            {
                Console.Error.WriteLine("Failed to create execution engine");
                throw new Exception("LLVM build failed");
            }
            if (error is not null)
            {
                Console.Error.WriteLine($"Error: {error}");
                unsafe
                {
                    byte[] chars = Encoding.UTF8.GetBytes(error);
                    fixed (byte* charPtr = &chars[0])
                        LLVM.DisposeMessage((IntPtr)charPtr);
                }
                throw new Exception("LLVM build failed");
            }
            LLVMGenericValueRef[] args =
        {
            LLVM.CreateGenericValueOfInt(LLVM.Int32Type(), (ulong)x, true),
            LLVM.CreateGenericValueOfInt(LLVM.Int32Type(), (ulong)y, true)
        };
            LLVMGenericValueRef res = LLVM.RunFunction(engine, sum, args);
            
            return (int)LLVM.GenericValueToInt(res, true);
        }
    }

    /// <summary>
    /// Evaluates whether the provided integer is even, using LLVM JIT.
    /// We have finally found a better implementation.
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public static bool IsEven(int x)
    {
        LLVMModuleRef mod = LLVM.ModuleCreateWithName("my_module");

        LLVMTypeRef[] param_types = { LLVM.Int32Type(), LLVM.Int32Type() };
        LLVMTypeRef ret_type = LLVM.FunctionType(LLVM.Int32Type(), param_types, false);
        LLVMValueRef func = LLVM.AddFunction(mod, "mod", ret_type);

        LLVMBasicBlockRef entry = LLVM.AppendBasicBlock(func, "entry");

        LLVMBuilderRef builder = LLVM.CreateBuilder();
        LLVM.PositionBuilderAtEnd(builder, entry);
        LLVMValueRef tmp = LLVM.BuildSRem(builder, LLVM.GetParam(func, 0), LLVM.GetParam(func, 1), "tmp");
        LLVM.BuildRet(builder, tmp);

        {
            LLVM.VerifyModule(mod, LLVMVerifierFailureAction.LLVMAbortProcessAction, out string? error);
            if (error is not null) unsafe
                {
                    byte[] chars = Encoding.UTF8.GetBytes(error);
                    fixed (byte* charPtr = &chars[0])
                        LLVM.DisposeMessage((IntPtr)charPtr);
                }
        }

        LLVM.LinkInMCJIT();
        InitializeTarget(Platform.CurrentTarget);

        {
            if (LLVM.CreateExecutionEngineForModule(out LLVMExecutionEngineRef engine, mod, out string? error))
            {
                Console.Error.WriteLine("Failed to create execution engine");
                throw new Exception("LLVM build failed");
            }
            if (error is not null)
            {
                Console.Error.WriteLine($"Error: {error}");
                unsafe
                {
                    byte[] chars = Encoding.UTF8.GetBytes(error);
                    fixed (byte* charPtr = &chars[0])
                        LLVM.DisposeMessage((IntPtr)charPtr);
                }
                throw new Exception("LLVM build failed");
            }
            LLVMGenericValueRef[] args =
            {
                LLVM.CreateGenericValueOfInt(LLVM.Int32Type(), (ulong)x, false),
                LLVM.CreateGenericValueOfInt(LLVM.Int32Type(), (ulong)2, false)
            };
            LLVMGenericValueRef res = LLVM.RunFunction(engine, func, args);
            return LLVM.GenericValueToInt(res, false) == 0;
        }
    }
}