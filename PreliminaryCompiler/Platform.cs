using System;
using System.Text;
using System.Runtime.InteropServices;

public record Target(CPUArch arch, Vendor vendor, OS os);

public class Platform
{
    public static Target CurrentTarget { get; private set; } = NativeTarget();

    public static string GetTriple(Target target)
    {
        StringBuilder output = new(32);

        output.Append(target.arch switch
        {
            CPUArch.i686 => "i686",
            CPUArch.x86_64 => "x86_64",
            CPUArch.arm => "arm",
            CPUArch.aarch64 => "aarch64",
            _ => throw new Exception($"Unsupported target architecture: {target.arch}")
        });

        output.Append(target.vendor switch
        {
            Vendor.Apple => "-apple",
            Vendor.PC => "-pc",
            Vendor.Unknown => "-unknown",
            Vendor.Alpine => "-alpine",
            Vendor.None => "",
            _ => throw new Exception($"Unsupported target vendor: {target.vendor}")
        });

        output.Append(target.os switch
        {
            OS.GNULinux => target.arch == CPUArch.arm ? "-linux-gnueabihf" : "-linux-gnu",
            OS.MUSLLinux => "-linux-musl",
            OS.Windows => "-windows-msvc",
            OS.WindowsGNU => "-windows-gnu",
            OS.Darwin => "-darwin",
            _ => throw new Exception($"Unsupported target operating system: {target.os}")
        });

        return output.ToString();
    }

    public static Target NativeTarget()
    {
        CPUArch arch = RuntimeInformation.ProcessArchitecture switch
        {
            Architecture.X86 => CPUArch.i686,
            Architecture.X64 => CPUArch.x86_64,
            Architecture.Arm64 => CPUArch.aarch64,
            Architecture.Arm or Architecture.Armv6 => CPUArch.arm,
            _ => throw new Exception($"Unsupported target architecture: {RuntimeInformation.ProcessArchitecture}")
        };

        OS os = OS.None;
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
        {
            os = OS.GNULinux;
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
        {
            os = OS.MacOS;
        }

        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            os = OS.Windows;
        }

        Vendor vendor = os switch
        {
            OS.GNULinux or OS.Windows => Vendor.PC,
            OS.MUSLLinux => Vendor.Alpine,
            OS.Darwin => Vendor.Apple,
            _ => Vendor.Unknown
        };
        if (arch is CPUArch.arm or CPUArch.aarch64) vendor = Vendor.None;

        return new(arch, vendor, os);
    }

    public static Target ParseTriple(string triple)
    {
        string[] sections = triple.Split('-'); //Split the triple out into several sections, which we can then parse
        CPUArch architecture = sections[0] switch //This one is the easiest to parse
        {
            "x86_64" => CPUArch.x86_64,
            "i686" => CPUArch.i686,
            "arm" => CPUArch.arm,
            "aarch64" => CPUArch.aarch64,
            _ => CPUArch.Unknown
        };

        Vendor vendor = Vendor.None;
        OS os = OS.None;

        for (int i = 1; i < sections.Length; i++)
        {
            if (sections[i] == "apple") vendor = Vendor.Apple;
            if (sections[i] == "pc") vendor = Vendor.PC;
            if (sections[i] == "alpine") vendor = Vendor.Alpine;
            if (sections[i] == "unknown") vendor = Vendor.Unknown;

            if (sections[i] == "linux")
            {
                i++;
                if (i == sections.Length) os = OS.UnknownLinux;
                else
                {
                    if (sections[i].StartsWith("gnu")) os = OS.GNULinux;
                    if (sections[i] == "musl") os = OS.MUSLLinux;
                }
            }

            if (sections[i].StartsWith("darwin")) os = OS.Darwin;

            if (sections[i] == "windows")
            {
                i++;
                if (i == sections.Length) os = OS.Windows;
                else
                {
                    if (sections[i].StartsWith("msvc")) os = OS.WindowsMSVC;
                    if (sections[i] is "gnu") os = OS.WindowsGNU;
                }
            }
        }
        return new(architecture, vendor, os);
    }

    public static void SetTarget(Target target)
    {
        CurrentTarget = target;
    }

    public static ulong GetPointerSize() =>
    CurrentTarget.arch switch
    {
        CPUArch.arm or CPUArch.i686 => 4,
        CPUArch.aarch64 or CPUArch.x86_64 => 8,
        _ => 4
    };

    public static void InitializeTargetInfo()
    {
        ulong pointerSize = GetPointerSize();
        TypeSystem.Types.Add("stringptr", new("stringptr", pointerSize, 273, TypeSystem.TypeClass.IntConvertible));
        TypeSystem.Types.Add("opaqueptr", new("opaqueptr", pointerSize, 274, TypeSystem.TypeClass.IntConvertible));
        TypeSystem.Types.Add("intptr", new("intptr", pointerSize, 276, TypeSystem.TypeClass.SInteger));
        TypeSystem.Types.Add("uintptr", new("uintptr", pointerSize, 277, TypeSystem.TypeClass.UInteger));
    }
}

public enum CPUArch
{
    i686,
    x86_64,
    arm,
    aarch64,
    Unknown
}

public enum Vendor
{
    PC,
    Apple,
    Alpine,
    Unknown,
    None
}

public enum OS
{
    GNULinux,
    MUSLLinux,
    UnknownLinux,
    Darwin,
    MacOS = Darwin,
    WindowsMSVC,
    Windows = WindowsMSVC,
    WindowsGNU,
    None
}