public class RhCompilerException : Exception
{
    public string File { get; }
    public int Line { get; }
    public int Col { get; }
    public override string Message { get; }

    public RhCompilerException(string file, int line, int col, string message)
    {
        File = file;
        Line = line;
        Col = col;
        Message = message;
    }

    public RhCompilerException(Token token, string message)
    {
        File = token.file;
        Line = token.line;
        Col = token.col;
        Message = message;
    }
}