using System;
using System.Collections.Generic;
using System.Text;

public record StatementGroup(List<List<Token>> statements, List<StatementGroup>? children = null);

public class Parser
{
    public enum Keyword
    {
        Rh_using,
        Rh_for,
        Rh_while,
        Rh_if,
        Rh_extern,
        Rh_typedef,
        Rh_sizeof
    }

    public static Dictionary<string, Keyword> Keywords = new()
    {
        { "using", Keyword.Rh_using },
        { "extern", Keyword.Rh_extern }, //we literally need extern to compile hello world
        { "typedef", Keyword.Rh_typedef }, //might as well because it's easy enough
        { "sizeof", Keyword.Rh_sizeof }
    };

    public static Dictionary<string, BuiltInOperator> BuiltInOperators = new()
    {
        {"+", BuiltInOperator.Add},
        {"-", BuiltInOperator.Subtract},
        {"*", BuiltInOperator.Multiply},
        {"/", BuiltInOperator.Divide},
        {"%", BuiltInOperator.Modulus},
        {"==", BuiltInOperator.EqualTo},
        {"!=", BuiltInOperator.NotEqual},
        {"&&", BuiltInOperator.And},
        {"||", BuiltInOperator.Or},
        {"!", BuiltInOperator.Not},
        {"<", BuiltInOperator.LessThan},
        {">", BuiltInOperator.GreaterThan},
        {"<=", BuiltInOperator.LessOrEqual},
        {">=", BuiltInOperator.GreaterOrEqual},
        {"&", BuiltInOperator.And},
        {"|", BuiltInOperator.Or},
        {"^", BuiltInOperator.Not},
        {"<<", BuiltInOperator.LeftShift},
        {">>", BuiltInOperator.RightShift},
    };

    public static SyntaxTree? ProcessTokens(List<Token> input)
    {
        try
        {
            SyntaxTree output = new SyntaxTree(input[0]);
            Scope currentScope = output.GlobalScope;
            Token previousToken, currentToken;
            int i = 1;

            bool rh_extern = false;
            bool uncheckedParams = false;

            void resetFlags()
            {
                rh_extern = false;
                uncheckedParams = false;
            }

            /*void readFlags()
            {
                while (true)
                {
                    switch (Keywords[currentToken.text])
                    {
                        case (Keyword.Rh_extern):
                            rh_extern = true;
                            break;

                        default:
                            ErrorReporting.CompilerError(currentToken, $"Invalid token preceding variable or function: {currentToken.text}");
                            break;
                    }
                    nextToken();
                    if (!Keywords.ContainsKey(currentToken.text)) break;
                }
            }*/

            bool nextToken()
            {
                previousToken = currentToken;
                i++;
                if (i == input.Count)
                {
                    ErrorReporting.CompilerError(currentToken, "Unexpected end of file");
                    return false;
                }
                currentToken = input[i];
                return true;
            }

            /* void expectSemicolon()
            {
                nextToken();
                if (currentToken.text != ";") ErrorReporting.CompilerError(currentToken, $"Expected semicolon but got {currentToken.text}");
            } */

            List<Token> readStatement()
            {
                List<Token> output = new(16);
                while (currentToken.text != "{" && currentToken.text != "}")
                {
                    if (currentToken.text == "" && output.Count > 0) //The only blank tokens are SOF and EOF
                    {
                        ErrorReporting.CompilerError(currentToken, $"Expected end of statement but got end of file instead");
                    }
                    output.Add(currentToken);
                    if (currentToken.text == ";") break;
                    if (i < input.Count - 1)
                        nextToken();
                    else break;
                }
                return output;
            }

            StatementGroup readCompoundStatement()
            {
                if (currentToken.text == "{")
                    nextToken();
                List<List<Token>> output = new(16);
                List<StatementGroup> children = new();

                while (currentToken.text != "}")
                {
                    var statement = readStatement();
                    if (statement.Count > 0)
                        output.Add(statement);

                    if (currentToken.text == "{")
                    {
                        children.Add(readCompoundStatement());
                    }
                    nextToken();
                }

                return new(output, children.Count > 0 ? children : null);
            }

            List<Token> currentStatement = new();
            List<List<Token>> statements = new();
            for (; i < input.Count; i++) 
            {
                currentToken = input[i];
                statements.Add(readStatement());
            }
            i = 1;

            foreach (List<Token> statement in statements)
            {
                for (int j = 0; j < statement.Count; j++)
                {
                    int k = j;
                    if (statement[j].text == "(" && j > 0 && Utils.IsValidIdentifier(statement[j - 1].text))
                    {
                        k--;
                        Token identifier = statement[k];
                        if (!Utils.IsValidIdentifier(identifier.text))
                        {
                            ErrorReporting.CompilerError(identifier, $"Invalid identifier: {identifier.text}");
                        }
                        if (k > 0 && Utils.IsValidIdentifier(statement[k - 1].text, true))
                        {
                            //Function declaration
                            k--;
                            Token type = statement[k];
                            RhType functionType = TypeSystem.GetRhType(type);
                            //Handle keyword flags
                            while (k > 0)
                            {
                                k--;
                                switch (statement[k].text)
                                {
                                    case ("extern"):
                                        rh_extern = true;
                                        break;

                                    case ("unchecked"):
                                        ErrorReporting.CompilerWarning(statement[k], "Unchecked functions may cause runtime errors- use with caution");
                                        uncheckedParams = true;
                                        break;

                                    default:
                                        ErrorReporting.CompilerError(statement[k], $"Unrecognized token in function declaration: {statement[k].text}");
                                        break;
                                }
                            }

                            k = j + 1;

                            List<RhVariable> parameters = new();

                            while (statement[k].text != ")")
                            {
                                RhType paramType = TypeSystem.GetRhType(statement[k]);
                                k++;
                                if (!Utils.IsValidIdentifier(statement[k].text))
                                    ErrorReporting.CompilerError(statement[k], $"Expected parameter name but got {statement[k].text}");
                                parameters.Add(new(statement[k].text, paramType.name));
                                k++;
                                if (statement[k].text == ")") break;
                                if (statement[k].text != ",") ErrorReporting.ExpectError(statement[k], "`,` and next parameter or `)`");
                                k++;
                            }
                            RhFunction function = new(identifier.text, type.text, parameters, uncheckedParams: uncheckedParams);
                            //FunctionDeclaration declaration = new(currentStatement[0], currentScope, identifier.text, function, rh_extern, uncheckedParams);

                            if (output.Functions.TryGetValue(function.identifier, out List<RhFunction>? flist))
                            {
                                flist.Add(function);
                            }
                            else
                            {
                                output.Functions.Add(function.identifier, new());
                                output.Functions[function.identifier].Add(function);
                            }
                        }
                    }
                    else if (statement[j].text == "typedef")
                    {
                        k = j + 1;
                        try
                        {
                            Token newTypeName = statement[k];
                            k++;
                            string baseTypeName = TypeSystem.GetRhType(statement[k]).name;
                            if (TypeSystem.TypeAliases.ContainsKey(newTypeName.text) || TypeSystem.Types.ContainsKey(newTypeName.text))
                            {
                                ErrorReporting.CompilerWarning(newTypeName, $"Redefinition of existing type {newTypeName.text}");
                            }
                            if (!TypeSystem.TypeAliases.ContainsKey(newTypeName.text))
                            {
                                TypeSystem.TypeAliases.Add(newTypeName.text, baseTypeName);
                            }
                            else
                            {
                                TypeSystem.TypeAliases[newTypeName.text] = baseTypeName;
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            ErrorReporting.CompilerError(statement.Last(), "Unexpected end of statement");
                        }
                        j = k;
                    }
                }
            }
            //StatementGroup? currentMultiStatement = null;

            for (; i < input.Count; i++)
            {
                currentToken = input[i];

                currentStatement = readStatement();

                for (int j = 0; j < currentStatement.Count; j++)
                {
                    int k = j;
                    if (currentStatement[j].text == "=")
                    {
                        //Variable declaration or assignment
                        k--;
                        Token identifier = currentStatement[k];
                        if (!Utils.IsValidIdentifier(identifier.text))
                        {
                            ErrorReporting.CompilerError(identifier, $"Invalid identifier: {identifier.text}");
                        }
                        if (k > 0)
                        {
                            //Treat as declaration
                            k--;
                            Token type = currentStatement[k];
                            TypeSystem.GetRhType(type);
                            k--;
                            while (k >= 0)
                            {
                                switch (currentStatement[k].text)
                                {
                                    case ("extern"):
                                        rh_extern = true;
                                        break;

                                    default:
                                        ErrorReporting.CompilerError(currentStatement[k], $"Unknown modifier preceding variable declaration: {currentStatement[k].text}");
                                        break;
                                }
                                k--;
                            }
                            try
                            {
                                k = j + 1;
                                List<Token> expression = new();
                                while (currentStatement[k].text != ";")
                                {
                                    expression.Add(currentStatement[k]);
                                    k++;
                                }
                                //RhValue value = TypeHandler.ParseValue(type, currentStatement[k]);
                                //k++;
                                if (currentStatement[k].text == ";")
                                {
                                    /*if (type.text == "stringptr")
                                    {
                                        output.Strings.Add(new RhString(output.Strings.Count, value.contents));
                                        value = new("stringptr", BitConverter.GetBytes((ulong)output.Strings.Count - 1));
                                    }*/
                                    //currentScope.AddVariable(type, identifier, currentStatement[k - 1]);
                                    //VariableDeclaration declaration = new(currentStatement[0], currentStatement[k], currentScope, identifier.text, type.text, ParseExpression(expression, currentScope), rh_extern);
                                    currentScope.AddVariable(type, identifier, expression);
                                    //currentScope.Variables.Add(declaration.Identifier, new(declaration.Identifier, declaration.Type));
                                    goto endStatementLoop;
                                }
                                else
                                {
                                    ErrorReporting.CompilerError(currentStatement[k], $"Unexpected token: {currentStatement[k].text}");
                                }
                            }
                            catch (IndexOutOfRangeException)
                            {
                                ErrorReporting.CompilerError(currentStatement[k - 1], "Unexpected end of statement");
                            }
                        }
                        else
                        {
                            k = j + 1;
                            List<Token> expression = new();
                            for (; k < currentStatement.Count && currentStatement[k].text != ";"; k++)
                            {
                                expression.Add(currentStatement[k]);
                            }
                            
                            if (currentStatement[k].text == ";")
                            {
                                RhExpression expr = ParseExpression(expression, currentScope);
                                //RhValue value = TypeSystem.ParseValue(new(currentScope.Variables[identifier.text].type, 0, 0, output.File), currentStatement[k]);
                                VariableAssignment assignment = new(currentStatement[0], currentScope, identifier.text, expr);
                            }
                            else
                            {
                                ErrorReporting.ExpectError(currentStatement[k], ";");
                            }
                        }
                    }
                    else if (currentStatement[j].text == "(" && j > 0 && Utils.IsValidIdentifier(currentStatement[j - 1].text))
                    {
                        //Function declaration or call
                        k--;
                        Token identifier = currentStatement[k];
                        if (!Utils.IsValidIdentifier(identifier.text))
                        {
                            ErrorReporting.CompilerError(identifier, $"Invalid identifier: {identifier.text}");
                        }
                        if (k > 0 && Utils.IsValidIdentifier(currentStatement[k - 1].text, true))
                        {
                            //Function declaration
                            k--;
                            Token type = currentStatement[k];
                            RhType functionType = TypeSystem.GetRhType(type);
                            //Handle keyword flags
                            while (k > 0)
                            {
                                k--;
                                switch (currentStatement[k].text)
                                {
                                    case ("extern"):
                                        rh_extern = true;
                                        break;

                                    case ("unchecked"):
                                        //The warning used to come from here but we pre-parse every function declaration now so this would just print it twice
                                        //ErrorReporting.CompilerWarning(currentStatement[k], "Unchecked functions may cause runtime errors- use with caution");
                                        uncheckedParams = true;
                                        break;

                                    default:
                                        ErrorReporting.CompilerError(currentStatement[k], $"Unrecognized token in function declaration: {currentStatement[k].text}");
                                        break;
                                }
                            }

                            k = j + 1;

                            List<RhVariable> parameters = new();

                            while (currentStatement[k].text != ")")
                            {
                                RhType paramType = TypeSystem.GetRhType(currentStatement[k]);
                                k++;
                                if (!Utils.IsValidIdentifier(currentStatement[k].text))
                                    ErrorReporting.CompilerError(currentStatement[k], $"Expected parameter name but got {currentStatement[k].text}");
                                parameters.Add(new(currentStatement[k].text, paramType.name));
                                k++;
                                if (currentStatement[k].text == ")") break;
                                if (currentStatement[k].text != ",") ErrorReporting.ExpectError(currentStatement[k], "`,` and next parameter or `)`");
                                k++;
                            }
                            RhFunction function = new(identifier.text, type.text, parameters, uncheckedParams: uncheckedParams);
                            FunctionDeclaration declaration = new(currentStatement[0], currentScope, identifier.text, function, rh_extern, uncheckedParams);
                            currentScope.AddFunction(declaration);
                            //currentScope.Children.Add(declaration);
                            k++;
                            if (k < currentStatement.Count && currentStatement[k].text == ";")
                            {
                                if (!rh_extern)
                                    ErrorReporting.CompilerError(currentStatement[k], "Functions not marked extern must declare a body");
                            }
                            else if (currentToken.text != "{") ErrorReporting.ExpectError(currentToken, "function body or `;`");
                        }
                        else
                        {
                            //Function call logic
                            j++;
                            List<RhExpression> parameters = new();
                            int nestingLevel = 1;
                            while (j < currentStatement.Count && (currentStatement[j].text != ")" || nestingLevel > 1))
                            {
                                
                                List<Token> expressionElements = new();
                                do
                                {
                                    if (currentStatement[j].text != "," || nestingLevel > 1)
                                        expressionElements.Add(currentStatement[j]);
                                    if (currentStatement[j].text == "(") nestingLevel++;
                                    if (currentStatement[j].text == ")") nestingLevel--;
                                    j++;
                                } while (j < currentStatement.Count && (currentStatement[j].text != "," && currentStatement[j].text != ")" || nestingLevel > 1));
                                
                                RhExpression expression = ParseExpression(expressionElements, currentScope);
                                parameters.Add(expression);
                                expressionElements = new();
                            }
                            if (currentScope.Tree.Functions.TryGetValue(identifier.text, out List<RhFunction>? result))
                            {
                                if (result.Any(x => x.parameters.Count == parameters.Count || x.uncheckedParams))
                                {
                                    var call = new FunctionCall(currentScope, currentStatement[0], result.First(x => x.parameters.Count == parameters.Count || x.uncheckedParams));
                                    foreach (RhExpression expr in parameters)
                                    {
                                        call.Parameters.Add(expr);
                                    }
                                    currentScope.Children.Add(call);
                                }
                                else
                                {
                                    ErrorReporting.CompilerError(identifier, $"There is no overload for function {identifier.text} that takes the given set of parameters");
                                }
                            }
                            else
                            {
                                ErrorReporting.CompilerError(identifier, $"Function with name {identifier.text} does not exist in this context");
                            }
                            //new FunctionCall(currentScope, currentStatement[0], currentScope.Functions[identifier.text].First(x => TypeHandler.ParamListsSame()))
                        }
                    }
                    else if (currentStatement[j].text == "if")
                    {
                        k++;
                        List<Token> expression = new();
                        k = j + 1;
                        while (k != currentStatement.Count)
                        {
                            expression.Add(currentStatement[k]);
                            k++;
                        }
                        IfStatement statement = new(currentStatement[j], currentScope, ParseExpression(expression, currentScope));
                        currentScope.Children.Add(statement);
                        //currentToken should be the first thing after the if statement
                        if (currentToken.text != "{")
                            ErrorReporting.CompilerError(currentToken, "An if statement must be followed by {");
                        //currentScope = statement;
                        j = k;
                    }
                    else if (currentStatement[j].text == "else")
                    {
                        if (currentScope.Children[^1] is IfStatement ifStatement)
                        {
                            ElseStatement elseStatement = new(currentStatement[j], currentScope, ifStatement);
                            ifStatement.Else = elseStatement;
                            if (currentToken.text != "{")
                                ErrorReporting.ExpectError(currentToken, "{");
                            currentScope.Children.Add(elseStatement);
                        }
                        else
                        {
                            ErrorReporting.CompilerError(currentStatement[j], "An else statement must directly follow an if statement");
                        }
                    }
                    else if (currentStatement[j].text == "while")
                    {
                        k++;
                        List<Token> expression = new();
                        k = j + 1;
                        while (k != currentStatement.Count)
                        {
                            expression.Add(currentStatement[k]);
                            k++;
                        }
                        WhileStatement statement = new(currentStatement[j], currentScope, ParseExpression(expression, currentScope));
                        currentScope.Children.Add(statement);
                        //currentToken should be the first thing after the if statement
                        if (currentToken.text != "{")
                            ErrorReporting.ExpectError(currentToken, "{");
                        //currentScope = statement;
                        j = k;
                    }
                    else if (currentStatement[j].text == "exit")
                    {
                        k++;
                        if (currentStatement[k].text == ";")
                        {
                            ExitCall exitCall = new(currentStatement[j], currentStatement[k], currentScope, 0);
                            currentScope.Children.Add(exitCall);
                        }
                        else
                        {
                            if (int.TryParse(currentStatement[k].text, out int exitCode))
                            {
                                k++;
                                if (currentStatement[k].text == ";")
                                {
                                    ExitCall exitCall = new(currentStatement[j], currentStatement[k], currentScope, exitCode);
                                    currentScope.Children.Add(exitCall);
                                }
                                else
                                {
                                    ErrorReporting.CompilerError(currentStatement[k], $"Unexpected token: {currentStatement[k].text} (expected semicolon)");
                                }
                            }
                            else
                            {
                                ErrorReporting.CompilerError(currentStatement[k], $"Could not read exit code `{currentStatement[k].text}` as int32");
                            }
                        }
                        j = k;
                    }
                    else if (currentStatement[j].text == "return")
                    {
                        k = j + 1;
                        if (currentStatement[k].text == ";")
                        {
                            currentScope.Children.Add(new ReturnStatement(currentStatement[j], currentScope));
                        }
                        else
                        {
                            List<Token> expression = new();
                            while (currentStatement[k].text != ";" && k < currentStatement.Count)
                            {
                                expression.Add(currentStatement[k]);
                                k++;
                            }
                            RhExpression rhExpression = ParseExpression(expression, currentScope);
                            currentScope.Children.Add(new ReturnStatement(currentStatement[j], currentScope, rhExpression));
                        }
                        j = k;
                    }
                    resetFlags();
                }
            endStatementLoop:


                if (currentToken.text == "{")
                {
                    if (currentScope.Children[^1] is Scope scope)
                    {
                        currentScope = scope;
                    }
                    else
                    {
                        Scope newScope = new BasicScope(currentToken, currentScope);
                        currentScope.Children.Add(newScope);
                        currentScope = newScope;
                    }
                }

                if (currentToken.text == "}")
                {
                    currentScope.EndToken = currentToken;
                    currentScope = currentScope.Parent!;
                }

                // if (Keywords.ContainsKey(currentToken.text))
                // {
                //     switch (Keywords[currentToken.text])
                //     {
                //         /*case (Keyword.Rh_if):
                //             {

                //                 break;
                //             }*/

                //         case (Keyword.Rh_extern):
                //             rh_extern = true;
                //             break;

                //         //Maybe integrate this into the syntax tree or something
                //         //But also maybe not because it only exists at the Rhodium level and there's no reason to pass this on to LLVM
                //         case (Keyword.Rh_typedef):
                //             nextToken();
                //             Token newTypeName = currentToken;
                //             nextToken();
                //             string baseTypeName = TypeHandler.GetRhType(currentToken).name;
                //             if (TypeHandler.TypeAliases.ContainsKey(newTypeName.text) || TypeHandler.Types.ContainsKey(newTypeName.text))
                //                 ErrorReporting.CompilerWarning(newTypeName, $"Redefinition of existing type {newTypeName.text}");
                //             if (!TypeHandler.TypeAliases.ContainsKey(newTypeName.text))
                //                 TypeHandler.TypeAliases.Add(newTypeName.text, baseTypeName);
                //             else
                //             {
                //                 TypeHandler.TypeAliases[newTypeName.text] = baseTypeName;
                //             }
                //             expectSemicolon();
                //             currentScope.Children.Add(new TypeDefinition(newTypeName, currentToken, currentScope, newTypeName.text, baseTypeName));
                //             break;

                //         default:
                //             ErrorReporting.CompilerError(currentToken.file, currentToken.line, currentToken.col, $"Keyword {currentToken.text} is not implemented");
                //             break;
                //     }
                // }
                // else if (TypeHandler.IsTypeName(currentToken.text))
                // {
                //     Token type = currentToken;
                //     Token? identifier = null;
                //     try
                //     {
                //         nextToken();
                //         if (Utils.IsValidIdentifier(currentToken.text))
                //         {
                //             identifier = currentToken;
                //         }
                //         else
                //         {
                //             ErrorReporting.CompilerError(currentToken, $"Invalid variable or function name: {currentToken.text}");
                //         }

                //         nextToken();

                //         if (currentToken.text == "(")
                //         {
                //             List<RhVariable> parameters = new();
                //             //Function definition logic
                //             nextToken();
                //             if (currentToken.text == ")")
                //             {
                //                 if (identifier != null)
                //                     if (!currentScope.Functions.ContainsKey(identifier.text) && !currentScope.Variables.ContainsKey(identifier.text))
                //                     {
                //                         currentScope.AddFunction(type, identifier, parameters);
                //                         Console.WriteLine($"Function declared with type {type.text}, identifier {identifier.text}, and no parameters.");
                //                     }
                //                     else
                //                     {
                //                         ErrorReporting.CompilerError(identifier, $"The name {identifier.text} already exists in this context");
                //                     }
                //             }
                //             else
                //             {
                //             param:
                //                 if (TypeHandler.IsTypeName(currentToken.text, true))
                //                 {
                //                     string paramType = currentToken.text;
                //                     nextToken();
                //                     string paramName = currentToken.text;
                //                     if (Utils.IsValidIdentifier(paramName))
                //                     {
                //                         parameters.Add(new(paramName, paramType));
                //                     }
                //                     else ErrorReporting.CompilerError(currentToken, $"{paramName} is not a valid parameter name");

                //                     nextToken();
                //                     if (currentToken.text == ",")
                //                     {
                //                         nextToken();
                //                         goto param;
                //                     }
                //                 }
                //                 else
                //                 {
                //                     ErrorReporting.CompilerError(currentToken, $"Expected type of parameter but got {currentToken.text} instead");
                //                 }
                //                 if (identifier != null)
                //                 {
                //                     currentScope.AddFunction(type, identifier, parameters);
                //                     Console.WriteLine($"Function declared with type {type.text}, identifier {identifier.text}, and the following parameters:");
                //                 }
                //                 foreach (RhVariable variable in parameters)
                //                 {
                //                     //Console.WriteLine($"{variable.type} {variable.identifier}");
                //                 }
                //             }
                //             nextToken();
                //             switch (currentToken.text)
                //             {
                //                 case ("{"):
                //                     currentScope = (Scope)currentScope.Children.Last();
                //                     break;

                //                 case (";"):
                //                     if (rh_extern)
                //                     {
                //                         resetFlags();
                //                         break;
                //                     }
                //                     else ErrorReporting.CompilerError(currentToken, "Functions not marked extern must declare a body");
                //                     break;

                //                 default:
                //                     ErrorReporting.CompilerError(currentToken, $"Unexpected token: {currentToken.text}(Expected function body after declaration)");
                //                     break;
                //             }
                //         }
                //         else
                //         {
                //             //Console.WriteLine($"Variable declared with type {type.text} and identifier {identifier.text}");

                //             if (currentToken.text == "=")
                //             {
                //                 nextToken();
                //                 if (identifier != null)
                //                 {
                //                     Console.WriteLine($"Variable {identifier.text} is being set to {currentToken.text}");
                //                     currentScope.AddVariable(type, identifier, currentToken);
                //                 }
                //                 expectSemicolon();
                //                 resetFlags();
                //             }
                //             else if (currentToken.text == ";")
                //             {
                //                 ErrorReporting.CompilerError(currentToken, "All variables must be initialized with a value");
                //             }
                //             else
                //             {
                //                 ErrorReporting.CompilerError(currentToken, $"Unexpected token: {currentToken.text}");
                //             }
                //         }
                //     }
                //     catch (IndexOutOfRangeException)
                //     {
                //         ErrorReporting.CompilerError(currentToken.file, currentToken.line, currentToken.col, "Incomplete variable or function definition");
                //     }
                // }
                // else if (currentScope.Functions.ContainsKey(currentToken.text))
                // {
                //     ErrorReporting.CompilerError(currentToken.file, currentToken.line, currentToken.col, "Function calls are not implemented");
                // }
                // else if (currentScope.Variables.ContainsKey(currentToken.text))
                // {
                //     Token identifier = currentToken;
                //     nextToken();
                //     switch (currentToken.text)
                //     {
                //         case ("="):
                //             nextToken();
                //             RhValue assignValue = TypeHandler.ParseValue(new(currentScope.Variables[identifier.text].type, 0, 0, output.File), currentToken);
                //             Console.WriteLine($"Variable {identifier.text} is being set to {TypeHandler.GetString(assignValue)}");
                //             expectSemicolon();
                //             currentScope.Children.Add(new VariableAssignment(identifier, currentToken, currentScope, identifier.text, assignValue));
                //             break;

                //         default:
                //             ErrorReporting.CompilerError(currentToken, $"Unexpected token: {currentToken.text}");
                //             break;
                //     }

                //     //ErrorReporting.CompilerError(currentToken.file, currentToken.line, currentToken.col, "Variable operations are not implemented");
                // }
                // else if (currentToken.text == "{")
                // {
                //     Scope newScope = new Scope(currentToken, currentScope);
                //     currentScope.Children.Add(newScope);
                //     currentScope = newScope;
                //     /* https://en.wikipedia.org/wiki/Scope_(computer_science)
                //     uint sum_of_squares(uint N)
                //     {
                //         uint ret = 0;
                //         for (uint n = 1; n <= N; n++)
                //         {
                //             uint n_squared = n * n;
                //             ret += n_squared;
                //         }
                //         return ret;
                //     }
                //     Console.WriteLine(sum_of_squares(69));*/
                // }
                // else if (currentToken.text == "}")
                // {
                //     currentScope.EndToken = currentToken;
                //     currentScope = currentScope.Parent ?? output.GlobalScope; //In practice, currentScope.Parent should never be null, but we do this just to be sure
                //     /*uint sum_of_squares(uint N)
                //     {
                //         uint ret = 0;
                //         for (uint n = 1; n <= N; n++)
                //         {
                //             uint n_squared = n * n;
                //             ret += n_squared;
                //         }
                //         return ret;
                //     }
                //     Console.WriteLine(sum_of_squares(420));*/
                // }
                // else if (currentToken.text == ";")
                // {
                //     resetFlags();
                //     int main()
                //     {
                //         int x, y;
                //         x = 40; y = 2;
                //         Console.WriteLine("X + Y = {0}", x + y);
                //         return 0;
                //     }
                //     main();
                // }
                // else if (currentToken.text.StartsWith('"'))
                // {
                //     //The only valid token that starts with " ends with "- otherwise it causes a lexer error
                //     string literalContents = currentToken.text.Substring(1, currentToken.text.Length - 2);
                //     output.Strings.Add(new(output.Strings.Count, Encoding.UTF8.GetBytes(literalContents)));
                // }
                // else
                // {
                //     ErrorReporting.CompilerError(currentToken, $"The name {currentToken.text} does not exist in this context");
                // }

                previousToken = currentToken;
            }
            if (ErrorReporting.Errors > 0)
            {
                return SyntaxTree.InvalidTree;
            }
            return output;
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine(e.ToString());
            ErrorReporting.CompilerError(input.Last(), "Unexpected end of file");
            return null;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            return null;
        }
    }

    public static RhExpression ParseExpression(List<Token> expresssion, Scope currentScope)
    {
        foreach(Token tok in expresssion)
        {
            Console.Write($"{tok.text} ");
        }
        Console.WriteLine();
        if (expresssion.Count == 0) return RhExpression.Empty(new("", 0, 0, ""), currentScope);
        List<ExpressionElement> elements = new();
        Token currentToken = expresssion[0];
        int i = 0;
        bool nextToken()
        {
            if (i >= expresssion.Count - 1) return false;
            i++;
            currentToken = expresssion[i];
            return true;
        }
        
        bool prevToken()
        {
            if (i <= 0) return false;
            i--;
            currentToken = expresssion[i];
            return true;
        }

        for (; i < expresssion.Count; i++)
        {
            currentToken = expresssion[i];
            //Check element type
            //Check order: grouping, literal, operator, identifier
            if (currentToken.text == "(")
            {
                elements.Add(new ExpressionOpenParen(currentToken));
            }
            else if (currentToken.text == ")")
            {
                elements.Add(new ExpressionCloseParen(currentToken));
            }
            else if (TryParseLiteral(currentToken, currentScope, out RhValue value))
            {
                /*if (value.type == "stringptr")
                {
                    value = ParseString(currentToken, currentScope);
                    ExpressionElement element = new(ExpressionElementType.Value, currentToken, value.contents);
                    Console.WriteLine(BitConverter.ToUInt64(element.contents));
                    elements.Add(element);
                    continue;
                }*/
                elements.Add(new ExpressionValue(currentToken, value));
            }
            else if (BuiltInOperators.ContainsKey(currentToken.text))
            {
                elements.Add(new ExpressionBuiltInOp(currentToken, BuiltInOperators[currentToken.text]));
            }
            else if (currentToken.text == "sizeof")
            {
                Token identifier = currentToken;
                //Console.WriteLine(identifier);
                nextToken();
                //Console.WriteLine("reading sizeof");
                try
                {
                    if (currentToken.text != "(") ErrorReporting.CompilerError(currentToken, "sizeof expression requires parentheses and type or variable name");
                    nextToken();
                    if (currentScope.Variables.TryGetValue(currentToken.text, out RhVariable? variable))
                    {
                        RhType variableType = TypeSystem.Types[variable.type];
                        byte[] contents = Platform.GetPointerSize() switch
                        {
                            8 => BitConverter.GetBytes(variableType.size),
                            4 => BitConverter.GetBytes((uint)variableType.size),
                            2 => BitConverter.GetBytes((ushort)variableType.size),
                            _ => BitConverter.GetBytes((uint)variableType.size)
                        };

                        ExpressionValue sizeofValue = new(currentToken, new("uintptr", contents));
                        elements.Add(sizeofValue);
                        nextToken();
                        if (currentToken.text != ")") ErrorReporting.CompilerError(currentToken, "Expected ) at the end of sizeof expression");
                    }
                    else if (TypeSystem.ResolveTypeAlias(currentToken.text, out string typeName))
                    {
                        RhType type = TypeSystem.Types[typeName];
                        byte[] contents = Platform.GetPointerSize() switch
                        {
                            8 => BitConverter.GetBytes(type.size),
                            4 => BitConverter.GetBytes((uint)type.size),
                            2 => BitConverter.GetBytes((ushort)type.size),
                            _ => BitConverter.GetBytes((uint)type.size) //Default to 32-bit if we encounter an unexpected pointer size
                        };

                        ExpressionValue sizeofValue = new(currentToken, new("uintptr", contents));
                        elements.Add(sizeofValue);
                        nextToken();
                        if (currentToken.text != ")") ErrorReporting.CompilerError(currentToken, "Expected ) at the end of sizeof expression");
                    }
                }
                catch (IndexOutOfRangeException) { ErrorReporting.CompilerError(expresssion[^1], "Unexpected end of sizeof expression"); }
            }
            else if (TypeSystem.ResolveTypeAlias(currentToken.text, out string type))
            {
                elements.Add(new ExpressionTypeName(currentToken, TypeSystem.Types[type]));
            }
            else if (Utils.IsValidIdentifier(currentToken.text))
            {
                Token identifier = currentToken;
                //Console.WriteLine(identifier);
                nextToken();
                if (currentToken.text == "(")
                {
                    //Console.WriteLine("reading function call");
                    List<byte> elementBytes = Utils.MakeRhIFString(identifier.text).ToList();
                    List<RhExpression> parameters = new();
                    int nestingLevel = 1;
                    nextToken();
                    while ((currentToken.text != ")" || nestingLevel > 1) && i < expresssion.Count)
                    {
                        List<Token> currentExpression = new();
                        while (nestingLevel > 1 || currentToken.text != "," && currentToken.text != ")" && i < expresssion.Count)
                        {
                            if (currentToken.text == "(") nestingLevel++;
                            if (currentToken.text == ")") nestingLevel--;
                            currentExpression.Add(currentToken);
                            if (!nextToken()) goto endLoop;
                        }
                        parameters.Add(ParseExpression(currentExpression, currentScope));
                        nextToken();
                    }
                    endLoop:
                    RhFunction? function = null;
                    var functionList = currentScope.Tree.Functions.FirstOrDefault(x => x.Key == identifier.text, new(string.Empty, new()));
                    if (functionList.Key != string.Empty)
                    {
                        function = functionList.Value.FirstOrDefault(x => x.parameters.Count == parameters.Count || x.uncheckedParams);
                    }
                    if (function is null)
                    {
                        ErrorReporting.CompilerError(identifier, $"Could not find a function {identifier.text} with {parameters.Count} parameters");
                        continue;
                    }
                    elements.Add(new ExpressionFunctionCall(identifier, function, parameters));
                }
                else
                {
                    prevToken();
                    if (currentScope.Variables.ContainsKey(identifier.text))
                        elements.Add(new ExpressionVariable(identifier, currentScope.Variables[identifier.text]));
                    else
                        ErrorReporting.CompilerError(identifier, $"The name '{identifier.text}' does not exist in this context");
                }
            }
            else
            {
                ErrorReporting.CompilerError(currentToken, $"Unexpected element in expression: {currentToken.text}");
            }
        }
        foreach (var e in elements)
        {
            Console.Write($"{e.Type} ");
        }
        Console.WriteLine();
        return new(expresssion[0], expresssion.Last(), currentScope, elements);
    }

    public static FunctionCall? ParseFunctionCall(List<Token> statement, Scope currentScope)
    {
        Token identifier = statement[0];

        //Function call logic
        int index = 1;
        List<RhExpression> parameters = new();
        int nestingLevel = 1;
        while (index < statement.Count && (statement[index].text != ")" || nestingLevel > 1))
        {
            if (statement[index].text == "(") nestingLevel++;
            if (statement[index].text == ")") nestingLevel--;
            List<Token> expressionElements = new();
            do
            {
                if (statement[index].text != "," || nestingLevel >= 1)
                    expressionElements.Add(statement[index]);
                index++;
            } while (index < statement.Count && (statement[index].text != "," && statement[index].text != ")" || nestingLevel > 1));
            RhExpression expression = ParseExpression(expressionElements, currentScope);
            parameters.Add(expression);
            expressionElements = new();
        }
        if (currentScope.Tree.Functions.TryGetValue(identifier.text, out List<RhFunction>? result))
        {
            if (result.Any(x => x.parameters.Count == parameters.Count))
            {
                var call = new FunctionCall(currentScope, statement[0], result.First(x => x.parameters.Count == parameters.Count));
                foreach (RhExpression expr in parameters)
                {
                    call.Parameters.Add(expr);
                }
                return call;
            }
            else
            {
                ErrorReporting.CompilerError(identifier, $"There is no overload for function {identifier.text} that takes the given set of parameters");
            }
        }
        else
        {
            ErrorReporting.CompilerError(identifier, $"Function with name {identifier.text} does not exist in this context");
        }
        //new FunctionCall(currentScope, currentStatement[0], currentScope.Functions[identifier.text].First(x => TypeHandler.ParamListsSame()))
        return null;
    }

    public static bool TryParseLiteral(Token tok, Scope scope, out RhValue result)
    {
        if (bool.TryParse(tok.text, out bool bValue))
        {
            result = new RhValue("bool", BitConverter.GetBytes(bValue));
            return true;
        }
        if (int.TryParse(tok.text, out int i32value))
        {
            result = new RhValue("int32", BitConverter.GetBytes(i32value));
            return true;
        }
        else if (uint.TryParse(tok.text, out uint u32value))
        {
            result = new RhValue("uint32", BitConverter.GetBytes(u32value));
            return true;
        }
        else if (long.TryParse(tok.text, out long value))
        {
            result = new RhValue("int64", BitConverter.GetBytes(value));
            return true;
        }
        else if (ulong.TryParse(tok.text, out ulong uvalue))
        {
            result = new RhValue("uint64", BitConverter.GetBytes(uvalue));
            return true;
        }
        else if (double.TryParse(tok.text, out double dvalue))
        {
            result = new RhValue("float64", BitConverter.GetBytes(dvalue));
            return true;
        }
        else if (tok.text.StartsWith('"'))
        {
            result = ParseString(tok, scope);
            return true;
        }
        else if (tok.text.StartsWith('\''))
        {
            result = new("int32", TypeSystem.ParseCharacterLiteral(tok));
            return true;
        }
        result = null!;
        return false;
    }

    private static RhValue ParseString(Token tok, Scope scope)
    {
        scope.Tree.Strings.Add(new RhString(scope.Tree.Strings.Count, TypeSystem.ParseStringLiteral(tok)));
        return new("stringptr", BitConverter.GetBytes((ulong)scope.Tree.Strings.Count - 1));
    }
}
