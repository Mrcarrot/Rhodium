//Rhodium Type System
//Version 2- KISS edition
//We don't talk about RhType 1

using System.Text;

/// <summary>
/// Defines certain information about a Rhodium type.
/// </summary>
/// <param name="name">The type's Rhodium name.</param>
/// <param name="size">The minimum size in bytes of the type.</param>
/// <param name="id">The type's numeric ID.</param>
/// <returns></returns>
public record RhType(string name, ulong size, ulong id, TypeSystem.TypeClass typeClass);

/// <summary>
/// Defines a binary value associated with a particular type.
/// </summary>
/// <param name="type">The Rhodium name of the type.</param>
/// <param name="contents">The binary contents of the type.</param>
/// <returns></returns>
public record RhValue(string type, byte[] contents);

/// <summary>
/// Defines a container that can hold a value of a certain type.
/// </summary>
/// <param name="identifier">The variable's name.</param>
/// <param name="type">The Rhodium name of the variable's type.</param>
/// <returns></returns>
public record RhVariable(string identifier, string type);

/// <summary>
/// Defines a function with the given name and parameter list.
/// </summary>
/// <param name="identifier">The function's name.</param>
/// <param name="type">The type of the function.</param>
/// <param name="parameters">The list of parameters the function accepts.</param>
/// <param name="overloads">Other functions with the same name but different parameter lists.</param>
/// <param name="uncheckedParams">Whether or not the parser should skip the step of verifying that the parameters passed to the function match its signature.</param>
/// <returns></returns>
public record RhFunction(string identifier, string type, List<RhVariable> parameters, List<RhFunction>? overloads = null, bool uncheckedParams = false);

/// <summary>
/// Defines a string stored in the syntax tree to be retrieved by ID.
/// </summary>
/// <param name="id">The string's numeric ID in the syntax tree.</param>
/// <param name="contents">The string's UTF-8 contents.</param>
/// <returns></returns>
public record RhString(long id, byte[] contents);
//public record RhPtr(Scope pointedScope, RhVariable pointedValue);

public static class TypeSystem
{
    public static readonly RhValue NullValue = new("void", new byte[0]);

    public static Dictionary<string, RhType> Types = new()
    {
        //Signed integer types
        { "int64", new("int64", 8, 259, TypeClass.SInteger) },
        { "int32", new("int32", 4, 258, TypeClass.SInteger) },
        { "int16", new("int16", 2, 257, TypeClass.SInteger) },
        { "int8", new("int8", 1, 256, TypeClass.SInteger) },
        
        //Unsigned integer types
        { "uint64", new("uint64", 8, 265, TypeClass.UInteger) },
        { "uint32", new("uint32", 4, 264, TypeClass.UInteger) },
        { "uint16", new("uint16", 2, 263, TypeClass.UInteger) },
        { "uint8", new("uint8", 1, 262, TypeClass.UInteger) },
        
        //Floating-point types
        { "float64", new("float64", 8, 270, TypeClass.Float) },
        { "float32", new("float32", 4, 269, TypeClass.Float) },
        { "float16", new("float16", 2, 268, TypeClass.Float) },

        //Special types
        { "void", new("void", 0, 0, TypeClass.NonNumeric) },
        { "bool", new("bool", 1, 275, TypeClass.IntConvertible) },

        //Pointer types are now automatically added to this list by the Platform class, rather than hardcoding the 64-bit pointer size.
        /*
        { "stringptr", new("stringptr", 8, 273) }, //We'll get rid of this when I stop being lazy and add either real pointers or arrays
        { "opaqueptr", new("opaqueptr", 8, 274) },  //Here it is, the real pointer
                                                   //However strings are still handled by the stringptr type and it does some special stuff I don't care to change so for now we have strongly typed strings
        */
    };

    public enum TypeClass
    {
        IntConvertible,
        UInteger,
        SInteger,
        FloatConvertible,
        Float,
        
        NonNumeric,
    }

    public static Dictionary<string, string> TypeAliases = new()
    {
        /*{ "char", "int32" },
        { "byte", "int8" },
        { "char8", "int8" }*/
    };

    public static string[] PossibleMistakeTypes =
    {
        "int",
        "long",
        "short",
        "float",
        "single",
        "double"
    };

    public static RhType GetRhType(Token typeName)
    {
        if (ResolveTypeAlias(typeName.text, out string baseType))
        {
            return Types[baseType];
        }
        ErrorReporting.CompilerError(typeName, $"Type {typeName.text} not found");
        return new("RH_UNKNOWN_TYPE", 0, 0, TypeClass.NonNumeric);
    }

    public static string CodegenTypeConversion(string sourceType, string destType, string sourceName, ref int operationIndex)
    {
        /*
        Conversion Instructions:
        sitofp <integer type> <value> to <float type>
        uitofp
        fptosi
        fptoui
        sext <integer type> <value> to <larger type>
        zext
        trunc
        fpext
        fptrunc
        ptrtoint
        inttoptr
        bitcast
        */
        /*
         Excerpt from the C standard(draft) about integer conversions:
         6.3.1.3 Signed and unsigned integers
            When a value with integer type is converted to another integer type other than _Bool , if the value
            can be represented by the new type, it is unchanged.
            Otherwise, if the new type is unsigned, the value is converted by repeatedly adding or subtracting
            one more than the maximum value that can be represented in the new type until the value is in the
            range of the new type.
            Otherwise, the new type is signed and the value cannot be represented in it; either the result is
            implementation-defined or an implementation-defined signal is raised.
         */
        if (!ResolveTypeAlias(sourceType, out string canonicalSType) || !ResolveTypeAlias(destType, out string canonicalDType))
        {
            throw new InvalidOperationException($"rhc: Convert {sourceType}->{destType} failed; one or more types invalid\nTHIS IS AN INTERNAL COMPILER BUG. Please report it at https://codeberg.org/Mrcarrot/Rhodium. Be sure to provide some source code that triggers this bug.");
        }

        RhType srcType = Types[canonicalSType];
        RhType dstType = Types[canonicalDType];

        if (srcType.id == dstType.id) return $"%__{operationIndex++} = {GetLLVMTypeName(srcType.name)} %{sourceName}";
        ConversionType conversionType = GetConversionType(canonicalSType, canonicalDType);
        //throw new NotImplementedException();
        return $"%__{operationIndex++} = bitcast {GetLLVMTypeName(srcType.name)} %{sourceName} to {GetLLVMTypeName(dstType.name)}";
    }

    public static string GetOperationInstruction(string type, ExpressionOperation op)
    {
        switch (op)
        {
            case ExpressionOperation.Add:
                return type switch
                {
                    var x when x.StartsWith("int") || x.StartsWith("uint") || x.EndsWith("ptr") => "add",
                    var x when x.StartsWith("float") => "fadd",
                    _ => throw new NotImplementedException()
                };
            case ExpressionOperation.Subtract:
                return type switch
                {
                    var x when x.StartsWith("int") || x.StartsWith("uint") || x.EndsWith("ptr") => "sub",
                    var x when x.StartsWith("float") => "fsub",
                    _ => throw new NotImplementedException()
                };
            case ExpressionOperation.Multiply:
                return type switch
                {
                    var x when x.StartsWith("int") || x.StartsWith("uint") => "mul",
                    var x when x.StartsWith("float") => "fmul",
                    _ => throw new NotImplementedException()
                };
            case ExpressionOperation.Divide:
                return type switch
                {
                    var x when x.StartsWith("int") => "sdiv",
                    var x when x.StartsWith("uint") => "udiv",
                    var x when x.StartsWith("float") => "fdiv",
                    _ => throw new NotImplementedException()
                };
        }

        throw new NotImplementedException();
    }

    /// <summary>
    /// Checks if the provided name is a valid type name, including aliases.
    /// </summary>
    /// <param name="typeName"></param>
    /// <returns>Whether or not the type name is valid. Puts the base type name after following aliases into result.</returns>
    public static bool ResolveTypeAlias(string typeName, out string result)
    {
        while (TypeAliases.ContainsKey(typeName))
        {
            typeName = TypeAliases[typeName];
        }
        result = typeName;
        return Types.ContainsKey(typeName);
    }

    public static RhType? PromoteType(string t1, string t2) => PromoteType(Types[t1], Types[t2]);

    public static RhType? PromoteType(RhType t1, RhType t2)
    {
        if (t1.id == t2.id) return t1;
        if (t1.name == "void") return t2; //Due to how we're handling operators void always promotes- we check the expression for validity later
        if (t2.name == "void") return t1;
        
        bool fp = t1.typeClass == TypeClass.Float || t2.typeClass == TypeClass.Float;
        bool signed = t1.typeClass == TypeClass.SInteger || t2.typeClass == TypeClass.SInteger;
        bool numeric = t1.typeClass != TypeClass.NonNumeric && t2.typeClass != TypeClass.NonNumeric;
        if (!numeric) return null; //At this time we don't have compound expressions of non-numeric types
        if (fp)
        {
            return Types[ulong.Max(t1.size, t2.size) switch
            {
                <= 2 => "float16",
                <= 4 => "float32",
                > 4 => "float64"
            }];
        }
        else if (signed)
        {
            return Types[ulong.Max(t1.size, t2.size) switch
            {
                <= 1 => "int8",
                2 => "int16",
                <= 4 => "int32",
                > 4 => "int64"
            }];
        }
        else
        {
            return Types[ulong.Max(t1.size, t2.size) switch
            {
                <= 1 => "uint8",
                2 => "uint16",
                <= 4 => "uint32",
                > 4 => "uint64"
            }];
        }
    }

    /// <summary>
    /// Parses tokens into an RhValue with type and binary data
    /// </summary>
    /// <param name="type"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static RhValue ParseValue(Token type, Token value)
    {
        string typeName = type.text;
        while (TypeAliases.ContainsKey(typeName))
        {
            typeName = TypeAliases[typeName];
        }
        switch (typeName)
        {
            case ("int64"):
                return ParseInt64(value);

            case ("int32"):
                return ParseInt32(value);

            case ("int16"):
                return ParseInt16(value);

            case ("int8"):
                return ParseInt8(value);

            case ("uint8"):
                return ParseUInt8(value);

            case ("uint16"):
                return ParseUInt16(value);

            case ("uint32"):
                return ParseUInt32(value);

            case ("uint64"):
                return ParseUInt64(value);

            case ("float16"):
                return ParseFloat16(value);

            case ("float32"):
                return ParseFloat32(value);

            case ("float64"):
                return ParseFloat64(value);

            case ("bool"):
                return ParseBool(value);

            case ("void"):
                ErrorReporting.CompilerError(value, "Cannot assign a value to type void");
                break;

            case ("stringptr"):
                return new("stringptr", ParseStringLiteral(value));

            case ("int"):
                ErrorReporting.CompilerError(type, "The type 'int' does not exist(did you mean int32 or int64?)");
                break;

            case ("long"):
                ErrorReporting.CompilerError(type, "The type 'long' does not exist(did you mean int64?)");
                break;

            case ("short"):
                ErrorReporting.CompilerError(type, "The type 'short' does not exist(did you mean int16?)");
                break;

            case ("float"):
                ErrorReporting.CompilerError(type, "The type 'float' does not exist(did you mean float32 or float64?)");
                break;

            case ("single"):
                ErrorReporting.CompilerError(type, "The type 'single' does not exist(did you mean float32?)");
                break;

            case ("double"):
                ErrorReporting.CompilerError(type, "The type 'double' does not exist(did you mean float64?)");
                break;

            default:
                ErrorReporting.CompilerError(type, $"Type name {typeName} not found");
                break;
        }
        return NullValue;
    }

    /// <summary>
    /// Checks if the given string is a type name accepted by the value parser.
    /// </summary>
    /// <param name="str">The name of the type.</param>
    /// <param name="strict">Whether or not to include possible mistakes such as int instead of int32 or double instead of float64.</param>
    /// <returns></returns>
    public static bool IsTypeName(string str, bool strict = false)
    {
        return Types.ContainsKey(str) || TypeAliases.ContainsKey(str) || (!strict && PossibleMistakeTypes.Contains(str));
    }

    public static bool AreTypesCompatible(string t1, string t2)
    {
        if (!ResolveTypeAlias(t1, out string canonicalt1) || !ResolveTypeAlias(t2, out string canonicalt2))
        {
            return false;
        }
        else return !(Types[canonicalt1].typeClass == TypeClass.NonNumeric || Types[canonicalt2].typeClass == TypeClass.NonNumeric) 
            || canonicalt1 == canonicalt2;
    }

    public static ConversionType GetConversionType(string startType, string destType)
    {
        if (startType == destType) return ConversionType.SameType;
        if (!ResolveTypeAlias(startType, out string canonicalt1) || !ResolveTypeAlias(destType, out string canonicalt2))
        {
            return ConversionType.None;
        }
        if (canonicalt1 == canonicalt2) return ConversionType.CastEquivalent; //If the type names passed in here aren't the same but the underlying representation is, we require a cast
        RhType sType = Types[canonicalt1];
        RhType dType = Types[canonicalt2];
        if (dType.size < sType.size) return ConversionType.Narrowing;
        if (dType.typeClass == TypeClass.NonNumeric || sType.typeClass == TypeClass.NonNumeric) return ConversionType.None;
        if ((dType.typeClass == TypeClass.SInteger || dType.typeClass == TypeClass.UInteger) && sType.typeClass == TypeClass.Float)
            return ConversionType.Narrowing;
        if (sType.typeClass == TypeClass.SInteger && dType.typeClass == TypeClass.UInteger) return ConversionType.SignedToUnsigned;
        if (sType.typeClass == TypeClass.UInteger && dType.typeClass == TypeClass.SInteger) return ConversionType.UnsignedToSigned;
        return ConversionType.Implicit;
    }

    public static string GetDefaultReturn(string type) => type switch {
        string s when s.StartsWith("int") || s.StartsWith("uint") => $"ret {GetLLVMTypeName(type)} 0",
        string s when s.StartsWith("float") => $"ret {GetLLVMTypeName(type)} 0.0",
        "bool" => $"ret i1 false",
        "stringptr" or "opaqueptr" => $"ret ptr 0",
        _ => "ret void"
    };

    public enum ConversionType
    {
        Implicit,
        Narrowing,
        CastEquivalent,
        SameType,
        UnsignedToSigned,
        SignedToUnsigned,
        FloatToInt,
        IntToFloat,
        None
    }

    public static List<(string, string)> ImplicitConversions = new()
    {
        ("int8", "int16"),
        ("int16", "int32"),
        ("int32", "int64")
    };

    public static string GetLLVMString(RhValue value)
    {
        return value.type switch
        {
            //Use hexadecimal float literals for LLVM
            //These aren't always necessary, but they're simple to generate
            //WARNING: THIS MAY BREAK ON BIG-ENDIAN PROCESSORS(such as MIPS or sometimes PowerPC)
            //I HAVE NOT TESTED THIS CODE ON ANYTHING ELSE OR FOUND COMPREHENSIVE DOCUMENTATION ON THIS SUBJECT
            //In any case, C# doesn't officially support any big-endian architectures so ¯\_(ツ)_/¯
            "float16" => $"0x{GetLEHexString(BitConverter.GetBytes(((double)BitConverter.ToHalf(value.contents))))}",
            "float32" => $"0x{GetLEHexString(BitConverter.GetBytes(((double)BitConverter.ToSingle(value.contents))))}",
            "float64" => $"0x{GetLEHexString(value.contents)}",
            /*"float16" => AddTrailingZero(BitConverter.ToHalf(value.contents).ToString()),
            "float32" => AddTrailingZero(BitConverter.ToSingle(value.contents).ToString()),
            "float64" => AddTrailingZero(BitConverter.ToDouble(value.contents).ToString()),*/
            _ => GetString(value)
        };
    }

    public static string AddTrailingZero(string str)
    {
        return str.Contains('.') ? str : $"{str}.0";
    }

    /// <summary>
    /// Converts a byte array to a little-endian hexadecimal string using capital letters and two digits per byte.
    /// </summary>
    /// <param name="bytes"></param>
    /// <returns></returns>
    private static string GetLEHexString(IEnumerable<byte> bytes)
    {
        StringBuilder output = new(bytes.Count() * 2);
        foreach (byte b in bytes.Reverse())
        {
            output.Append(b.ToString("X2"));
        }
        return output.ToString();
    }

    public static string GetString(RhValue value)
    {
        switch (value.type)
        {
            case ("int64"):
                return BitConverter.ToInt64(value.contents).ToString();

            case ("int32"):
                return BitConverter.ToInt32(value.contents).ToString();

            case ("int16"):
                return BitConverter.ToInt16(value.contents).ToString();

            case ("int8"):
                return ((sbyte)value.contents[0]).ToString();

            case ("uint8"):
                return value.contents[0].ToString();

            case ("uint64"):
                return BitConverter.ToUInt64(value.contents).ToString();

            case ("uint32"):
                return BitConverter.ToUInt32(value.contents).ToString();

            case ("uint16"):
                return BitConverter.ToUInt16(value.contents).ToString();

            case ("float16"):
                return BitConverter.ToHalf(value.contents).ToString();

            case ("float32"):
                return BitConverter.ToSingle(value.contents).ToString();

            case ("float64"):
                return BitConverter.ToDouble(value.contents).ToString();

            case ("stringptr"):
            {
                ulong index = BitConverter.ToUInt64(value.contents);
                return $"literal string {index}";
                //Old string logic from before I changed the contents to reference a string from the tree
                /*byte[] lengthBytes = new byte[8];
                Array.Copy(value.contents, lengthBytes, 8);
                byte[] stringBytes = value.contents.Skip(8).ToArray();
                ulong length = BitConverter.ToUInt64(lengthBytes);
                string contents = Encoding.UTF8.GetString(stringBytes, 0, (int)length)
                    .Replace("\n", @"\n");
                return $"{length} \"{contents}\"";*/
            }

            case ("uintptr"):
            {
                return Platform.GetPointerSize() switch
                {
                    8 => BitConverter.ToUInt64(value.contents).ToString(),
                    4 => BitConverter.ToUInt32(value.contents).ToString(),
                    _ => BitConverter.ToUInt32(value.contents).ToString()
                };
            }

            case ("intptr"):
            {
                return Platform.GetPointerSize() switch
                {
                    8 => BitConverter.ToInt64(value.contents).ToString(),
                    4 => BitConverter.ToInt32(value.contents).ToString(),
                    _ => BitConverter.ToInt32(value.contents).ToString()
                };
            }

            case ("bool"):
                return BitConverter.ToBoolean(value.contents).ToString().ToLower();

            default:
                ErrorReporting.CompilerError("Unknown file", 0, 0, $"The type {value.type} was not found for conversion to string. This is an internal compiler error. Please report this bug at https://codeberg.org/Mrcarrot/Rhodium.");
                return "";
        }
    }

    public static string GetLLVMTypeName(string type)
    {
        if (TypeAliases.ContainsKey(type)) return GetLLVMTypeName(TypeAliases[type]);
        return type switch
        {
            "int64" or "uint64" => "i64",
            "int32" or "uint32" => "i32",
            "int16" or "uint16" => "i16",
            "int8" or "uint8" => "i8",

            "float64" => "double",
            "float32" => "float",
            "float16" => "half",

            "bool" => "i1",
            "stringptr" => "ptr",
            "opaqueptr" => "ptr",
            "void" => "void",

            "intptr" or "uintptr" => $"i{Platform.GetPointerSize() * 8}",

            _ => "RH_UNKNOWN_TYPE"
        };
    }

    public static RhValue ParseInt8(Token value)
    {
        if (sbyte.TryParse(value.text, out sbyte int8result))
        {
            byte[] arrayresult = { unchecked((byte)int8result) }; //Convert to binary without bounds checking for direct conversion
            return new("int8", arrayresult);
        }
        else if (value.text.StartsWith('\''))
        {
            byte[] bytes = ParseCharacterLiteral(value);
            if (bytes.Length > 1) ErrorReporting.CompilerError(value, $"The character '{Encoding.UTF8.GetChars(bytes)[0]}' is too large to fit into int8(maybe use int32?)");
            else return new("int8", bytes);
        }
        else if (value.text == "True" || value.text == "true")
        {
            byte[] contents =
            {
                1
            };
            return new("int8", contents);
        }
        else if (value.text == "False" || value.text == "false")
        {
            byte[] contents =
            {
                0
            };
            return new("int8", contents);
        }
        else
        {
            ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int8");
            return new("void", new byte[0]);
        }
        return NullValue;
    }

    public static RhValue ParseInt32(Token value)
    {
        if (int.TryParse(value.text, out int int32result))
        {
            return new("int32", BitConverter.GetBytes(int32result));
        }
        else if (value.text.StartsWith('\''))
        {
            byte[] bytes = ParseCharacterLiteral(value);
            //Make sure we've got exactly 4 bytes-
            //>4 is not possible with a single UTF-8 character.
            while (bytes.Length < 4)
            {
                bytes = bytes.Prepend<byte>(0).ToArray(); //This feels disgusting
            }
            return new("int32", bytes);
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int32");
        return NullValue;
    }

    public static RhValue ParseInt64(Token value)
    {
        if (long.TryParse(value.text, out long result))
        {
            return new("int64", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int64");
        return NullValue;
    }

    public static RhValue ParseInt16(Token value)
    {
        if (short.TryParse(value.text, out short result))
        {
            return new("int16", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int16");
        return NullValue;
    }

    public static RhValue ParseUInt8(Token value)
    {
        if (byte.TryParse(value.text, out byte int8result))
        {
            byte[] arrayResult = { int8result };
            return new("int8", arrayResult);
        }
        else if (value.text.StartsWith('\''))
        {
            byte[] bytes = ParseCharacterLiteral(value);
            if (bytes.Length > 1) ErrorReporting.CompilerError(value, $"The character '{Encoding.UTF8.GetChars(bytes)[0]}' is too large to fit into int8(maybe use int32?)");
            else return new("int8", bytes);
        }
        else if (value.text == "True" || value.text == "true")
        {
            byte[] contents =
            {
                1
            };
            return new("int8", contents);
        }
        else if (value.text == "False" || value.text == "false")
        {
            byte[] contents =
            {
                0
            };
            return new("int8", contents);
        }
        else
        {
            ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int8");
            return new("void", new byte[0]);
        }
        return NullValue;
    }

    public static RhValue ParseUInt32(Token value)
    {
        if (int.TryParse(value.text, out int int32result))
        {
            return new("int32", BitConverter.GetBytes(int32result));
        }
        else if (value.text.StartsWith('\''))
        {
            byte[] bytes = ParseCharacterLiteral(value);
            //Make sure we've got exactly 4 bytes-
            //>4 is not possible with a single UTF-8 character.
            while (bytes.Length < 4)
            {
                bytes = bytes.Prepend<byte>(0).ToArray(); //This feels disgusting
            }
            return new("int32", bytes);
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int32");
        return NullValue;
    }

    public static RhValue ParseUInt64(Token value)
    {
        if (long.TryParse(value.text, out long result))
        {
            return new("int64", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int64");
        return NullValue;
    }

    public static RhValue ParseUInt16(Token value)
    {
        if (short.TryParse(value.text, out short result))
        {
            return new("int16", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as int16");
        return NullValue;
    }

    public static RhValue ParseFloat16(Token value)
    {
        if (Half.TryParse(value.text, out Half result))
        {
            return new("float16", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as float16");
        return NullValue;
    }

    public static RhValue ParseFloat32(Token value)
    {
        if (float.TryParse(value.text, out float result))
        {
            return new("float32", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as float32");
        return NullValue;
    }

    public static RhValue ParseFloat64(Token value)
    {
        if (double.TryParse(value.text, out double result))
        {
            return new("float64", BitConverter.GetBytes(result));
        }
        else ErrorReporting.CompilerError(value, $"Could not read '{value.text}' as float64");
        return NullValue;
    }

    public static byte[] ParseCharacterLiteral(Token literal)
    {
        string literalContents = literal.text.Substring(1, literal.text.Length - 2);
        if (literalContents.StartsWith('\\'))
        {
            try
            {
                char output = literalContents[1] switch
                {
                    'n' => '\n',  //Line feed
                    't' => '\t',  //Horizontal tab
                    'r' => '\r',  //Carriage return
                    'a' => '\a',  //Bell character
                    'v' => '\v',  //Vertical tab
                    'f' => '\f',  //Form feed
                    'b' => '\b',  //Backspace
                    '0' => '\0',  //Null character
                    '"' => '"',   //Double quote
                    '\\' => '\\', //Backslash
                    _ => throw new RhCompilerException(literal, $"Unrecognized escape sequence: \\{literalContents[1]}")
                };
                return Encoding.UTF32.GetBytes(output.ToString());
            }
            catch (RhCompilerException e)
            {
                ErrorReporting.CompilerError(e.File, e.Line, e.Col, e.Message);
            }
        }
        else
        {
            if (literalContents.Length == 1)
            {
                return Encoding.UTF32.GetBytes(literalContents);
            }
            else if (literalContents.Length > 1)
                ErrorReporting.CompilerError(literal, $"More than one character in character literal", "Rhodium strings use double quotes (\"\")");
            else
                ErrorReporting.CompilerError(literal, "Empty character literal", "Rhodium strings use double quotes(\"\")");
        }
        return new byte[0];
    }

    public static RhValue ParseBool(Token literal)
    {
        if (literal.text is "True" or "true") return new("bool", BitConverter.GetBytes(true));
        if (literal.text is "False" or "false") return new("bool", BitConverter.GetBytes(false));
        ErrorReporting.CompilerError(literal, $"Could not read {literal.text} as bool");
        return new("bool", BitConverter.GetBytes(false));
    }

    public static byte[] ParseStringLiteral(Token literal)
    {
        string text = literal.text;
        if (text.StartsWith('"'))
        {
            text = text.Substring(1, text.Length - 2);
        }
        StringBuilder escapedText = new(text.Length);
        for (int i = 0; i < text.Length; i++)
        {
            if (text[i] == '\\')
            {
                i++;

                try
                {
                    escapedText.Append(
                        text[i] switch
                        {
                            'n' => '\n',  //Line feed
                            't' => '\t',  //Horizontal tab
                            'r' => '\r',  //fuck windows
                            'a' => '\a',  //Bell character
                            'v' => '\v',  //Vertical tab
                            'f' => '\f',  //Form feed
                            'b' => '\b',  //Backspace
                            '0' => '\0',  //Null character
                            '"' => '"',   //Double quote
                            '\\' => '\\', //Backslash
                            _ => throw new RhCompilerException(literal, $"Unrecognized escape sequence: \\{text[i]}")
                        }
                    );
                }
                catch (RhCompilerException e)
                {
                    ErrorReporting.CompilerError(e.File, e.Line, e.Col, e.Message);
                }
            }
            else escapedText.Append(text[i]);
        }
        List<byte> textBytes = Encoding.UTF8.GetBytes(escapedText.ToString()).ToList();
        byte[] sizeBytes = BitConverter.GetBytes((ulong)textBytes.Count);
        for (int i = 0; i < sizeBytes.Length; i++)
        {
            textBytes.Insert(i, sizeBytes[i]);
        }
        return textBytes.ToArray();
    }

    public static bool ParamListsSame(List<RhVariable> a, List<RhVariable> b)
    {
        if (a.Count != b.Count) return false;
        for (int i = 0; i < a.Count; i++)
        {
            if (a[i].type != b[i].type) return false;
        }
        return true;
    }
}
