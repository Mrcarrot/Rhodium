#ifndef RHC_SYNTAXTREE_H
#define RHC_SYNTAXTREE_H
#include <memory>
#include <llvm/IR/Value.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/IRBuilder.h>
#include <vector>
#include <string>
#include <map>
#include <span>

class Scope;

class STNode
{
public:
    std::weak_ptr<Scope> Parent;
    virtual llvm::Value *Codegen();
};

class Scope : public virtual STNode
{
public:
    std::weak_ptr<Scope> Parent;
    llvm::Value *Codegen();
    std::vector<STNode> Children;
    std::map<std::string, llvm::Value *> NamedValues;
    std::unique_ptr<llvm::Module> Module;
    std::unique_ptr<llvm::LLVMContext> Context;
    std::unique_ptr<llvm::IRBuilder<>> Builder;
    Scope();
    Scope(std::weak_ptr<Scope> parent);
    void AddChild(STNode &node);
};

enum class ExpressionElementType : uint16_t
{
    Value,
    Variable,
    BuiltInOp,
    CustomOp,
    OpenParen,
    CloseParen,
    FunctionCall
};

typedef struct
{
    ExpressionElementType type;
    std::span<uint8_t> contents;
} ExpressionElement;

typedef struct
{
    std::vector<ExpressionElement> Elements;
} Expression;

class VariableAssignment : public virtual STNode
{
public:
    std::weak_ptr<Scope> Parent;
    llvm::Value *Codegen();
    std::string VariableName;

};

class SyntaxTree
{
public:
    std::shared_ptr<Scope> GlobalScope;
    std::string FileName;
    SyntaxTree(std::string fileName);
};

struct RhVariable
{
public:
    bool Ref;
    bool Const;
    bool Pointer;
    bool Array;
    uint64_t ArraySize;
    uint64_t Type;
    std::string Name;
    RhVariable(uint64_t type, std::string name, bool rh_ref = false, bool rh_const = false, bool rh_ptr = false, bool rh_array = false, uint64_t arraySize = 0);
};

class FunctionDeclaration : public STNode
{
public:
    bool Extern;
    uint64_t Type;
    std::string Name;
    uint64_t ParamCount;
    std::vector<RhVariable> Parameters;
    FunctionDeclaration(uint64_t type, std::string name, std::vector<RhVariable> parameters, bool Extern = false);
    //llvm::Value *Codegen();
};
#endif