#ifndef RHC_SYNTAXTREE_NUMERICS_H
#define RHC_SYNTAXTREE_NUMERICS_H
#include "SyntaxTree.h"
#include <cstdint>

class ExprInt8 : public STNode
{
    int8_t value;

public: 
    ExprInt8(int8_t val) : value(val) {}
};

class ExprInt16 : public STNode
{
    int16_t value;

public: 
    ExprInt16(int16_t val) : value(val) {}
};

class ExprInt32 : public STNode
{
    int32_t value;

public: 
    ExprInt32(int32_t val) : value(val) {}
};

class ExprInt64 : public STNode
{
    int64_t value;

public: 
    ExprInt64(int64_t val) : value(val) {}
};

class ExprInt128 : public STNode
{
    __int128_t value;

public: 
    ExprInt128(__int128_t val) : value(val) {}
};

class ExprUInt8 : public STNode
{
    uint8_t value;

public: 
    ExprUInt8(uint8_t val) : value(val) {}
};

class ExprUInt16 : public STNode
{
    uint16_t value;

public: 
    ExprUInt16(uint16_t val) : value(val) {}
};

class ExprUInt32 : public STNode
{
    uint32_t value;

public: 
    ExprUInt32(int32_t val) : value(val) {}
};

class ExprUInt64 : public STNode
{
    uint64_t value;

public: 
    ExprUInt64(uint64_t val) : value(val) {}
};

class ExprUInt128 : public STNode
{
    __uint128_t value;

public: 
    ExprUInt128(__uint128_t val) : value(val) {}
};

class ExprFloat16 : public STNode
{
    uint16_t value;

public:
    ExprFloat16(__uint16_t val) : value(val) {}
};

class ExprFloat32 : public STNode
{
    _Float32 value;

public:
    ExprFloat32(_Float32 val) : value(val) {}
};

class ExprFloat64 : public STNode
{
    _Float64 value;
    
public: 
    ExprFloat64(_Float64 val) : value(val) {}
};
#endif