#include <string>
#include <vector>

namespace rhc
{
    namespace errors
    {
        typedef struct errorstruct
        {
            std::string message;
            uint64_t tokenIndex;
        } error;

        static int ErrorCount, WarningCount;

        static std::vector<error> Errors;
        static std::vector<error> Warnings;

        void LogError(std::string message);
        void LogWarning(std::string message);
    }
}