#include "RhcCodegen.h"
#include "RhcErrorLogging.h"
#include "RhcUtils.h"
#include <iostream>
#include <map>

namespace rhc
{
    static llvm::LLVMContext TheContext;
    static llvm::IRBuilder<> Builder(TheContext);
    static std::unique_ptr<llvm::Module> TheModule;
    static std::map<std::string, llvm::Value *> NamedValues;
    llvm::Value *ExpressionCodegen(llvm::LLVMContext &context, const std::span<uint8_t> &bytes, size_t &index)
    {
        uint64_t expressionSize = rhif::getValue<uint64_t>(bytes, index);
        std::vector<llvm::Value *> expression;
        for (uint64_t i = 0; i < expressionSize; i++)
        {
            uint16_t classifier = rhif::getValue<uint16_t>(bytes, index);
            switch (classifier)
            {
                case (0):
                {
                    RhType type = rhif::getValue<RhType>(bytes, index);
                    switch (type)
                    {
                        case (RhType::INT8):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(8, rhif::getValue<int8_t>(bytes, index), true)));
                            break;
                        case (RhType::INT16):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(16, rhif::getValue<int16_t>(bytes, index), true)));
                            break;
                        case (RhType::INT32):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(32, rhif::getValue<int32_t>(bytes, index), true)));
                            break;
                        case (RhType::INT64):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(64, rhif::getValue<int64_t>(bytes, index), true)));
                            break;
                        case (RhType::INT128):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(128, rhif::getValue<__int128_t>(bytes, index), true)));
                            break;

                        case (RhType::UINT8):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(8, rhif::getValue<uint8_t>(bytes, index), false)));
                            break;
                        case (RhType::UINT16):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(16, rhif::getValue<uint16_t>(bytes, index), false)));
                            break;
                        case (RhType::UINT32):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(32, rhif::getValue<uint32_t>(bytes, index), false)));
                            break;
                        case (RhType::UINT64):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(64, rhif::getValue<uint64_t>(bytes, index), false)));
                            break;
                        case (RhType::UINT128):
                            expression.push_back(llvm::ConstantInt::get(
                                context, llvm::APInt(128, rhif::getValue<__uint128_t>(bytes, index), false)));
                            break;

                        case (RhType::FLOAT32):
                            expression.push_back(
                                llvm::ConstantFP::get(context, llvm::APFloat(rhif::getValue<_Float32>(bytes, index))));
                            break;

                        case (RhType::FLOAT64):
                            expression.push_back(
                                llvm::ConstantFP::get(context, llvm::APFloat(rhif::getValue<_Float64>(bytes, index))));
                            break;
                            /*
                            case (FLOAT128):
                                expression.push_back(llvm::ConstantFP::get(context,
                            llvm::APFloat(rhif::getValue<__float128>(bytes))))); break;
                            */

                        default:
                            rhc::errors::LogError("Unrecognized or unsupported type");
                    }
                    break;
                }
                case (1):
                    // Variable expression

                    break;

                case (2):
                {
                    // Built-in operator- currently only unary and binary operators
                    uint16_t op = rhif::getValue<uint16_t>(bytes, index);
                    switch (op)
                    {
                        case 0:
                            // +
                            // Steps:
                            // 1. Type check
                            // 2. If compatible, promote as needed
                            // 3. Add the appropriate LLVM operators
                            break;

                        case 1:
                            // -
                            break;

                        case 2:
                            // *
                            break;

                        case 3:
                            // /
                            break;

                        case 4:
                            // %
                            break;

                        case 5:
                            // ==
                            break;

                        case 6:
                            // !=
                            break;

                        case 7:
                            // &&
                            break;

                        case 8:
                            // ||
                            break;

                        case 9:
                            // !
                            break;

                        case 10:
                            // <
                            break;

                        case 11:
                            // >
                            break;

                        case 12:
                            // <=
                            break;

                        case 13:
                            // >=
                            break;

                        case 14:
                            // &
                            break;

                        case 15:
                            // |
                            break;

                        case 16:
                            // ^
                            break;

                        case 17:
                            // <<
                            break;

                        case 18:
                            // >>
                            break;

                        default:
                            break;
                    }
                    break;
                }

                case (3):
                    // Custom string operator- to be implemented later
                    break;

                case (4):
                    // Open parenthesis

                case (5):
                    // Close parenthesis

                case (6):
                    // RhIF operation

                default:
                    std::cerr << "rhc: Unrecognized expression element class: " << classifier << std::endl;
                    exit(1);
                    break;
            }
        }
    }
} // namespace rhc