#include <cstring>
#include <ctype.h>
#include <string>
#include <iostream>

#include "RhcUtils.h"

namespace rhc
{
    namespace rhif
    {
        std::string getString(uint8_t *&bytes)
        {
            uint64_t len = getValue<uint64_t>(bytes);
            size_t n = (size_t)len; // I don't love this cast but it should work for most reasonably expected situations
            std::string output;
            output.reserve(n);
            for (size_t i = 0; i < n; i++)
            {
                output.push_back((char)bytes[i]);
            }
            bytes += n;
            return output;
        }

        std::string getString(uint8_t *&bytes, size_t n)
        {
            std::string output;
            output.reserve(n);
            for (size_t i = 0; i < n; i++)
            {
                output.push_back((char)bytes[i]);
            }
            bytes += n;
            return output;
        }

        std::string getString(const std::span<uint8_t> &bytes, size_t &index)
        {
            uint64_t len = getValue<uint64_t>(bytes, index);
            size_t n = (size_t)len;
            std::string output;
            output.reserve(n);
            for (size_t i = index; i < index + n; i++)
            {
                output.push_back((char)bytes[i]);
            }
            index += n;
            return output;
        }

        std::string getString(const std::span<uint8_t> &bytes, size_t &index, size_t n)
        {
            std::string output;
            output.reserve(n);
            for (size_t i = index; i < index + n; i++)
            {
                output.push_back((char)bytes[i]);
            }
            index += n;
            return output;
        }
    }

    void setForegroundColor(ConsoleColor color)
    {
        std::cout << "\x1b[" << 30 + (int)color << "m";
    }

    void setBackgroundColor(ConsoleColor color)
    {
        std::cout << "\x1b[" << 40 + (int)color << "m";
    }

    void resetColors()
    {
        std::cout << "\x1b[0m";
    }
}