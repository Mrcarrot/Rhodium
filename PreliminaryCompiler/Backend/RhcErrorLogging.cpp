#include "RhcErrorLogging.h"
#include "RhcUtils.h"
#include <iostream>

void rhc::errors::LogError(std::string message)
{
    error error;
    error.message = message;
    error.tokenIndex = 0;
    Errors.push_back(error);
    ErrorCount++;
    setForegroundColor(ConsoleColor::Red);
    std::cerr << "rhc: " << message << std::endl;
    resetColors();
}

void rhc::errors::LogWarning(std::string message)
{
    error warning;
    warning.message = message;
    warning.tokenIndex = 0;
    Warnings.push_back(warning);
    WarningCount++;
    setForegroundColor(ConsoleColor::Yellow);
    std::cerr << "rhc: " << message << std::endl;
    resetColors();
}