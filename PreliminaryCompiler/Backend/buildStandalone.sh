#!/bin/bash

clang++ SyntaxTree.cpp TestingMain.cpp RhcErrorLogging.cpp RhcUtils.cpp RhIFCompiler.cpp -o RhCBackend `llvm-config --cxxflags --ldflags --system-libs --libs core` -std=c++20 $@