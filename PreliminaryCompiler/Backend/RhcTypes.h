#include <cctype>
#include <cstddef>
#include <cstdint>
#include <map>
#include "RhcUtils.h"

namespace rhc
{
    static std::map<RhType, size_t> TypeSizes
    {
        { RhType::INT8, 1},
        { RhType::INT16, 2}, 
        { RhType::INT32, 4}, 
        { RhType::INT64, 8},
        { RhType::INT128, 16},
        { RhType::INT256, 32},
        { RhType::UINT8, 1},
        { RhType::UINT16, 2}, 
        { RhType::UINT32, 4},
        { RhType::UINT64, 8},
        { RhType::UINT128, 16},
        { RhType::UINT256, 32},
        { RhType::FLOAT16, 2},
        { RhType::FLOAT32, 4}, 
        { RhType::FLOAT64, 8}, 
        { RhType::FLOAT128, 16},
        { RhType::FLOAT256, 32},
    };
}