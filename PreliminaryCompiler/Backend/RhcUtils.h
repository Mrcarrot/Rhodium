#include <cstring>
#include <ctype.h>
#include <string>
#include <iostream>
#include <span>
#include <stdint.h>

#ifndef RHC_UTILS_H

#define RHC_UTILS_H

static const uint32_t RHIF_VERSION = 0;

namespace rhc
{

    namespace rhif
    {
        //Gets a string from the RhIF code, automatically reading its size value, and increments the index by the correct value.
        std::string getString(uint8_t *&bytes);

        //Gets n characters from the RhIF code and places them into a string, then increments the index by the correct value.
        std::string getString(uint8_t *&bytes, size_t n);

        //Gets a value of type T from the RhIF code and increments the index by its size.
        template <typename T>
        T getValue(uint8_t *&bytes)
        {
            T output;
            std::memcpy(&output, bytes, sizeof(T));
            bytes += sizeof(T);
            return output;
        }

        //Gets a string from the RhIF code, automatically reading its size value, and increments the index by the correct value.
        std::string getString(const std::span<uint8_t> &bytes, size_t &index);

        //Gets n characters from the RhIF code and places them into a string, then increments the index by the correct value.
        std::string getString(const std::span<uint8_t> &bytes, size_t &index, size_t n);

        //Gets a value of type T from the RhIF code and increments the index by its size.
        template <typename T>
        T getValue(const std::span<uint8_t> &bytes, size_t &index)
        {
            T output;
            std::memcpy(&output, bytes.data() + index, sizeof(T));
            index += sizeof(T);
            return output;
        }

        //Gets a value of type T from the RhIF code but does not increment the index by its size.
        template <typename T>
        T peekValue(const std::span<uint8_t> &bytes, size_t index)
        {
            T output;
            std::memcpy(&output, bytes.data() + index, sizeof(T));
            return output;
        }
    }

    enum class ConsoleColor
    {
        Black = 0,
        Red,
        Green,
        Yellow,
        Blue,
        Magenta,
        Cyan,
        White,
        Grey = 60,
        BrightRed,
        BrightGreen,
        BrightYellow,
        BrightBlue,
        BrightMagenta,
        BrightCyan,
        BrightWhite
    };

    void setForegroundColor(ConsoleColor color);

    void setBackgroundColor(ConsoleColor color);

    void resetColors();

    enum class RhType : uint64_t
    {
        VOID = 0,
        INT8 = 256,
        INT16,
        INT32,
        INT64,
        INT128,
        INT256,
        UINT8,
        UINT16,
        UINT32,
        UINT64,
        UINT128,
        UINT256,
        FLOAT16,
        FLOAT32,
        FLOAT64,
        FLOAT128,
        FLOAT256,
        STRINGPTR,
        RAWPTR = 0xFFFFFFFFFFFFFFFF
    };
}

#endif