#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ctype.h>
#include <iostream>
#include <llvm/IR/Value.h>
#include <map>
#include <memory>
#include <span>
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>

#include "ParseContext.h"
#include "RhIFCompiler.h"
#include "RhcCodegen.h"
#include "RhcErrorLogging.h"
#include "RhcTypes.h"
#include "RhcUtils.h"
#include "SyntaxTree.h"
using namespace llvm;
using namespace rhc;

static LLVMContext llvmContext;
static rhif::ParseContext parseContext;

extern "C" void CompileRhIF(uint8_t *bytes)
{
    rhc::errors::LogError("Test error");
    rhc::errors::LogWarning("Test warning");
    // Each rhif::get<type> function also advances the pointer at bytes by the correct amount.
    // This is why it is not being done manually.
    if (rhif::getString(bytes, 4).compare("RhIF") != 0)
    {
        std::cerr << "rhc: Could not read magic bytes RhIF. This indicates an internal compiler error or data "
                     "corruption. Aborting.";
        return;
    }
    std::cout << "rhc: Found magic bytes RhIF" << std::endl;
    uint32_t formatVersion = rhif::getValue<uint32_t>(bytes);
    std::cout << "rhc: Got RhIF version " << formatVersion << std::endl;
    if (formatVersion != RHIF_VERSION)
    {
        std::cerr << "rhc: RhIF version mismatch: expected version " << RHIF_VERSION << " but data is version "
                  << formatVersion << std::endl;
        return;
    }
    uint64_t codeSize = rhif::getValue<uint64_t>(bytes);
    std::cout << "rhc: RhIF indicates that it is " << codeSize << " bytes long." << std::endl;
    std::span<uint8_t> code(bytes, codeSize - 32);
    size_t index = 0;
    uint8_t *codeEnd = bytes + codeSize - 32;
    std::string fileName = rhif::getString(code, index);
    std::cout << "rhc: Got RhIF file name " << fileName << std::endl;

    SyntaxTree tree(fileName);

    uint16_t opCode;
    while (index < code.size())
    {
        opCode = rhif::getValue<uint16_t>(code, index);
        switch (opCode)
        {
            case 0:
                // Begin scope
                std::cout << "rhc: Got bsc operation" << std::endl;
                break;

            case 1:
                // End scope
                std::cout << "rhc: Got esc operation" << std::endl;
                break;

            case 2:
            {
                // Function declaration
                uint32_t flags = rhif::getValue<uint32_t>(code, index);
                bool rh_extern = flags & 0x20'00'00'00;
                uint64_t type = rhif::getValue<uint64_t>(code, index);
                std::string name = rhif::getString(code, index);
                uint64_t paramCount = rhif::getValue<uint64_t>(code, index);
                std::vector<RhVariable> parameters;

                for (uint64_t i = 0; i < paramCount; i++)
                {
                    uint32_t paramFlags = rhif::getValue<uint32_t>(code, index);
                    bool paramRef = paramFlags & 0x80'00'00'00;
                    bool paramConst = paramFlags & 0x40'00'00'00;
                    bool paramDefaultValue = paramFlags & 0x20'00'00'00;
                    bool paramPtr = paramFlags & 0x10'00'00'00;
                    bool paramArray = paramFlags & 0x08'00'00'00;
                    uint64_t arraySize = 0;
                    if (paramArray)
                        arraySize = rhif::getValue<uint64_t>(code, index);

                    uint64_t paramType = rhif::getValue<uint32_t>(code, index);
                    std::string paramName = rhif::getString(code, index);
                    RhVariable variable(paramType, paramName, paramRef, paramConst, paramPtr, paramArray, arraySize);
                    parameters.push_back(variable);
                }

                FunctionDeclaration declaration = FunctionDeclaration(type, name, parameters, rh_extern);
                tree.GlobalScope->AddChild(declaration);
                break;
            }
            case 3:
                // Function call
                break;

            case 4:
            {
                // Variable declaration
                uint32_t flags = rhif::getValue<uint32_t>(code, index);
                bool rh_ref = flags & 0x80'00'00'00;
                bool rh_const = flags & 0x40'00'00'00;
                bool rh_extern = flags & 0x20'00'00'00;
                bool rh_ptr = flags & 0x10'00'00'00;
                bool rh_array = flags & 0x08'00'00'00;
                uint64_t arraySize = 0;
                if (rh_array)
                    arraySize = rhif::getValue<uint64_t>(code, index);

                uint64_t type = rhif::getValue<uint64_t>(code, index);
                std::string name = rhif::getString(code, index);

                if (!rh_extern)
                {
                    uint64_t expressionLength = rhif::getValue<uint64_t>(code, index);
                    std::cout << "rhc: Reading expression of length " << expressionLength << std::endl;
                    for (uint64_t i = 0; i < expressionLength; i++)
                    {
                        ExpressionElementType elementType = rhif::getValue<ExpressionElementType>(code, index);
                        if (elementType == ExpressionElementType::Value)
                        {
                            uint64_t dataType = rhif::getValue<uint64_t>(code, index);
                        }
                    }
                }

                break;
            }
            case 5:
                // Variable assignment
                break;

            case 6:
                // Typedef- probably don't need to do much for this one since it's evaluated at parse time currently
                break;

            case 7:
                // Exit
                break;

            case 8:
                // If
                break;

            case 9:
                // Else
                break;

            case 10:
                // While
                break;

            case 11:
                // Post-While
                break;

            case 12:
                // Return
                break;

            default:

                rhc::errors::LogError("Unrecognized RhIF operation: " + std::to_string(opCode));
                break;
        }
    }
}

Expression parseExpression(const std::span<uint8_t> &bytes, size_t &index)
{
    Expression output;
    uint64_t expressionLength = rhif::getValue<uint64_t>(bytes, index);
    for (uint64_t i = 0; i < expressionLength; i++)
    {
        ExpressionElement element;
        element.type = rhif::getValue<ExpressionElementType>(bytes, index);
        switch (element.type)
        {
            case ExpressionElementType::Value:
            {
                RhType type = rhif::peekValue<RhType>(bytes, index);
                size_t valueSize;
                if (type == RhType::STRINGPTR)
                {
                    valueSize = rhif::peekValue<uint64_t>(bytes, index) + 8;
                }
                else
                {
                    valueSize = TypeSizes[type];
                }
                element.contents = bytes.subspan(index, valueSize + 8);
                index += valueSize + 8;
            }

            case ExpressionElementType::Variable:
            {
                uint64_t length = rhif::peekValue<uint64_t>(bytes, index);
                element.contents = bytes.subspan(index, length + 8);
                index += length + 8;
            }

            case ExpressionElementType::BuiltInOp:
            {
                element.contents = bytes.subspan(index, 2);
                index += 2;
            }

            case ExpressionElementType::CustomOp:
            {
                uint64_t length = rhif::peekValue<uint64_t>(bytes, index);
                element.contents = bytes.subspan(index, length + 8);
                index += length + 8;
            }

            case ExpressionElementType::OpenParen:
            {
                element.contents = bytes.subspan(index, 0);
            }

            case ExpressionElementType::CloseParen:
            {
                element.contents = bytes.subspan(index, 0);
            }

            case ExpressionElementType::FunctionCall:
            {
                
            }
        }
    }
}