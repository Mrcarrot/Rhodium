#include "SyntaxTree.h"

SyntaxTree::SyntaxTree(std::string fileName)
{
    FileName = fileName;
    GlobalScope = std::make_shared<Scope>();
}

Scope::Scope()
{
    NamedValues = std::map<std::string, llvm::Value*>();
    Parent = std::weak_ptr<Scope>();
}

Scope::Scope(std::weak_ptr<Scope> parent)
{
    std::shared_ptr<Scope> parentShared = parent.lock();
    Parent = parent;
    if (parentShared != nullptr)
    {
        NamedValues = parentShared->NamedValues;
    }
}

llvm::Value* Scope::Codegen()
{
    
}

void Scope::AddChild(STNode &node)
{
    Children.push_back(std::move(node));
}

FunctionDeclaration::FunctionDeclaration(uint64_t type, std::string name, std::vector<RhVariable> parameters, bool Extern)
{
    Type = type;
    Name = name;
    ParamCount = parameters.size();
    Parameters = parameters;
}

RhVariable::RhVariable(uint64_t type, std::string name, bool rh_ref, bool rh_const, bool rh_ptr, bool rh_array, uint64_t arraySize)
{
    Type = type;
    Name = name;
    Ref = rh_ref;
    Const = rh_const;
    Pointer = rh_ptr;
    Array = rh_array;
    ArraySize = arraySize;
}