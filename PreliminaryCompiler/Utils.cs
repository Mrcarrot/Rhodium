using System.Text;

public class Utils
{
    public const string CurrentVersion = "pre-alpha";

    public static readonly string[] ReservedWords = 
    {
        "if",
        "typedef",
        "for",
        "while",
        "extern",
        "return",
        "sizeof"
    };

    public const ulong PtrBit = 0x8000000000000000;
    /// <summary>
    /// Returns true if the specified character may occupy the starting position in an identifier
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public static bool IsIdentifierStartCharacter(char c)
    {
        return (!Char.IsDigit(c) && !IsGroupingCharacter(c) && !IsOperatorCharacter(c));
    }

    /// <summary>
    /// Returns true if the specified character may be in any position after the first in an identifier
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public static bool IsIdentifierCharacter(char c)
    {
        return (Char.IsLetterOrDigit(c) || c == '_') && (!IsGroupingCharacter(c) && !IsOperatorCharacter(c));
    }

    /// <summary>
    /// Checks whether the provided string is a valid Rhodium identifier.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="includeInternal">Whether to include identifiers that may not be user-defined.</param>
    /// <returns></returns>
    public static bool IsValidIdentifier(string s, bool includeInternal = false)
    {
        if (s.Length == 0) return false;
        if (!IsIdentifierStartCharacter(s[0])) return false;
        if (s.StartsWith("__") && !includeInternal) return false; //__ is reserved for compiler-internal definitions
        if (ReservedWords.Contains(s) && !includeInternal) return false;

        for (int i = 1; i < s.Length; i++)
        {
            if (!IsIdentifierCharacter(s[i])) return false;
        }

        return true;
    }

    public static bool IsGroupingCharacter(char c)
    {
        return c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}' || c == ';';
    }

    public static bool IsScopeChangingCharacter(char c)
    {
        return c == ';' || c == '{' || c == '}';
    }

    public static bool IsOperatorCharacter(char c)
    {
        return Char.IsPunctuation(c) || c == '+' || c == '<' || c == '>' || c == '=';
    }

    public static bool IsNumericLiteral(string s)
    {
        if (s.Length == 0) return false;
        if (s.Any(x => !Char.IsAsciiDigit(x) && x != '.' && x != '\''))
        {
            if (s.StartsWith("0x") && !s[2..].Any(x => !Char.IsAsciiHexDigit(x) && x != '.' && x != '\'')) //Valid hex literal
            {
                return true;
            }
            else if (s.StartsWith("0b") && !s[2..].Any(x => x != '0' && x != '1' && x != '.' && x != '\''))
            {
                return true;
            }
            return false;
        }
        return true;
    }

    public static char GetMatchingGroupingChar(char c) => c switch
    {
        '(' => ')',
        ')' => '(',
        '[' => ']',
        ']' => '[',
        '{' => '}',
        '}' => '{',
        '\'' or '"' => c,
        _ => throw new ArgumentOutOfRangeException("Input character is not a recognized grouping character")
    };

    public static byte[] MakeRhIFString(string str)
    {
        var sizeBytes = BitConverter.GetBytes((long)str.Length);
        var contentBytes = Encoding.UTF8.GetBytes(str);
        List<byte> output = new(sizeBytes.Length + contentBytes.Length);
        output.AddRange(sizeBytes);
        output.AddRange(contentBytes);
        return output.ToArray();
    }
}