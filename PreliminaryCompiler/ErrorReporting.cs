#define RHC_DEBUG

#if RHC_DEBUG
using System.Runtime.CompilerServices;
#endif

public class ErrorReporting
{
    public static int Errors = 0;
    public static int Warnings = 0;

    /// <summary>
    /// Writes an error message for the given token to stderr and increments the error counter.
    /// </summary>
    /// <param name="token"></param>
    /// <param name="message"></param>
#if RHC_DEBUG
    public static void CompilerError(Token token, string message, string? note = null,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
#else
    public static void CompilerError(Token token, string message, string? note = null)
#endif
    {
        Errors++;
        Console.ForegroundColor = ConsoleColor.Red;
        WriteError(message, token.file, token.line, token.col);

        if (note != null)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteNote(note, token.file, token.line, token.col);
        }

#if RHC_DEBUG
        WriteNote($"Called from {memberName}({sourceFilePath}:{sourceLineNumber})");
#endif

        Console.ResetColor();
    }

    /// <summary>
    /// Writes an error message with the given information to stderr and increments the error counter.
    /// </summary>
    /// <param name="file"></param>
    /// <param name="line"></param>
    /// <param name="col"></param>
    /// <param name="message"></param>
#if RHC_DEBUG
    public static void CompilerError(string file, int line, int col, string message, string? note = null,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
#else
    public static void CompilerError(string file, int line, int col, string message, string? note = null)
#endif
    {
        Errors++;
        Console.ForegroundColor = ConsoleColor.Red;
        WriteError(message, file, line, col);

        if (note != null)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteNote(note, file, line, col);
        }

#if RHC_DEBUG
        WriteNote($"Called from {memberName}({sourceFilePath}:{sourceLineNumber})");
#endif

        Console.ResetColor();
    }

    /// <summary>
    /// Writes a warning message for the given token to stderr and increments the warning counter.
    /// </summary>
    /// <param name="token"></param>
    /// <param name="message"></param>
    #if RHC_DEBUG
    public static void CompilerWarning(Token token, string message, string? note = null,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
    #else
    public static void CompilerWarning(Token token, string message, string? note = null)
    #endif
    {
        Warnings++;
        Console.ForegroundColor = ConsoleColor.Yellow;
        WriteWarning(message, token.file, token.line, token.col);
        if (note != null)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteNote(note, token.file, token.line, token.col);
        }

#if RHC_DEBUG
        WriteNote($"Called from {memberName}({sourceFilePath}:{sourceLineNumber})");
#endif

        Console.ResetColor();
    }

    /// <summary>
    /// Writes a warning message with the given information to stderr and increments the warning counter.
    /// </summary>
    /// <param name="file"></param>
    /// <param name="line"></param>
    /// <param name="col"></param>
    /// <param name="message"></param>
    #if RHC_DEBUG
    public static void CompilerWarning(string file, int line, int col, string message, string? note = null,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
    #else
    public static void CompilerWarning(string file, int line, int col, string message, string? note = null)
    #endif    
    {
        Warnings++;
        Console.ForegroundColor = ConsoleColor.Yellow;
        WriteWarning(message, file, line, col);
        if (note != null)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteNote(note, file, line, col);
        }
        Console.ResetColor();
    }

    /// <summary>
    /// Writes an error message with the given information to stderr.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="file"></param>
    /// <param name="line"></param>
    /// <param name="col"></param>
    public static void WriteError(string message, string file, int line, int col)
    {
        Console.Error.WriteLine($"rhc: Error: {file}, line {line} col {col}: {message}");
    }

    /// <summary>
    /// Writes a warning message with the given information to stderr.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="file"></param>
    /// <param name="line"></param>
    /// <param name="col"></param>
    public static void WriteWarning(string message, string file, int line, int col)
    {
        Console.Error.WriteLine($"rhc: Warning: {file}, line {line} col {col}: {message}");
    }

    /// <summary>
    /// Writes a note message with the given information to stderr.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="file"></param>
    /// <param name="line"></param>
    /// <param name="col"></param>
    public static void WriteNote(string message, string file, int line, int col)
    {
        Console.Error.WriteLine($"rhc: Note: {file}, line {line} col {col}: {message}");
    }

    /// <summary>
    /// Writes an error message to stderr.
    /// </summary>
    /// <param name="message"></param>
    public static void WriteError(string message)
    {
        Console.Error.WriteLine($"rhc: Error: {message}");
    }

    /// <summary>
    /// Writes a warning message to stderr.
    /// </summary>
    /// <param name="message"></param>
    public static void WriteWarning(string message)
    {
        Console.Error.WriteLine($"rhc: Warning: {message}");
    }

    /// <summary>
    /// Writes a note message to stderr.
    /// </summary>
    /// <param name="message"></param>
    public static void WriteNote(string message)
    {
        Console.Error.WriteLine($"rhc: Note: {message}");
    }

    /// <summary>
    /// Sends an error message in the form "Expected {expected} but got {token.text}".
    /// </summary>
    /// <param name="token"></param>
    /// <param name="expected"></param>
    public static void ExpectError(Token token, string expected)
    {
        CompilerError(token, $"Expected {expected} but got {token.text}");
    }
}