	.text
	.file	"HelloWorld.rh"
	.globl	Write                           # -- Begin function Write
	.p2align	4, 0x90
	.type	Write,@function
Write:                                  # @Write
	.cfi_startproc
# %bb.0:
	movq	%rdi, -8(%rsp)
	retq
.Lfunc_end0:
	.size	Write, .Lfunc_end0-Write
	.cfi_endproc
                                        # -- End function
	.globl	WriteLine                       # -- Begin function WriteLine
	.p2align	4, 0x90
	.type	WriteLine,@function
WriteLine:                              # @WriteLine
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	movq	%rdi, (%rsp)
	callq	puts@PLT
	popq	%rax
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end1:
	.size	WriteLine, .Lfunc_end1-WriteLine
	.cfi_endproc
                                        # -- End function
	.globl	GetANumber                      # -- Begin function GetANumber
	.p2align	4, 0x90
	.type	GetANumber,@function
GetANumber:                             # @GetANumber
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	callq	rand@PLT
	movl	%eax, 4(%rsp)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end2:
	.size	GetANumber, .Lfunc_end2-GetANumber
	.cfi_endproc
                                        # -- End function
	.globl	main                            # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	movl	%edi, 12(%rsp)
	movq	%rsi, 24(%rsp)
	movl	$0, 16(%rsp)
	movq	16(%rsp), %rdi
	callq	time@PLT
	movl	%eax, 36(%rsp)
	movl	%eax, %edi
	callq	srand@PLT
	movl	$.L.str.0, %edi
	callq	WriteLine
	movl	$10, 32(%rsp)
	callq	GetANumber
	movl	%eax, 8(%rsp)
	movq	24(%rsp), %rsi
	movl	$.L.str.1, %edi
	callq	printf@PLT
	movl	12(%rsp), %esi
	movl	$.L.str.2, %edi
	callq	printf@PLT
	movl	8(%rsp), %edx
	movl	$.L.str.3, %edi
	movl	$5, %esi
	movl	$15, %ecx
	callq	printf@PLT
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc
                                        # -- End function
	.type	.L.str.0,@object                # @.str.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.0:
	.asciz	"Hello, world!"
	.size	.L.str.0, 14

	.type	.L.str.1,@object                # @.str.1
.L.str.1:
	.asciz	"%p\n"
	.size	.L.str.1, 4

	.type	.L.str.2,@object                # @.str.2
.L.str.2:
	.asciz	"%d\n"
	.size	.L.str.2, 4

	.type	.L.str.3,@object                # @.str.3
.L.str.3:
	.asciz	"%d * %d = %d\n"
	.size	.L.str.3, 14

	.section	".note.GNU-stack","",@progbits
