using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

/// <summary>
/// Represents a source code token and stores information about it.
/// </summary>
/// <param name="text">The text contents of the token.</param>
/// <param name="line">The line number of the file from which the token was read.</param>
/// <param name="col">The column number of the file where the token begins.</param>
/// <param name="file">The file from which the token was read.</param>
/// <returns></returns>
public record Token(string text, int line, int col, string file);

/// <summary>
/// Represents a single character of source code and its location.
/// </summary>
/// <param name="c">The character.</param>
/// <param name="line">The line on which the character is found.</param>
/// <param name="col">The column at which the character is found.</param>
/// <param name="file">The file from which the character was read.</param>
/// <returns></returns>
public record CharPosition(char c, int line, int col, string file);

public class Lexer
{


    public static List<Token>? LexFile(string file, bool preprocess, List<string>? includedFiles = null, List<string>? definedIdentifiers = null) => 
    LexCode(
        File.ReadAllText(file).ReplaceLineEndings("\n"), //fuck windows
            file, preprocess, includedFiles, definedIdentifiers);
    
    public static List<Token>? LexCode(string code, string file, bool preprocess, List<string>? includedFiles = null, List<string>? definedIdentifiers = null)
    {
        
        string rootFilePath = new FileInfo(file).FullName;
        //Invoke the C Preprocessor for basic include support- will be removed later
        //Has been removed as of the addition of basic native preprocessing
        /*if (preprocess)
        {
            //Check the platform and load the very low-level code accordingly
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                code = $"#define RHPLATFORM_LINUX\n{code}";
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                code = $"#define RHPLATFORM_WINDOWS\n{code}";
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                code = $"#define RHPLATFORM_MACOS\n{code}";
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.FreeBSD))
                code = $"#define RHPLATFORM_FREEBSD\n{code}";
            else code = $"#define RHPLATFORM_UNKNOWN\n{code}";

            code = RuntimeInformation.OSArchitecture switch
            {
                Architecture.X64 => $"#define RHARCH_AMD64\n{code}",
                Architecture.X86 => $"#define RHARCH_I686\n{code}",
                Architecture.Arm => $"#define RHARCH_ARM\n{code}",
                Architecture.Arm64 => $"#define RHARCH_ARM64\n{code}",
                _ => $"#define RHARCH_UNKNOWN {code}"
            };

            Console.WriteLine("rhc: Running C Preprocessor");

            string tempFile = $"{Path.GetDirectoryName(file)}/rh_temp_preprocessor_file.rh";

            File.WriteAllText(tempFile, code);

            ProcessStartInfo preprocessor = new("cpp", tempFile);
            preprocessor.RedirectStandardOutput = true;
            var cppProcess = Process.Start(preprocessor);
            if (cppProcess is null)
            {
                ErrorReporting.WriteError("C preprocessor failed to start");
                ErrorReporting.Errors++;
                return new();
            }
            code = cppProcess.StandardOutput.ReadToEnd();
            cppProcess.WaitForExit();
            /*ProcessStartInfo preprocessor = new("cpp", "-");
            preprocessor.RedirectStandardOutput = true;
            preprocessor.RedirectStandardInput = true;
            preprocessor.UseShellExecute = false;
            var cppProcess = Process.Start(preprocessor);
            if (cppProcess is null)
            {
                ErrorReporting.WriteError("C preprocessor failed to start");
                ErrorReporting.Errors++;
                return new();
            }
            cppProcess.StandardInput.Write(code);
            code = cppProcess.StandardOutput.ReadToEnd();
            cppProcess.WaitForExit();
        }
        else Console.WriteLine("rhc: Skipping C Preprocessor step");*/

        List<Token> output = new List<Token>()
        {
            new("", 0, 0, file) //Start of file token
        };
        Stack<CharPosition> Grouping = new();

        /*List<string> splitFile = file.Split('\n').Where(x => x.Trim() != "").Where(x => !x.Trim().StartsWith("//")).ToList();
        splitFile.ForEach(x => x = x.Trim());

        file = "";
        splitFile.ForEach(x => file += $"\n{x}");
        splitFile = file.Split().ToList();
        splitFile = splitFile.Where(x => x != "").ToList();
        
        for (int i = 0; i < splitFile.Count; i++)
        {
            Console.WriteLine(splitFile[i]);
        }*/

        string currentToken = "";
        Token? previousToken() => output.Any() ? output.Last() : null;

        //Keep track of the last character so we can check its class- its initial value, the null terminator, will not satisfy any check.
        char previousChar = '\0';

        int line = 1, col = 0;
        //int rootFileLine = 1;

        bool addCurrentToken()
        {
            if (currentToken.Trim() != "")
            {
                output.Add(new Token(currentToken.Trim(), line, col - currentToken.Trim().Length, file));
                return true;
            }
            return false;
        }

        includedFiles = includedFiles ?? new();
        definedIdentifiers = definedIdentifiers ?? new();
        bool inIfBlock = false;
        bool lastIfCondition = false;
        int ifNestingLevel = 0;

        for (int i = 0; i < code.Length; i++)
        {
            char c = code[i];
            col++;
            if (Char.IsWhiteSpace(c))
            {
                addCurrentToken();
                currentToken = "";
                if (c == '\n')
                {
                    line++;
                    col = 0;
                }
            }
            else if ((c == '"' || c == '\'') && previousChar != '\\')
            {
                addCurrentToken();
                currentToken = c.ToString();
                do
                {
                    i++;
                    col++;
                    if (i == code.Length)
                    {
                        ErrorReporting.CompilerError(file, line, col, $"Expected `{c}` but got end of file instead");
                        return null;
                    }
                    if (code[i] == '\r' || code[i] == '\n')
                    {
                        ErrorReporting.CompilerError(file, line, col, $"Expected `{c}` but got newline instead");
                        return null;
                    }
                    currentToken += code[i];
                } while (code[i] != c || code[i - 1] == '\\');
                addCurrentToken();
                currentToken = "";
                continue;
            }
            else if ((c == '/' && previousChar == '/'))
            {
                if (output[^1].text == "/") output.RemoveAt(output.Count - 1);
                while (code[i] != '\n' && i < code.Length)
                {
                    i++;
                }
                line++;
                col = 0;
                currentToken = "";
                continue;
            }
            else if (c == '#')
            {
                (int directiveStartLine, int directiveStartCol) = (line, col);
                StringBuilder hashString = new(64);
                while (i < code.Length && code[i] != '\n')
                {
                    hashString.Append(code[i]);
                    i++;
                }
                //Console.WriteLine(hashString);
                string[] lineSplit = Regex.Split(hashString.ToString(), "(?:^| )(\"(?:[^\"])*\"|[^ ]*)").Where(x => x.Length >= 1).ToArray();
                switch (lineSplit[0])
                {
                    case ("#include"):
                    {
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #include preprocessor directive takes one argument");
                        }
                        else
                        {
                            string includedFile = lineSplit[1];
                            if (includedFile.StartsWith('"'))
                            {
                                if (includedFile.EndsWith('"'))
                                {
                                    includedFile = includedFile[1..^1];
                                }
                                else
                                {
                                    ErrorReporting.CompilerError(file, line, col, "Mismatched quotes in preprocessor directive");
                                }
                            }
                            includedFile = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(file)!, includedFile));
                            if (!includedFiles.Contains(includedFile))
                            {
                                if (!File.Exists(includedFile))
                                {
                                    ErrorReporting.CompilerError(file, line, col, $"Included file {includedFile} not found");
                                }
                                else
                                {
                                    includedFiles.Add(includedFile);
                                    var includedTokens = LexFile(includedFile, false, includedFiles, definedIdentifiers);
                                    output.AddRange(includedTokens!.GetRange(1, includedTokens.Count - 2));
                                }
                            }
                        }
                        break;
                    }
                    case ("#define"):
                    {
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #define preprocessor directive takes one argument");
                        }
                        else
                        {
                            string identifier = lineSplit[1];
                            if (!definedIdentifiers.Contains(identifier))
                                definedIdentifiers.Add(identifier);
                        }
                        break;
                    }
                    case ("#undefine"):
                    {
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #undefine preprocessor directive takes one argument");
                        }
                        else
                        {
                            string identifier = lineSplit[1];
                            definedIdentifiers.Remove(identifier);
                        }
                        break;
                    }
                    case ("#if_defined"):
                    {
                        ifNestingLevel = 1;
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #if_defined preprocessor directive takes one argument");
                        }
                        else
                        {
                            string identifier = lineSplit[1];
                            if (!definedIdentifiers.Contains(identifier))
                            {
                                while (true)
                                {
                                    while (code[i] != '#')
                                    {
                                        i++;
                                        col++;
                                        if (i == code.Length)
                                        {
                                            ErrorReporting.CompilerError(file, line, col, $"Expected #endif preprocessor directive but got end of file instead");
                                            return null;
                                        }
                                        if (code[i] == '\n')
                                        {
                                            line++;
                                            col = 0;
                                        }
                                    }
                                    hashString = new(64);
                                    while (code[i] != '\n')
                                    {
                                        hashString.Append(code[i]);
                                        i++;
                                        if (i == code.Length) break;
                                        col++;
                                    }
                                    line++;
                                    col = 0;
                                    if (hashString.ToString().StartsWith("#if")) ifNestingLevel++;
                                    if (hashString.ToString() == "#endif") ifNestingLevel--;
                                    if (ifNestingLevel == 0) break;
                                }
                            }
                        }
                        break;
                    }
                    case ("#if_not_defined"):
                    {
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #if_not_defined preprocessor directive takes one argument");
                        }
                        else
                        {
                            string identifier = lineSplit[1];
                            if (definedIdentifiers.Contains(identifier))
                            {
                                while (true)
                                {
                                    while (code[i] != '#')
                                    {
                                        i++;
                                        col++;
                                        if (i == code.Length)
                                        {
                                            ErrorReporting.CompilerError(file, line, col, $"Expected #endif preprocessor directive but got end of file instead");
                                            return null;
                                        }
                                        if (code[i] == '\n')
                                        {
                                            line++;
                                            col = 0;
                                        }
                                    }
                                    hashString = new(64);
                                    while (code[i] != '\n')
                                    {
                                        hashString.Append(code[i]);
                                        i++;
                                        if (i == code.Length) break;
                                        col++;
                                    }
                                    line++;
                                    col = 0;
                                    if (hashString.ToString().StartsWith("#if")) ifNestingLevel++;
                                    if (hashString.ToString() == "#endif") ifNestingLevel--;
                                    if (ifNestingLevel == 0) break;
                                }
                            }
                        }
                        break;
                    }
                    case ("#if_os"):
                    {
                        ifNestingLevel++;
                        Target target = Platform.CurrentTarget;
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #if_os preprocessor directive takes one argument");
                        }
                        bool isTargetOS = lineSplit[1].ToLower() switch
                        {
                            "linux" or "linux-gnu" => target.os == OS.GNULinux,
                            "linux-musl" => target.os == OS.MUSLLinux,
                            "osx" or "darwin" or "macos" => target.os == OS.Darwin,
                            "win" or "windows" => target.os is OS.WindowsGNU or OS.WindowsMSVC,
                            _ => false
                        };
                        //Console.WriteLine($"{lineSplit[1]} {(isTargetOS ? "is" : "is not")} {target.os}");
                        if (!isTargetOS)
                        {
                            while (true)
                            {
                                while (code[i] != '#')
                                {
                                    i++;
                                    col++;
                                    if (i == code.Length)
                                    {
                                        ErrorReporting.CompilerError(file, line, col, $"Expected #endif preprocessor directive but got end of file instead", $"#if_os directive at {directiveStartLine}:{directiveStartCol}");
                                        return null;
                                    }
                                    if (code[i] == '\n')
                                    {
                                        line++;
                                        col = 0;
                                    }
                                }
                                hashString = new(64);
                                while (code[i] != '\n')
                                {
                                    hashString.Append(code[i]);
                                    i++;
                                    if (i == code.Length) break;
                                    col++;
                                }
                                line++;
                                col = 0;
                                if (hashString.ToString().StartsWith("#if")) ifNestingLevel++;
                                if (hashString.ToString() is "#endif" or "#else") 
                                {
                                    ifNestingLevel--;
                                }
                                if (ifNestingLevel <= 0) break;
                                /*
                                i 01234567
                                1 code();
                                i 890123
                            col  0123456
                                2 #else
                                       ^
                                */
                            }
                        }
                        inIfBlock = true;
                        lastIfCondition = isTargetOS;
                        break;
                    }
                    case ("#if_arch"):
                    {
                        ifNestingLevel++;
                        Target target = Platform.CurrentTarget;
                        if (lineSplit.Length != 2)
                        {
                            ErrorReporting.CompilerError(file, line, col, "The #if_os preprocessor directive takes one argument");
                        }
                        bool isTargetArch = lineSplit[1].ToLower() switch
                        {
                            "amd64" or "x86_64" => target.arch == CPUArch.x86_64,
                            "i686" or "x86" => target.arch == CPUArch.i686,
                            "arm" => target.arch == CPUArch.arm,
                            "aarch64" or "arm64" => target.arch == CPUArch.aarch64,
                            _ => false
                        };
                        if (!isTargetArch)
                        {
                            while (true)
                            {
                                while (code[i] != '#')
                                {
                                    i++;
                                    col++;
                                    if (i == code.Length)
                                    {
                                        ErrorReporting.CompilerError(file, line, col, $"Expected #endif preprocessor directive but got end of file instead");
                                        return null;
                                    }
                                    if (code[i] == '\n')
                                    {
                                        line++;
                                        col = 0;
                                    }
                                }
                                hashString = new(64);
                                while (code[i] != '\n')
                                {
                                    hashString.Append(code[i]);
                                    i++;
                                    if (i == code.Length) break;
                                    col++;
                                }
                                line++;
                                col = 0;
                                if (hashString.ToString() == "#endif") break;
                                /*
                                i 01234567
                                1 code();
                                i 890123
                            col  0123456
                                2 #else
                                       ^
                                */
                                if (hashString.ToString() == "#else") break;
                            }
                        }
                        inIfBlock = true;
                        lastIfCondition = isTargetArch;
                        break;
                    }
                    case ("#else"):
                    {
                        if (!inIfBlock) ErrorReporting.CompilerError(file, line, col, "#else directive is invalid: not in if block");
                        if (lastIfCondition)
                        {
                            while (true)
                            {
                                while (code[i] != '#')
                                {
                                    i++;
                                    col++;
                                    if (i == code.Length)
                                    {
                                        ErrorReporting.CompilerError(file, line, col, $"Expected #endif preprocessor directive but got end of file instead");
                                        return null;
                                    }
                                    if (code[i] == '\n')
                                    {
                                        line++;
                                        col = 0;
                                    }
                                }
                                hashString = new(64);
                                while (code[i] != '\n')
                                {
                                    hashString.Append(code[i]);
                                    i++;
                                    if (i == code.Length) break;
                                    col++;
                                }
                                line++;
                                col = 0;
                                if (hashString.ToString() == "#endif") break;
                            }
                        }
                        break;
                    }
                    case ("#endif"):
                    {
                        ifNestingLevel--;
                        if (ifNestingLevel == 0) inIfBlock = false;
                        break;
                    }
                    case ("#warning"):
                    {   
                        StringBuilder warningMessage = new(32);
                        for (int j = 1; j < lineSplit.Length; j++) warningMessage.Append($"{lineSplit[j]} ");
                        ErrorReporting.WriteWarning(warningMessage.ToString());
                        break;
                    }
                    default:
                    {
                        ErrorReporting.CompilerError(file, line, col, $"Invalid preprocessor directive: {lineSplit[0]}");
                        break;
                    }
                }
                line++;
                col = 0;
                continue;
            }
            else if (c == '*' && previousChar == '/') //Because I see this approximately once every 5 minutes of looking at this code and freak out, no,
            {                                         //This is not scanning for '*/'- it's in reverse order. It's actually scanning for '/*'.
                output.RemoveAt(output.Count - 1);
                while (code[i] != '/' || (i > 0 && code[i - 1] != '*'))
                {
                    i++;
                    col++;
                    if (i == code.Length)
                    {
                        ErrorReporting.CompilerError(file, line, col, $"Expected `*/` end of comment but got end of file instead");
                        return null;
                    }
                    if (code[i] == '\n')
                    {
                        line++;
                        col = 0;
                    }
                }
                currentToken = "";
                continue;
            }
            else if (Utils.IsGroupingCharacter(c))
            {
                if (c == '{' || c == '[' || c == '(')
                {
                    Grouping.Push(new CharPosition(c, line, col, file));
                }
                if (c == '}' || c == ']' || c == ')')
                {
                    if (Grouping.TryPeek(out CharPosition? last))
                    {
                        if (last.c == Utils.GetMatchingGroupingChar(c))
                        {
                            Grouping.Pop();
                        }
                        else
                        {
                            ErrorReporting.CompilerError(file, line, col, $"Mismatched grouping: got '{c}' but expected '{Utils.GetMatchingGroupingChar(last.c)}'");
                        }
                    }
                    else
                    {
                        ErrorReporting.CompilerError(file, line, col, $"Mismatched grouping: got closing '{c}' but no opening '{Utils.GetMatchingGroupingChar(c)}'");
                    }
                }
                addCurrentToken();
                currentToken = "";
                output.Add(new Token(c.ToString(), line, col, file));
                continue;
            }
            else if (Utils.IsOperatorCharacter(c) && c != '_' && !(Utils.IsNumericLiteral(currentToken.Trim()) && (c == '.' || c == '\'')))
            {
                if (!Utils.IsOperatorCharacter(previousChar))
                {
                    addCurrentToken();
                    currentToken = "";
                    output.Add(new Token(c.ToString(), line, col, file));
                    previousChar = c;
                    continue;
                }
                else
                {
                    Token? previousTok = previousToken();
                    if (previousTok is not null)
                        output[output.Count - 1] = previousTok with { text = previousTok.text + c };
                    previousChar = c;
                    continue;
                }
            }
            else if (!Char.IsDigit(c) && c != '.' && c != '\'')
            {
                if (currentToken.Trim() != "" && Char.IsDigit(currentToken[0]))
                {
                    output.Add(new Token(currentToken.Trim(), line, col - currentToken.Trim().Length, file));
                    currentToken = "";
                }
            }

            if (!Utils.IsIdentifierCharacter(c) && Utils.IsIdentifierCharacter(previousChar) && !((c == '.' || c == '\'') && Utils.IsNumericLiteral(currentToken.Trim())))
            {
                addCurrentToken();
                currentToken = "";
            }

            currentToken += c;

            previousChar = c;
        }
        addCurrentToken();
        //Console.WriteLine($"Read {output.Count} tokens.");
        while (Grouping.Any())
        {
            CharPosition last = Grouping.Pop();

            ErrorReporting.CompilerError(file, line, col, $"Expected closing '{Utils.GetMatchingGroupingChar(last.c)}' but got end of file instead (Opening '{last.c}' at line {last.line} col {last.col})");
        }
        output.Add(new("", line + 1, 0, file));
        return ErrorReporting.Errors == 0 ? 
            output : 
            null;
    }
}
