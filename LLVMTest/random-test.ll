@.str = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

declare i32 @printf(ptr noundef) #1
declare i32 @scanf(ptr noundef, ...) #1

define dso_local i32 @main(i32 %0, ptr %1)
{
    %3 = mul i32 5, 3
    %4 = alloca i32, align 4
    store i32 32, ptr %4, align 4

    call i32 @printf(ptr noundef @.str, i32 %3)
    
    ; *%3 = alloca i32, align 4
    ; call i64 @scanf(ptr noundef @.str.2, ptr noundef %3)
    ; %5 = load i32, ptr %3
    ; %6 = trunc i32 %5 to i1
    ; %7 = zext i1 %6 to i32
    ; call i64 @printf(ptr @.str, i32 %7)*/
    ret i32 0
}
