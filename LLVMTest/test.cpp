class testClass
{
public:
    int x;
    float y;
    int get_x()
    {
	return x;
    }
};

int main()
{
    testClass test;
    test.x = 4;
    test.y = 234.23;
    int x = test.get_x();
}
