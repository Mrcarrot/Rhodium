extern puts

section .data
    text1   db "Hello, world!",0

section .text
    global _start

Main:
    ; push rbp
    ; mov rbp, rsp
    ; mov word [rbp-4], 4

    sub rsp, 8
    mov rdi, text1
    call puts wrt ..plt
    xor eax, eax
    add rsp, 8
ret

_start:
    call Main
ret

section .bss
    dummy resb 4