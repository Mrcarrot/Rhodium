	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	mpcore
	.eabi_attribute	6, 6	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.fpu	vfpv2
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	15, 1	@ Tag_ABI_PCS_RW_data
	.eabi_attribute	16, 1	@ Tag_ABI_PCS_RO_data
	.eabi_attribute	17, 2	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 0	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"Hello.ll"
	.globl	main                            @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                              @ @main
main:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	ldr	r0, .LCPI0_3
.LPC0_2:
	add	r0, pc, r0
	ldr	r1, [r0]
	ldr	r0, .LCPI0_2
.LPC0_1:
	add	r0, pc, r0
	bl	printf
	ldr	r0, .LCPI0_1
.LPC0_0:
	add	r0, pc, r0
	bl	puts
	ldr	r0, .LCPI0_0
	pop	{r11, pc}
	.p2align	2
@ %bb.1:
.LCPI0_0:
	.long	0                               @ 0x0
.LCPI0_1:
	.long	.L.str-(.LPC0_0+8)
.LCPI0_2:
	.long	.L.str2-(.LPC0_1+8)
.LCPI0_3:
	.long	.Lsixnine-(.LPC0_2+8)
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.fnend
                                        @ -- End function
	.type	.L.str,%object                  @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Hello, world!"
	.size	.L.str, 14

	.type	.L.str2,%object                 @ @.str2
.L.str2:
	.asciz	"%d\n"
	.size	.L.str2, 4

	.type	.Lsixnine,%object               @ @sixnine
	.section	.rodata,"a",%progbits
	.p2align	2
.Lsixnine:
	.long	69                              @ 0x45
	.size	.Lsixnine, 4

	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym puts
	.addrsig_sym printf
	.addrsig_sym .Lsixnine
	.eabi_attribute	30, 5	@ Tag_ABI_optimization_goals
