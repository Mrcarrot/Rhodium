; ModuleID = 'Hello.ll'
source_filename = "Hello.ll"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "arm-pc-linux-gnu"

; Declare the string constant as a global constant.
@.str = private unnamed_addr constant [14 x i8] c"Hello, world!\00"
@.str2 = private unnamed_addr constant [4 x i8] c"%d\0A\00"

@sixnine = private constant i32 69

; External declaration of the puts function
declare i32 @puts(i8* nocapture) nounwind
declare i32 @printf(i8* noalias nocapture, i32 nocapture)

; Definition of main function
define i32 @main() {   ; i32()*
  ; Convert [15 x i8]* to i8*...
  %i = load i32, i32* @sixnine
  %cast211 = getelementptr [4 x i8], [4 x i8]* @.str2, i64 0, i64 0
  call i32 @printf(i8* %cast211, i32 %i)
  %cast210 = getelementptr [14 x i8], [14 x i8]* @.str, i64 0, i64 0

  ; Call puts function to write out the string to stdout.
  call i32 @puts(i8* %cast210)
  ret i32 0
}

; Named metadata
!0 = !{i32 42, null, !"string"}
!foo = !{!0}
