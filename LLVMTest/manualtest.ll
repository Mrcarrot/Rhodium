@.str = private unnamed_addr constant [15 x i8] c"Hello, world!\0A\00", align 1

declare i32 @puts(i8* noundef) #1
declare i32 @putchar(i32 noundef) #1

define dso_local i32 @main(i32 noundef %0, i8** noundef %1) #0 {
call i32 @puts(i8* noundef getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0))
call i32 @putchar(i32 noundef 98)
ret i32 0
}
