; ModuleID = 'test.c'
source_filename = "test.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.testStruct = type { i32, float }

@.str = private unnamed_addr constant [6 x i8] c"Linux\00", align 1
@.str.1 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"aaa\00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"bbb\00", align 1
@.str.4 = private unnamed_addr constant [6 x i8] c"AAAAA\00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local ptr @testFunc(i32 noundef %0) #0 {
  %2 = alloca i32, align 4
  store i32 %0, ptr %2, align 4
  ret ptr @.str
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @main(i32 noundef %0, ptr noundef %1) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca ptr, align 8
  %6 = alloca i32, align 4
  %7 = alloca ptr, align 8
  %8 = alloca i32, align 4
  %9 = alloca %struct.testStruct, align 4
  %10 = alloca i32, align 4
  %11 = alloca double, align 8
  store i32 0, ptr %3, align 4
  store i32 %0, ptr %4, align 4
  store ptr %1, ptr %5, align 8
  %12 = load i32, ptr %4, align 4
  %13 = add nsw i32 %12, 3
  %14 = call i32 (ptr, ...) @printf(ptr noundef @.str.1, i32 noundef %13)
  %15 = load i32, ptr %4, align 4
  %16 = icmp ne i32 %15, 0
  br i1 %16, label %17, label %19

17:                                               ; preds = %2
  %18 = call i32 @puts(ptr noundef @.str.2)
  br label %19

19:                                               ; preds = %17, %2
  br label %20

20:                                               ; preds = %23, %19
  %21 = load i32, ptr %4, align 4
  %22 = icmp ne i32 %21, 0
  br i1 %22, label %23, label %25

23:                                               ; preds = %20
  %24 = call i32 @puts(ptr noundef @.str.3)
  br label %20, !llvm.loop !6

25:                                               ; preds = %20
  store i32 0, ptr %6, align 4
  %26 = load i32, ptr %6, align 4
  %27 = call ptr @testFunc(i32 noundef %26)
  store ptr @.str.4, ptr %7, align 8
  %28 = call i32 (ptr, ...) @__isoc99_scanf(ptr noundef @.str.5, ptr noundef %8)
  %29 = load ptr, ptr %7, align 8
  %30 = call i32 @puts(ptr noundef %29)
  %31 = getelementptr inbounds %struct.testStruct, ptr %9, i32 0, i32 0
  store i32 4, ptr %31, align 4
  %32 = getelementptr inbounds %struct.testStruct, ptr %9, i32 0, i32 1
  store float 0x407A73AE20000000, ptr %32, align 4
  %33 = call ptr @testFunc(i32 noundef 5)
  %34 = call i32 @puts(ptr noundef %33)
  %35 = call i32 @putchar(i32 noundef 65)
  store i32 4, ptr %10, align 4
  %36 = load i32, ptr %10, align 4
  %37 = uitofp i32 %36 to double
  store double %37, ptr %11, align 8
  %38 = load ptr, ptr %5, align 8
  %39 = getelementptr inbounds ptr, ptr %38, i64 69
  %40 = load ptr, ptr %39, align 8
  %41 = call i32 @puts(ptr noundef %40)
  ret i32 0
}

declare i32 @printf(ptr noundef, ...) #1

declare i32 @puts(ptr noundef) #1

declare i32 @__isoc99_scanf(ptr noundef, ...) #1

declare i32 @putchar(i32 noundef) #1

attributes #0 = { noinline nounwind optnone sspstrong uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }

!llvm.module.flags = !{!0, !1, !2, !3, !4}
!llvm.ident = !{!5}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{i32 7, !"uwtable", i32 2}
!4 = !{i32 7, !"frame-pointer", i32 2}
!5 = !{!"clang version 15.0.7"}
!6 = distinct !{!6, !7}
!7 = !{!"llvm.loop.mustprogress"}
